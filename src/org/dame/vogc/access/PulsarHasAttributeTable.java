package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.dame.vogc.datatypes.Pulsar;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class PulsarHasAttributeTable {

    public Pulsar object;
    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new PulsarHasAttribute object in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId
     * @param attId
     * @return
     * @throws VogcException
     */
//    protected void insertPulsarHasAttribute(String userName, String password, PulsarHasAttribute att) throws VogcException{
////        if(att.getPlsAttId()<0 || att.getPlsId() == null || att.getSourceId() == null || att.getValue() == null)
////        throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
//        try {
//            Calendar cal = Calendar.getInstance();
//            Date date = new Date(cal.getTimeInMillis());
//            Connection con = connection.connect();
//            PreparedStatement stm2 = con.prepareStatement("insert into pulsar_has_attribute values(?,?,?,?,?,?,?)");
//
//            stm2.setInt(1, 7);
//            stm2.setString(2, "pippo10" );
//            stm2.setInt(3, 1);
//            stm2.setString(4, "pls_catalogue");
//            stm2.setString(5, "1.21");
//           // stm2.setBoolean(6, true);
//            stm2.setInt(6, 1);
//            stm2.setString(7, date.toString());
//
//            if(stm2.executeUpdate()== 0) throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_INSERT_FAILED));
//
//
//        } catch (SQLException ex) {
//            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
//        }
//        finally{
//          try {connection.disconnect();
//          }catch (SQLException ex) {
//              throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));}
//        }
//    }
    protected PulsarHasAttribute[] selectPlsHasAttByPulsarIdandObjectId(String userName, String password, String objectId, int attId) throws VogcException {

        ResultSet r = null;

        try {
            r = DbStatements.queryHistoryPulsar(connection, userName, password, objectId, attId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            PulsarHasAttribute attribute[] = new PulsarHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new PulsarHasAttribute(r.getInt("id"), r.getInt("plsAttributeId"), r.getString("pulsarId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected void insertPulsarHasAttribute(String userName, String password, PulsarHasAttribute att) throws VogcException {
        if (att.getPlsAttId() < 0 || att.getPlsId() == null || att.getSourceId() == null || att.getValue() == null) {
            throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into pulsar_has_attribute values(?,?,?,?,?,?,?)");
            stm.setInt(1, 0);
            stm.setString(2, att.getPlsId());
            stm.setInt(3, att.getPlsAttId());
            stm.setString(4, att.getSourceId());
            stm.setString(5, att.getValue());
            stm.setBoolean(6, att.isFirst());
            stm.setString(7, date.toString());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_INSERT_FAILED));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a pulsarHasAttribute object from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId
     * @throws VogcException
     */
    protected void deletePulsarHasAttribute(String userName, String password, int plsHasAttId) throws VogcException {
        try {
            if (plsHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from pulsar_has_attribute where id=?");
            stm.setInt(1, plsHasAttId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to update the pulsar id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the pulsarHasAttribute id
     * @param pulsarId the new pulsar id
     * @throws VogcException
     */
    protected void updatePulsarHasAttPulsarId(String userName, String password, int plsHasAttId, int pulsarId) throws VogcException {
        try {
            if (plsHasAttId < 0 || pulsarId < 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar_has_attribute", "pulsarId", pulsarId, "id", plsHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the pulsar attribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the pulsarHasAttribute id
     * @param plsAttributeId
     * @throws VogcException
     */
    protected void updatePulsarHasAttPlsAttributeId(String userName, String password, int plsHasAttId, int plsAttributeId) throws VogcException {
        try {
            if (plsHasAttId < 0 || plsAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar_has_attribute", "plsAttributeId", plsAttributeId, "id", plsHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the source id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId
     * @return
     * @throws VogcException
     */
    protected PulsarHasAttribute selectPulsarSourceId(String userName, String password, String objectId) throws VogcException {
        ResultSet r = null;
        try {
            if (objectId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }

            r = DbStatements.query2(connection, userName, password, "pulsar_has_attribute", "sourceId", "pulsarId", objectId);

            //SELECT sourceId from vogclusters.gcluster_has_attribute where gclusterId = "12345"
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            PulsarHasAttribute att = new PulsarHasAttribute();
            att.setSourceId(r.getString("sourceId"));
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected void updatePulsarHasAttSourceId(String userName, String password, int plsHasAttId, String sourceId) throws VogcException {
        try {
            if (plsHasAttId < 0 || sourceId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar_has_attribute", "sourceId", sourceId, "id", plsHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value in a starHasAttrbute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param value the new value to insert
     * @param GcclusterId
     * @param Gcclusterattribute
     * @throws org.dame.vogc.exceptions.VogcException
     */
    protected void updatePulsarHasAttValue(String userName, String password, String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        try {
            if (Gcclusterattribute == null || GcclusterId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar_has_attribute", value, "pulsarId", GcclusterId, "plsAttributeId", "pls_attribute", Gcclusterattribute) == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag first in a
     * PulsarHasAttrbute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the PulsarHasAttribute id
     * @param first the new boolean value to insert
     * @throws VogcException
     */
    protected void updatePulsarHasAttFirst(String userName, String password, int plsHasAttId, boolean first) throws VogcException {
        try {
            if (plsHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar_has_attribute", "first", first, "id", plsHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag first in a
     * PulsarHasAttrbute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param value
     * @param GcclusterId
     * @param Gcclusterattribute
     * @throws VogcException
     */
    protected void updatePulsarHasAttDate(String userName, String password, String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        try {
            if (Gcclusterattribute == null || GcclusterId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar_has_attribute", "pulsarId", GcclusterId, "plsAttributeId", "pls_attribute", Gcclusterattribute) == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
// protected void updatePulsarHasAttDate(String userName, String password, int plsHasAttId)throws VogcException{
//       try{
//          if (plsHasAttId  < 0)
//                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
//          Calendar cal = Calendar.getInstance();
//          Date date = new Date(cal.getTimeInMillis());
//          if (DbStatements.update(connection, userName, password, "pulsar_has_attribute", "date", date, "id", plsHasAttId) == 0)
//              throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
//       } catch (SQLException ex) {
//            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
//        }
//    }

    /**
     * Method to select a PulsarHasAttribute object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the attribute id to select
     * @return - a PulsarHasAttribute object
     * @throws VogcException
     */
    protected PulsarHasAttribute selectPulsarHasAttribute(String userName, String password, int plsHasAttId) throws VogcException {
        try {
            if (plsHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "pulsar_has_attribute", "*", "id", plsHasAttId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            PulsarHasAttribute att = new PulsarHasAttribute(plsHasAttId, r.getInt("plsAttributeId"), r.getString("pulsarId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the pulsar id of a PulsarHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the PulsarHasAttribute id
     * @return - the pulsar id
     * @throws VogcException
     */
    protected String selectPulsarHasAttPulsarId(String userName, String password, int plsHasAttId) throws VogcException {
        return this.selectPulsarHasAttribute(userName, password, plsHasAttId).getPlsId();
    }

    /**
     * Method to select the pulsar's attribute id in a PulsarHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the PulsarHasAttribute id
     * @return - the pulsar's attribute id
     * @throws VogcException
     */
    protected int selectPulsarHasAttPlsAttributeId(String userName, String password, int plsHasAttId) throws VogcException {

        return this.selectPulsarHasAttribute(userName, password, plsHasAttId).getPlsAttId();
    }

    /**
     * Method to select the source id in a PulsarHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the PulsarHasAttribute id
     * @return - the source id
     * @throws VogcException
     */
    protected String selectPulsarHasAttSourceId(String userName, String password, int plsHasAttId) throws VogcException {
        return this.selectPulsarHasAttribute(userName, password, plsHasAttId).getSourceId();

    }

    /**
     * Method to select an attribute's value in a PulsarHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the PulsarHasAttribute id
     * @return - the attribute's value
     * @throws VogcException
     */
    protected String selectPulsarHasAttValue(String userName, String password, int plsHasAttId) throws VogcException {

        return this.selectPulsarHasAttribute(userName, password, plsHasAttId).getValue();
    }

    /**
     * Method to select the value of the boolean flag first in a
     * PulsarHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId
     * @return - the boolean flag's value
     * @throws VogcException
     */
    protected boolean selectPulsarHasAttFirst(String userName, String password, int plsHasAttId) throws VogcException {

        return this.selectPulsarHasAttribute(userName, password, plsHasAttId).isFirst();
    }

    /**
     * Method to select the date of insert in a PulsarHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsHasAttId the PulsarHasAttribute id
     * @return - date of insert
     * @throws VogcException
     */
    protected Date selectPulsarHasAttDate(String userName, String password, int plsHasAttId) throws VogcException {

        return (Date) this.selectPulsarHasAttribute(userName, password, plsHasAttId).getDate();
    }

    /**
     * Method used to select all the PulsarHasAttribute objects having the same
     * pulsar id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param pulsarId
     * @return - an array of PulsarHasAttribute objects
     * @throws VogcException
     */
    protected PulsarHasAttribute[] selectPlsHasAttByPulsarId(String userName, String password, String pulsarId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "pulsar_has_attribute", "*", "pulsarId", pulsarId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            PulsarHasAttribute attribute[] = new PulsarHasAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new PulsarHasAttribute(r.getInt("id"), r.getInt("plsAttributeId"), r.getString("pulsarId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the PulsarHasAttribute objects having the same
     * source id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param sourceId the source id
     * @return - an array of PulsarHasAttribute objects
     * @throws VogcException
     */
    protected PulsarHasAttribute[] selectPlsHasAttBySourceId(String userName, String password, String sourceId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "pulsar_has_attribute", "*", "sourceId", sourceId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            PulsarHasAttribute[] attribute = new PulsarHasAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (PulsarHasAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the PulsarHasAttribute objects having the same
     * plsAttribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the pulsar's attribute id
     * @return - an array of PulsarHasAttribute objects
     * @throws VogcException
     */
    protected PulsarHasAttribute[] selectPlsHasAttByAttributeId(String userName, String password, int attributeId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "pulsar_has_attribute", "*", "plsAttributeId", attributeId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            PulsarHasAttribute attribute[] = new PulsarHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new PulsarHasAttribute(r.getInt("id"), r.getInt("plsAttributeId"), r.getString("pulsarId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
