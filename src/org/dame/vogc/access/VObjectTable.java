package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class VObjectTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method used to insert a new tuple in the vobject table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param object the VObject to insert in the database
     * @throws org.dame.vogc.exceptions.VogcException
     */
    protected void insertVObject(String userName, String password, VObject object) throws VogcException {

        if (object.getId() == null || object.getType() < 0) {
            throw new VogcException(Messages.getError(Messages.VOBJECT_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into vobject values(?,?)");
            stm.setString(1, object.getId());
            stm.setInt(2, object.getType());
            stm.executeUpdate();
            //     if(stm.executeUpdate()== 0) throw new VogcException(Messages.getError(Messages.VOBJECT_NOT_EXISTS));

            //
        } catch (SQLException ex) {

            if (ex.getSQLState().equals("23000")) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_ALREADY_EXISTS));
            } else {
                throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
            }
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                Logger.getLogger(VObjectTable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Method used to delete a tuple in the VObject table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the object's id to be deleted
     * @throws org.dame.vogc.exceptions.VogcException
     */
    protected void deleteVObject(String userName, String password, String objectId) throws VogcException {
        try {
            if (objectId == null) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_PARAMETER_WRONG_OR_NULL));
            }

            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from vobject where id=?");
            stm.setString(1, objectId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select a VObject from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the object's id to be selected
     */
    protected VObject selectVObject(String userName, String password, String objectId) throws VogcException {

        try {
            if (objectId == null) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "vobject", "*", "id", objectId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_NOT_EXISTS));
            }
            VObject object = new VObject(objectId, r.getInt("type"));
            return object;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a list of objects of the same type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param type the object type to be selected
     * @return - a VObject array
     */
    protected VObject[] selectVObjectsByType(String userName, String password, int type) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "vobject", "*", "type", type);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_NOT_EXISTS));
            }
            r.last();
            VObject[] object = new VObject[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                object[i] = (VObject) r.getStatement();
            }
            return object;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the type of an object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the object id
     * @return - the object type
     */
    protected int selectVObjectType(String userName, String password, String objectId) throws VogcException {
        return this.selectVObject(userName, password, objectId).getType();
    }

    /**
     * Method used to update the object id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param oldId the object id to be updated
     * @param newId the new object id
     */
    protected void updateVObjectId(String userName, String password, String oldId, String newId) throws VogcException {
        try {
            if (oldId == null || newId == null) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "vobject", "id", newId, "id", oldId) == 0) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to update the object type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the object id to be updated
     * @param type the new type to be inserted
     */
    protected void updateVObjectType(String userName, String password, String objectId, int type) throws VogcException {
        try {
            if (objectId == null || type < -1) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "vobject", "type", type, "id", objectId) == 0) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * This method selects all the object that match with value request
     *
     * @param userName username for database access
     * @param password password for database access
     * @throws VogcException
     */
    protected String[] selectPartialResult(String userName, String password, String value) throws VogcException {

        try {
            if (value == null) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_HAS_TAG_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();

            PreparedStatement stm = con.prepareStatement("SELECT * FROM vobject where id like '%" + value + "%'");
            // stm.setString(1, value);
            ResultSet r = stm.executeQuery();

            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_NOT_EXISTS));
            }
            r.last();
            String[] gclusterId = new String[r.getRow()];
            r.beforeFirst();
            int i = -1;

            while (r.next()) {
                i++;
                gclusterId[i] = r.getString("id");
            }


            return gclusterId;


        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }


    }
}
