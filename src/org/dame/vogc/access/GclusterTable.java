package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.Gcluster;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class GclusterTable {

//    private static final int objectType = 1;
    private static ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method used to insert a new tuple in the gcluster table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param cluster the Gcluster object to insert in the database
     * @throws VogcException
     */
    protected void insertGcluster(String userName, String password, Gcluster cluster) throws VogcException {

        if (cluster.getClusterType() < 0 || cluster.getClusterId() == null) {
            throw new VogcException(Messages.getError(Messages.GCLUSTER_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into gcluster values(?,?,?)");
            stm.setString(1, cluster.getClusterId());
            stm.setString(2, cluster.getName());
            stm.setInt(3, cluster.getType());
            stm.executeUpdate();

            /*    if(stm.executeUpdate()== 0)
             throw new VogcException(Messages.getError(Messages.GCLUSTER_NOT_EXISTS));*/
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to delete a tuple from the gcluster table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterId the gcluster object's id to delete
     * @throws org.dame.vogc.exceptions.VogcException
     */
    protected void deleteGcluster(String userName, String password, String gclusterId) throws VogcException {
        try {
            if (gclusterId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from gcluster where id=?");
            stm.setString(1, gclusterId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select a gcluster object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterId the cluster's id to delete
     * @return
     * @throws org.dame.vogc.exceptions.VogcException
     */
    protected Gcluster selectGcluster(String userName, String password, String gclusterId) throws VogcException {
        try {
            if (gclusterId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "gcluster", "*", "objectId", gclusterId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_NOT_EXISTS));
            }
            Gcluster cluster = new Gcluster(gclusterId, r.getString("name"), r.getInt("type"));

            return cluster;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }

    }

    /**
     * Method to select a list of globular clusters of the same type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param type the cluster type to select
     * @return - a Gcluster array
     * @throws VogcException
     */
    protected Gcluster[] selectGclustersByType(String userName, String password, int type) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "gcluster", "*", "type", type);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_NOT_EXISTS));
            }
            r.last();
            Gcluster cluster[] = new Gcluster[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                cluster[i] = new Gcluster(r.getString("objectId"), r.getString("name"), r.getInt("type"));
            }
            return cluster;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select the type of a cluster from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param clusterId the cluster id
     * @return - the integer representing the cluster type
     * @throws VogcException
     */
    protected int selectGclusterType(String userName, String password, String clusterId) throws VogcException {

        return this.selectGcluster(userName, password, clusterId).getClusterType();

    }

    /**
     * Method used to select the name of a cluster from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param clusterId the cluster id
     * @return - a string contanining the cluster name
     * @throws VogcException
     */
    protected String selectGclusterName(String userName, String password, String clusterId) throws VogcException {

        return this.selectGcluster(userName, password, clusterId).getName();
    }

    /**
     * Method used to update a cluser id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param oldId the cluster id to update
     * @param newId the new cluster id
     * @throws VogcException
     */
    protected void updateGclusterId(String userName, String password, String oldId, String newId) throws VogcException {
        try {
            if (oldId == null || newId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gcluster", "objectId", newId, "objectId", oldId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to update a cluser name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param clusterId the cluster id to update
     * @param name the new cluster name
     * @throws VogcException
     */
    protected void updateGclusterName(String userName, String password, String clusterId, String name) throws VogcException {
        try {
            if (clusterId == null || name == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gcluster", "name", name, "objectId", clusterId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to update a cluster type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param clusterId the cluster id to update
     * @param type the new cluster type
     * @throws VogcException
     */
    protected void updateGclusterType(String userName, String password, String clusterId, int type) throws VogcException {
        try {
            if (clusterId == null || type < 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gcluster", "type", type, "objectId", clusterId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    protected String[] selectClusterParameterValue(String userName, String password, int aInt, String value, String operator) throws VogcException {

        try {
            if (aInt == 999999999) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            double temp = Double.parseDouble(value);
            Connection con = connection.connect();
            PreparedStatement stm = null;
            if (operator.equals("uguale")) {
                stm = con.prepareStatement("SELECT * FROM gcluster_has_attribute where gcAttributeId = ? and value = ?");
            }
            if (operator.equals("minore")) {
                stm = con.prepareStatement("SELECT * FROM gcluster_has_attribute where gcAttributeId = ? and value < ?");
            }
            if (operator.equals("maggiore")) {
                stm = con.prepareStatement("SELECT * FROM gcluster_has_attribute where gcAttributeId = ? and value > ?");
            }

            stm.setInt(1, aInt);
            stm.setDouble(2, temp);

            //  PreparedStatement stm = con.prepareStatement("select id from vobject where id like '%"+aString+"%'");
            //PreparedStatement stm = con.prepareStatement("select id from tag where name = ?");// like '%"+aString+"%'");
            //stm.setString(1, aString);
            //  PreparedStatement stm = con.prepareStatement(" select * from vobject");
            ResultSet r = stm.executeQuery();

            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            String[] gclusterId = new String[r.getRow()];
            r.beforeFirst();
            int i = -1;

            while (r.next()) {
                i++;
                gclusterId[i] = r.getString("gclusterId");
            }

            return gclusterId;

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }
}
