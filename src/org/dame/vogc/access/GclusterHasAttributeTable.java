package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class GclusterHasAttributeTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new GclusterHasAttribute object in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param att the GclusterHasAttribute object to insert
     * @throws VogcException
     * @throws java.sql.SQLException
     */
    protected void insertGclusterHasAttribute(String userName, String password, GclusterHasAttribute att) throws VogcException, SQLException {

        PreparedStatement stm = null;

        if (att.getGcAttId() < 0 || att.getGclusterId() == null || att.getSourceId() == null || att.getValue() == null) {
            throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            stm = con.prepareStatement("insert into gcluster_has_attribute values(?,?,?,?,?,?,?)");
            stm.setInt(1, att.getId());
            stm.setString(2, att.getGclusterId());
            stm.setInt(3, att.getGcAttId());
            stm.setString(4, att.getSourceId());
            stm.setString(5, att.getValue());
            stm.setBoolean(6, att.isFirst());
            stm.setString(7, date.toString());

            stm.executeUpdate();
            //try{
            // stm.executeUpdate();
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS));

            //  }
            //  if(stm.executeUpdate()== 0) throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS));
            /*    } catch (SQLException ex) {
             throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));*/
        } finally {
            try {
                stm.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a GclusterHasAttribute object from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the attribute id to delete
     * @throws VogcException
     */
    protected void deleteGclusterHasAttribute(String userName, String password, int gclusterHasAttId) throws VogcException {

        PreparedStatement stm = null;
        try {
            if (gclusterHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            stm = con.prepareStatement("delete from gcluster_has_attribute where id=?");
            stm.setInt(1, gclusterHasAttId);
            System.out.println(stm.toString());
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                stm.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a GclusterHasAttribute object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the attribute id to select
     * @return - a GclusterHasAttribute object
     * @throws VogcException
     */
    protected GclusterHasAttribute selectGclusterHasAttribute(String userName, String password, int gclusterHasAttId) throws VogcException {
        ResultSet r = null;
        try {
            if (gclusterHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }

            r = DbStatements.query(connection, userName, password, "gcluster_has_attribute", "*", "id", gclusterHasAttId);

            //SELECT sourceId from vogclusters.gcluster_has_attribute where gclusterId = "12345"
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            GclusterHasAttribute att = new GclusterHasAttribute(gclusterHasAttId, r.getString("gclusterId"), r.getInt("gcAttributeId"), r.getString("sourceId"), r.getString("value"), r.getBoolean("first"), r.getDate("date"));
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }
//
    //
    //  NUOVA QUERY LUCA
    //
    //

    protected GclusterHasAttribute selectGclusterSourceId(String userName, String password, String objectId) throws VogcException {
        ResultSet r = null;
        try {
            if (objectId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }

            r = DbStatements.query2(connection, userName, password, "gcluster_has_attribute", "sourceId", "gclusterId", objectId);

            //SELECT sourceId from vogclusters.gcluster_has_attribute where gclusterId = "12345"
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            GclusterHasAttribute att = new GclusterHasAttribute();
            att.setSourceId(r.getString("sourceId"));
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to update a globular cluster id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the GclusterHasAttribute id
     * @param gclusterId the new gcluster id
     * @throws VogcException
     */
    protected void updateGclusterHasAttGclusterId(String userName, String password, int gclusterHasAttId, String gclusterId) throws VogcException {
        try {
            if (gclusterHasAttId < 0 || gclusterId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "gclusterId", gclusterId, "id", gclusterHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the globular cluster's attribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the gclusterHasAttribute id
     * @param gclusterAttId the new globular cluster's attribute id
     * @throws VogcException
     */
    protected void updateGclusterHasAttGcAttId(String userName, String password, int gclusterHasAttId, int gclusterAttId) throws VogcException {
        try {
            if (gclusterAttId < 0 || gclusterHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "gcAttributeId", gclusterAttId, "id", gclusterHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the source id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the GclusterHasAttribute object id
     * @param sourceId the new source id
     * @throws VogcException
     */
    protected void updateGclusterHasAttSourceId(String userName, String password, int gclusterHasAttId, String sourceId) throws VogcException {
        try {
            if (gclusterHasAttId < 0 || sourceId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "sourceId", sourceId, "id", gclusterHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value in a GclusterHasAttrbute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param GcclusterId
     * @param Gcclusterattribute
     * @param value the new value to insert
     * @throws VogcException
     */
// protected void updateGclusterHasAttValue(String userName, String password, int gclusterHasAttId, String value)throws VogcException{
//       try{
//          if (gclusterHasAttId  < 0 || value == null)
//                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
//          if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "value", value, "id", gclusterHasAttId) == 0)
//              throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
//       } catch (SQLException ex) {
//            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
//        }
//    }
    protected void updateGclusterHasAttValue(String userName, String password, String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        try {
            if (Gcclusterattribute == null || GcclusterId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            //DbStatements.update(connection, userName, password, "gcluster_has_attribute", "gclusterId", gclusterId, "id", gclusterHasAttId) == 0)
            if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", value, "gclusterId", GcclusterId, "gcAttributeId", "gc_attribute", Gcclusterattribute) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag first in a
     * GclusterHasAttrbute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the GclusterHasAttribute id
     * @param first the new boolean value to insert
     * @throws VogcException
     */
    protected void updateGclusterHasAttFirst(String userName, String password, int gclusterHasAttId, boolean first) throws VogcException {
        try {
            if (gclusterHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "first", first, "id", gclusterHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of date of insert in a GclusterHasAttrbute
     * table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param value
     * @param GcclusterId
     * @param Gcclusterattribute
     * @throws VogcException
     */
// protected void updateGclusterHasAttDate(String userName, String password, int gclusterHasAttId)throws VogcException{
//       try{
//          if (gclusterHasAttId  < 0)
//                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
//          Calendar cal = Calendar.getInstance();
//          Date date = new Date(cal.getTimeInMillis());
//          if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "date", date, "id", gclusterHasAttId) == 0)
//              throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
//       } catch (SQLException ex) {
//            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
//        }
//    }
    protected void updateGclusterHasAttDate(String userName, String password, String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        try {
            if (Gcclusterattribute == null || GcclusterId == null) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            // if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "date", date, "id", gclusterHasAttId) == 0)
            if (DbStatements.update(connection, userName, password, "gcluster_has_attribute", "gclusterId", GcclusterId, "gcAttributeId", "gc_attribute", Gcclusterattribute) == 0) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the globular cluster id of a GclusterHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the GclusterHasAttribute id
     * @return - the pulsar id
     * @throws VogcException
     */
    protected String selectGclusterHasAttGclusterId(String userName, String password, int gclusterHasAttId) throws VogcException {
        return this.selectGclusterHasAttribute(userName, password, gclusterHasAttId).getGclusterId();
    }

    /**
     * Method to select the globular cluster's attribute id in a
     * GclusterHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the GclusterHasAttribute id
     * @return - the globular cluster's attribute id
     * @throws VogcException
     */
    protected int selectGclusterHasAttGcAttId(String userName, String password, int gclusterHasAttId) throws VogcException {

        return this.selectGclusterHasAttribute(userName, password, gclusterHasAttId).getGcAttId();
    }

    /**
     * Method to select the source id in a GclusterHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the GclusterHasAttribute id
     * @return - the source id
     * @throws VogcException
     */
    protected String selectGclusterHasAttSourceId(String userName, String password, int gclusterHasAttId) throws VogcException {
        return this.selectGclusterHasAttribute(userName, password, gclusterHasAttId).getSourceId();

    }

    /**
     * Method to select an attribute's value in a GclusterHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterHasAttId the GclusterHasAttribute id
     * @return - the attribute's value
     * @throws VogcException
     */
    protected String selectGclusterHasAttValue(String userName, String password, int gclusterHasAttId) throws VogcException {

        return this.selectGclusterHasAttribute(userName, password, gclusterHasAttId).getValue();
    }

    /**
     * Method to select the value of the boolean flag first in a
     * GclusterHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param GclusterHasAttId the GclusterHasAttribute id
     * @return - the boolean flag's value
     * @throws VogcException
     */
    protected boolean selectGclusterHasAttFirst(String userName, String password, int GclusterHasAttId) throws VogcException {

        return this.selectGclusterHasAttribute(userName, password, GclusterHasAttId).isFirst();
    }

    /**
     * Method to select the date of insert in a GclusterHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param GclusterHasAttId the GclusterHasAttribute id
     * @return - date of insert
     * @throws VogcException
     */
    protected Date selectGclusterHasAttDate(String userName, String password, int GclusterHasAttId) throws VogcException {

        return (Date) this.selectGclusterHasAttribute(userName, password, GclusterHasAttId).getDate();
    }

    /**
     * Method used to select all the GclusterHasAttribute objects having the
     * same globular cluster id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gclusterId the globular cluster id
     * @return - an array of GclusterHasAttribute objects
     * @throws VogcException
     */
    protected GclusterHasAttribute[] selectGcHasAttByGclusterId(String userName, String password, String gclusterId) throws VogcException {

        ResultSet r = null;

        try {
            r = DbStatements.query(connection, userName, password, "gcluster_has_attribute", "*", "gclusterId", gclusterId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            GclusterHasAttribute attribute[] = new GclusterHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new GclusterHasAttribute(r.getInt("id"), r.getString("gclusterId"), r.getInt("gcAttributeId"), r.getString("sourceId"), r.getString("value"), r.getBoolean("first"), r.getDate("date"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a globular cluster attribute by Parameter Search
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - a GcAttribute object
     * @throws VogcException
     */
    protected GclusterHasAttribute[] selectParameter(String userName, String password) throws VogcException {
        ResultSet r = null;

        try {
            r = DbStatements.parameterQuery(connection, userName, password);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            GclusterHasAttribute attribute[] = new GclusterHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new GclusterHasAttribute(r.getInt("id"), r.getString("gclusterId"), r.getInt("gcAttributeId"), r.getString("sourceId"), r.getString("value"), r.getBoolean("first"), r.getDate("date"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected GclusterHasAttribute[] selectPhotometricParameter(String userName, String password) throws VogcException {
        ResultSet r = null;

        try {
            r = DbStatements.photometricQuery(connection, userName, password);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            GclusterHasAttribute attribute[] = new GclusterHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new GclusterHasAttribute(r.getInt("id"), r.getString("gclusterId"), r.getInt("gcAttributeId"), r.getString("sourceId"), r.getString("value"), r.getBoolean("first"), r.getDate("date"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected GclusterHasAttribute[] selectStructuralParameter(String userName, String password) throws VogcException {
        ResultSet r = null;

        try {
            r = DbStatements.structuralQuery(connection, userName, password);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            GclusterHasAttribute attribute[] = new GclusterHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new GclusterHasAttribute(r.getInt("id"), r.getString("gclusterId"), r.getInt("gcAttributeId"), r.getString("sourceId"), r.getString("value"), r.getBoolean("first"), r.getDate("date"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected GclusterHasAttribute[] selectGcHasAttByGclusterIdandObjectId(String userName, String password, String objectId, int attId) throws VogcException {

        ResultSet r = null;

        try {
            r = DbStatements.queryHistory(connection, userName, password, objectId, attId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            GclusterHasAttribute attribute[] = new GclusterHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new GclusterHasAttribute(r.getInt("id"), r.getString("gclusterId"), r.getInt("gcAttributeId"), r.getString("sourceId"), r.getString("value"), r.getBoolean("first"), r.getDate("date"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select all the GclusterHasAttribute objects having the
     * same gcAttribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the globular cluster's attribute id
     * @return - an array of GclusterHasAttribute objects
     * @throws VogcException
     */
    protected GclusterHasAttribute[] selectGcHasAttByAttributeId(String userName, String password, int attributeId) throws VogcException {

        ResultSet r = null;
        try {
            r = DbStatements.query(connection, userName, password, "gcluster_has_attribute", "*", "gcAttributeId", attributeId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            GclusterHasAttribute attribute[] = new GclusterHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new GclusterHasAttribute(r.getInt("id"), r.getString("gclusterId"), r.getInt("gcAttributeId"), r.getString("sourceId"), r.getString("value"), r.getBoolean("first"), r.getDate("date"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select all the GclusterHasAttribute objects having the
     * same source id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param sourceId the source id
     * @return - an array of GclusterHasAttribute objects
     * @throws VogcException
     */
    protected GclusterHasAttribute[] selectGcHasAttBySourceId(String userName, String password, String sourceId) throws VogcException {

        ResultSet r = null;

        try {
            r = DbStatements.query(connection, userName, password, "gcluster_has_attribute", "*", "sourceId", sourceId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            GclusterHasAttribute[] attribute = new GclusterHasAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (GclusterHasAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select the number of value inserted for an attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attId the attribute id
     * @return - the number of rows
     * @throws VogcException
     */
    protected int selectGcHasAttNumber(String userName, String password, int attId) throws VogcException {

        PreparedStatement stm = null;
        ResultSet r = null;

        if (attId < 0) {
            throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();

            stm = con.prepareStatement("SELECT count(*) FROM gcluster_has_attribute where gcAttributeId = ?");
            stm.setInt(1, attId);
            r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            int val = r.getInt("count(*)");
            return val;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                stm.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }
}
