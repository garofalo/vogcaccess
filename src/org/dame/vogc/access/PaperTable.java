package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.Paper;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class PaperTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new paper in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paper the paper object to insert
     * @return
     * @throws VogcException
     * @throws java.sql.SQLException
     */
    protected int insertPaper(String userName, String password, Paper paper) throws VogcException, SQLException {
        if (paper.getId() < 0 || paper.getName() == null || paper.getUri() == null) {
            throw new VogcException(Messages.getError(Messages.PAPER_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into paper values(?,?,?)");
            stm.setInt(1, paper.getId());
            stm.setString(2, paper.getName());
            stm.setString(3, paper.getUri());
            try {
                stm.executeUpdate();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.PAPER_INSERT_FAILED));
            }
            //if(stm.executeUpdate()== 0) throw new VogcException(Messages.getError(Messages.PAPER_INSERT_FAILED));
            stm = con.prepareStatement("select @@Identity as lastId");
            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.LAST_ID_NOT_RETRIEVED));
            }

            return r.getInt("lastId");
            //      } catch (SQLException ex) {
            //        throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a paper from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the paper id to delete
     * @throws VogcException
     */
    protected void deletePaper(String userName, String password, int paperId) throws VogcException {
        try {
            if (paperId < 0) {
                throw new VogcException(Messages.getError(Messages.PAPER_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from paper where id=?");
            stm.setInt(1, paperId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.PAPER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a paper from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the paper id to select
     * @return - a Paper object
     * @throws VogcException
     */
    protected Paper selectPaper(String userName, String password, int paperId) throws VogcException {
        try {
            if (paperId < 0) {
                throw new VogcException(Messages.getError(Messages.PAPER_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "paper", "*", "id", paperId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PAPER_NOT_EXISTS));
            }
            Paper paper = new Paper(paperId, r.getString("name"), r.getString("uri"));
            return paper;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the paper's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the paper id
     * @return - the paper's name
     * @throws VogcException
     */
    protected String selectPaperName(String userName, String password, int paperId) throws VogcException {

        return this.selectPaper(userName, password, paperId).getName();
    }

    /**
     * Method to select the author's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the paper id
     * @return - the paper's uri
     * @throws VogcException
     */
    protected String selectPaperUri(String userName, String password, int paperId) throws VogcException {

        return this.selectPaper(userName, password, paperId).getUri();
    }

    /**
     * Method to update the paper's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the paper's id
     * @param name the new paper's name
     * @throws VogcException
     */
    protected void updatePaperName(String userName, String password, int paperId, String name) throws VogcException {
        try {
            if (paperId < 0 || name == null) {
                throw new VogcException(Messages.getError(Messages.PAPER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "paper", "name", name, "id", paperId) == 0) {
                throw new VogcException(Messages.getError(Messages.PAPER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the paper's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the paper's id
     * @param uri the new paper's uri
     * @throws VogcException
     */
    protected void updatePaperUri(String userName, String password, int paperId, String uri) throws VogcException {
        try {
            if (paperId < 0 || uri == null) {
                throw new VogcException(Messages.getError(Messages.PAPER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "paper", "uri", uri, "id", paperId) == 0) {
                throw new VogcException(Messages.getError(Messages.PAPER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the last key inserted in the DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - the last key of the table
     * @throws VogcException
     */
    protected int selectLastPaper(String userName, String password) throws VogcException {

        try {
            Connection con = connection.connect();

            PreparedStatement stm = con.prepareStatement("SELECT MAX(id) FROM paper;");

            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PAPER_NOT_EXISTS));
            }
            int val = r.getInt("MAX(id)");
            return val;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected Paper[] selectAllPapers(String userName, String password) throws VogcException {

        try {
            ResultSet r = DbStatements.query(connection, userName, password, "paper", "*");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_NOT_EXISTS));
            }
            r.last();
            Paper papers[] = new Paper[r.getRow()];

            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                papers[i] = new Paper(r.getInt("id"), r.getString("name"), r.getString("uri"));
            }
            return papers;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
