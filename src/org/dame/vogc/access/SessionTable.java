package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.dame.vogc.datatypes.Session;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class SessionTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to save a new session in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id which starts the new session
     * @throws VogcException
     */
    protected void insertSession(String userName, String password, String userId) throws VogcException {
        if (userId == null) {
            throw new VogcException(Messages.getError(Messages.SESSION_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("Select max(id) from sessions");
            ResultSet r = stm.executeQuery();
            int id = 1;
            if (!r.next()) {
                id = r.getInt(1) + 1;
            }
            stm = con.prepareStatement("insert into session values(?,?,?,?)");
            stm.setInt(1, id);
            stm.setString(2, userId);
            stm.setString(3, date.toString());
            stm.setString(4, date.toString());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.SESSION_INSERT_FAILED));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a session from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ssid the session id to delete
     * @throws VogcException
     */
    protected void deleteSession(String userName, String password, int ssid) throws VogcException {
        try {
            if (ssid < 0) {
                throw new VogcException(Messages.getError(Messages.SESSION_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from session where id=?");
            stm.setInt(1, ssid);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.SESSION_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a session from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ssid
     * @return - a Session object
     * @throws VogcException
     */
    protected Session selectSession(String userName, String password, int ssid) throws VogcException {
        try {
            if (ssid < 0) {
                throw new VogcException(Messages.getError(Messages.SESSION_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "session", "*", "id", ssid);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.SESSION_NOT_EXISTS));
            }
            Session session = new Session(ssid, r.getString("userId"), r.getDate("creationDate"), r.getDate("lastAccessDate"));
            return session;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the sessiom's user id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ssid the session id
     * @return - the user id
     * @throws VogcException
     */
    protected String selectSessionUserId(String userName, String password, int ssid) throws VogcException {

        return this.selectSession(userName, password, ssid).getUserId();
    }

    /**
     * Method to select the sessiom's creation date
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ssid the session id
     * @return - the creation date
     * @throws VogcException
     */
    protected Date selectSessionCreationDate(String userName, String password, int ssid) throws VogcException {

        return (Date) this.selectSession(userName, password, ssid).getCreationDate();
    }

    /**
     * Method to select the sessiom's last access date
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ssid the session id
     * @return - the creation date
     * @throws VogcException
     */
    protected Date selectSessionLastAccessDate(String userName, String password, int ssid) throws VogcException {

        return (Date) this.selectSession(userName, password, ssid).getLastAccessDate();
    }

    /**
     * Method to update the session's last access date
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id
     * @throws VogcException
     */
    protected void updateSessionLastAccessDate(String userName, String password, int id) throws VogcException {
        Calendar cal = Calendar.getInstance();
        Date date = new Date(cal.getTimeInMillis());
        try {
            if (id == 0) {
                throw new VogcException(Messages.getError(Messages.SESSION_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "session", "lastAccessDate", date.toString(), "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.SESSION_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to retrieve all the session of a single user
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - an array of session object containing all the session of a
     * single user
     * @throws VogcException
     */
    protected Session[] selectUserSessions(String userName, String password, String userId) throws VogcException {
        try {
            if (userId == null) {
                throw new VogcException(Messages.getError(Messages.SESSION_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "session", "*", "userId", userId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.SESSION_NOT_EXISTS));
            }
            r.last();
            Session session[] = new Session[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                session[i] = new Session(r.getInt("ssid"), r.getString("userId"), r.getDate("creationDate"), r.getDate("lastAccessDate"));
            }
            return session;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to retrieve all the session id of a single user
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - an integer array contaning all the session id of a single user
     * @throws VogcException
     */
    protected int[] getUserSessionsId(String userName, String password, String userId) throws VogcException {
        if (userId == null) {
            throw new VogcException(Messages.getError(Messages.SESSION_PARAMETER_WRONG_OR_NULL));
        }
        int length = this.selectUserSessions(userName, password, userId).length;
        int[] id = new int[length];
        for (int i = 0; i < length; i++) {

            id[i] = this.selectUserSessions(userName, password, userId)[i].getSsid();

        }
        return id;
    }
}
