package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dame.vogc.datatypes.Notes;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class NotesTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new note in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param object the Notes object to be inserted
     * @throws VogcException
     */
    protected void insertNotes(String userName, String password, Notes object) throws VogcException {
        if (object.getTitle() == null || object.getNoteType() < 0 || object.getDescription() == null) {
            throw new VogcException(Messages.getError(Messages.NOTES_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into notes values(?,?,?,?)");
            stm.setInt(1, object.getId());
            stm.setString(2, object.getTitle());
            stm.setString(3, object.getDescription());
            stm.setInt(4, object.getNoteType());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.NOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a Note object
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the Notes object id to be deleted
     * @throws VogcException
     */
    protected void deleteNotes(String userName, String password, int objectId) throws VogcException {
        try {
            if (objectId < 0) {
                throw new VogcException(Messages.getError(Messages.NOTES_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from notes where id=?");
            stm.setInt(1, objectId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.NOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the notes of the same type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param type an integer representing the type of note
     * @return - an array of Notes objects
     * @throws VogcException
     */
    protected Notes[] selectNotesByType(String userName, String password, int type) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "notes", "*", "type", type);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.NOTES_NOT_EXISTS));
            }
            r.last();
            Notes[] notes = new Notes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = (Notes) r.getStatement();
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the note's title
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @param title a string contaning the note's title
     * @throws VogcException
     */
    protected void updateNotesTitle(String userName, String password, int id, String title) throws VogcException {
        try {
            if (id < 0 || title == null) {
                throw new VogcException(Messages.getError(Messages.NOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "notes", "title", title, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.NOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                Logger.getLogger(NotesTable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Method to update the note's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @param description
     * @throws VogcException
     */
    protected void updateNotesDescription(String userName, String password, int id, String description) throws VogcException {
        try {
            if (id < 0 || description == null) {
                throw new VogcException(Messages.getError(Messages.NOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "notes", "description", description, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.NOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the note's type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @param type
     * @throws VogcException
     */
    protected void updateNotesType(String userName, String password, int id, int type) throws VogcException {
        try {
            if (id < 0 || type < 1 || type > 2) {
                throw new VogcException(Messages.getError(Messages.NOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "notes", "type", type, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.NOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a BiblioNotes object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - a BiblioNotes object
     * @throws VogcException
     */
    protected Notes selectNotes(String userName, String password, int id) throws VogcException {
        try {
            if (id < 0) {
                throw new VogcException(Messages.getError(Messages.NOTES_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("SELECT bn.objectId, bn.gcHasAttributeId, bn.starHasAttributeId, bn.plsHasAttributeId, bn.userId, bn.imageId, bn.date, n.title, n.description, n.type "
                    + "FROM biblio_notes bn, notes n "
                    + "WHERE bn.id = n.id and n.id =?");
            stm.setInt(1, id);
            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.NOTES_NOT_EXISTS));
            }

            Notes object = new Notes(id, r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("plsHasAttributeId"), r.getInt("starHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getString("title"), r.getString("description"), r.getInt("type"));

            return object;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                Logger.getLogger(NotesTable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Method to select the note's title
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the note's title
     * @throws VogcException
     */
    protected String selectNotesTitle(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getTitle();
    }

    /**
     * Method to select the note's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the note's description
     * @throws VogcException
     */
    protected String selectNotesDescription(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getDescription();
    }

    /**
     * Method to select the type of note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - integer representing the type of note
     * @throws VogcException
     */
    protected int selectNotesType(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getNoteType();
    }

    /**
     * Method to select the id of the object that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @return - the object id
     * @throws VogcException
     */
    protected String selectNotesObjectId(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getObjectId();
    }

    /**
     * Method to select the globular cluster's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @return - the globular cluster's attribute id
     * @throws VogcException
     */
    protected int selectNotesGcAttributeId(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getGcAttributeId();
    }

    /**
     * Method to select the star's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @return - the star's attribute id
     * @throws VogcException
     */
    protected int selectNotesStarAttributeId(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getStarAttributeId();
    }

    /**
     * Method to select the pulsar's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @return - the pulsar's attribute id
     * @throws VogcException
     */
    protected int selectNotesPulsarAttributeId(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getPlsAttributeId();
    }

    /**
     * Method to select the user id which has inserted the notes
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @return - the user id
     * @throws VogcException
     */
    protected String selectNotesUserId(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getUserId();
    }

    /**
     * Method to select the image's id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the Notes id to select
     * @return - the image id
     * @throws VogcException
     */
    protected int selectNotesImageId(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getImageId();
    }

    /**
     * Method to select the date when the notes were inserted in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - a date
     * @throws VogcException
     */
    protected Date selectNotesDate(String userName, String password, int id) throws VogcException {
        return this.selectNotes(userName, password, id).getDate();
    }
}
