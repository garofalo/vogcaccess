package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author luca
 */
public class DbStatements {

    protected static ResultSet query(ConnectionManager connection, String userName, String password,
            String table, String field, String id, int IdValue) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("Select " + field + " from " + table + " where " + id + "=?");

            stm.setInt(1, IdValue);
            r = stm.executeQuery();
        } catch (SQLException ex) {
            try {
                con.close();
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return r;

    }

    protected static ResultSet query(ConnectionManager connection, String userName, String password,
            String table, String field, String id, String IdValue) throws SQLException {

        PreparedStatement stm;
        ResultSet r = null;

        Connection con = connection.connect();
        try {
            stm = con.prepareStatement("Select " + field
                    + " from " + table + " where " + id + "=?");

            stm.setString(1, IdValue);
            r = stm.executeQuery();

        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return r;

    }

    protected static ResultSet query(ConnectionManager connection, String userName, String password,
            String table, String field, String id, String IdValue, String field2, int field2Value) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("Select " + field + " from " + table + " where " + id + "=? and " + field2 + "=?");
            stm.setString(1, IdValue);
            stm.setInt(2, field2Value);
            r = stm.executeQuery();
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return r;

    }

    protected static ResultSet parameterQuery(ConnectionManager connection, String userName, String password) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT * FROM gcluster_has_attribute where gcAttributeId = 1 || gcAttributeId=2 || gcAttributeId=3"
                    + "|| gcAttributeId=4 || gcAttributeId=10 || gcAttributeId=11 || gcAttributeId=12"
                    + "|| gcAttributeId=13 || gcAttributeId=14");

            r = stm.executeQuery();
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return r;

    }

    protected static ResultSet photometricQuery(ConnectionManager connection, String userName, String password) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT * FROM gcluster_has_attribute where gcAttributeId = 15 || gcAttributeId=16 || gcAttributeId=17"
                    + "|| gcAttributeId=18 || gcAttributeId=19 || gcAttributeId=20 || gcAttributeId=21"
                    + "|| gcAttributeId=22 || gcAttributeId=23 || gcAttributeId=24 || gcAttributeId=25"
                    + "|| gcAttributeId=5 || gcAttributeId=6");

            r = stm.executeQuery();
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return r;

    }

    protected static ResultSet structuralQuery(ConnectionManager connection, String userName, String password) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("SELECT * FROM gcluster_has_attribute where gcAttributeId = 7 || gcAttributeId=8 || gcAttributeId=9"
                    + "|| gcAttributeId=26 || gcAttributeId=27 || gcAttributeId=28 || gcAttributeId=29"
                    + "|| gcAttributeId=30 || gcAttributeId=31 || gcAttributeId=32 || gcAttributeId=33"
                    + "|| gcAttributeId=34 || gcAttributeId=35 || gcAttributeId=36|| gcAttributeId=37 ");

            r = stm.executeQuery();
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return r;

    }

    protected static ResultSet queryHistory(ConnectionManager connection, String userName, String password, String field, int id) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("Select * from gcluster_has_attribute where "
                    + "gclusterId =  ? and gcAttributeId = ? Order by DATE desc limit 1");

            stm.setString(1, field);
            stm.setInt(2, id);
            r = stm.executeQuery();

        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return r;

    }

    protected static ResultSet queryHistoryPulsar(ConnectionManager connection, String userName, String password, String field, int id) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("Select * from pulsar_has_attribute where "
                    + "pulsarId =  ? and plsAttributeId = ? Order by DATE desc limit 1");

            stm.setString(1, field);
            stm.setInt(2, id);
            r = stm.executeQuery();

        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return r;

    }

    protected static ResultSet queryHistoryStar(ConnectionManager connection, String userName, String password, String field, int id) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("Select * from star_has_attribute where "
                    + "starId =  ? and starAttributeId = ? Order by DATE desc limit 1");

            stm.setString(1, field);
            stm.setInt(2, id);
            r = stm.executeQuery();

        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return r;

    }

    protected static ResultSet query(ConnectionManager connection, String userName, String password,
            String table, String field) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();

        try {
            PreparedStatement stm = con.prepareStatement("Select " + field + " from " + table);

            r = stm.executeQuery();

        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return r;

    }

    //NUOVA QUERY DI LUCA
    protected static ResultSet query2(ConnectionManager connection, String userName, String password,
            String table, String field, String id, String IdValue) throws SQLException {

        ResultSet r = null;

        Connection con = connection.connect();
        try {
            PreparedStatement stm = con.prepareStatement("Select " + field
                    + " from " + table + " where " + id + "=? limit 1");

            stm.setString(1, IdValue);
            r = stm.executeQuery();

        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return r;

    }

    protected static int update(ConnectionManager connection, String userName, String password,
            String table, String field, String fieldValue, String id, String IdValue) throws SQLException {

        PreparedStatement stm = null;
        Connection con = connection.connect();

        try {
            stm = con.prepareStatement("update " + table
                    + " set " + field + "=? where " + id + "=?");
            stm.setString(1, fieldValue);
            stm.setString(2, IdValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();
    }

    protected static int update(ConnectionManager connection, String userName, String password,
            String table, String field, int fieldValue, String id, String IdValue) throws SQLException {

        PreparedStatement stm = null;
        Connection con = connection.connect();

        try {

            stm = con.prepareStatement("update " + table + " set " + field + "=? where " + id + "=?");
            stm.setInt(1, fieldValue);
            stm.setString(2, IdValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();

    }

    protected static int update(ConnectionManager connection, String userName, String password,
            String table, String field, String fieldValue, String id, int IdValue) throws SQLException {

        Connection con = connection.connect();
        PreparedStatement stm = null;
        try {
            stm = con.prepareStatement("update " + table + " set " + field + "=? where " + id + "=?");
            stm.setString(1, fieldValue);
            stm.setInt(2, IdValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();

    }

    static int update(ConnectionManager connection, String userName, String password,
            String table, String field, int fieldValue, String id, int idValue) throws SQLException {
        PreparedStatement stm = null;
        Connection con = connection.connect();

        try {
            stm = con.prepareStatement("update " + table + " set " + field + "=? where " + id + "=?");
            stm.setInt(1, fieldValue);
            stm.setInt(2, idValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();
    }

    static int update(ConnectionManager connection, String userName, String password,
            String table, String field, boolean fieldValue, String id, int idValue) throws SQLException {

        Connection con = connection.connect();
        PreparedStatement stm = null;
        try {
            stm = con.prepareStatement("update " + table + " set " + field + "=? where " + id + "=?");
            stm.setBoolean(1, fieldValue);
            stm.setInt(2, idValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();
    }

    static int update(ConnectionManager connection, String userName, String password, String table, String field, java.util.Date fieldValue, String id, int idValue) throws SQLException {
        Connection con = connection.connect();

        PreparedStatement stm = null;
        try {
            stm = con.prepareStatement("update " + table + " set " + field + "=? where " + id + "=?");
            stm.setDate(1, (Date) fieldValue);
            stm.setInt(2, idValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();
    }

    static int update(ConnectionManager connection, String userName, String password, String table, String field, Boolean fieldValue, String id, String idValue) throws SQLException {
        Connection con = connection.connect();
        PreparedStatement stm = null;
        try {
            stm = con.prepareStatement("update " + table + " set " + field + "=? where " + id + "=?");
            stm.setBoolean(1, fieldValue);
            stm.setString(2, idValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();
    }

    static ResultSet query(ConnectionManager connection, String userName, String password, String table, String field, String field2, boolean field2Value) throws SQLException {

        PreparedStatement stm;
        ResultSet r = null;
        Connection con = connection.connect();
        try {
            stm = con.prepareStatement("Select " + field + " from " + table + " where" + field2 + "=?");
            stm.setBoolean(1, field2Value);
            r = stm.executeQuery();
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return r;
    }

    static int update(ConnectionManager connection, String userName, String password, String table, String field, java.util.Date fieldValue, String id, String idValue) throws SQLException {

        PreparedStatement stm = null;
        Connection con = connection.connect();
        try {
            stm = con.prepareStatement("update " + table + " set " + field + "=? where " + id + "=?");
            stm.setString(1, fieldValue.toString());
            stm.setString(2, idValue);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();
    }

    static int update(ConnectionManager connection, String userName, String password,
            String table, String value, String field, String fieldvalue, String field2, String field3, String field3value) throws SQLException {

        PreparedStatement stm = null;
        Connection con = connection.connect();
        try {
            stm = con.prepareStatement("update " + table + " set value = " + value + " where (" + field + " = ? ) and " + field2 + " = ( select id from " + field3 + " where name = ? )");
//        stm.setString(1,field);
//        stm.setString(2,value);
//        stm.setString(3,fieldvalue);
//        stm.setString(4,field2);
//        stm.setString(3,fieldvalue);
//        stm.setString(6,field3value);
            stm.setString(1, fieldvalue);
            stm.setString(2, field3value);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return stm.executeUpdate();
    }

    static int update(ConnectionManager connection, String userName, String password,
            String table, String field, String fieldvalue, String field2, String field3, String field3value) throws SQLException {

        PreparedStatement stm = null;
        Connection con = connection.connect();
        try {
            stm = con.prepareStatement("update " + table + " set date = curdate() where (" + field + " = ? ) and " + field2 + " = ( select id from " + field3 + " where name = ? )");

//        stm.setString(1,field);
//        stm.setString(2,value);
//        stm.setString(3,fieldvalue);
//        stm.setString(4,field2);
//        stm.setString(3,fieldvalue);
//        stm.setString(6,field3value);
            stm.setString(1, fieldvalue);
            stm.setString(2, field3value);
        } catch (SQLException ex) {
            try {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } catch (VogcException ex1) {
                Logger.getLogger(DbStatements.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return stm.executeUpdate();
    }
}
