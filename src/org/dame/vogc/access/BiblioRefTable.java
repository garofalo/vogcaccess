package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.BiblioRef;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class BiblioRefTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new note in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param object the BiblioRef object to be inserted
     * @throws VogcException
     */
    protected void insertBiblioRef(String userName, String password, BiblioRef object) throws VogcException {
        if (object.getPaperId() < 0 || object.getTitle() == null || object.getUri() == null || object.getYear() == null) {
            throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
        }
        try {

            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into biblio_ref values(?,?,?,?,?,?)");
            stm.setInt(1, object.getId());
            stm.setInt(2, object.getPaperId());
            stm.setString(3, object.getTitle());
            stm.setString(4, object.getDescription());
            stm.setString(5, object.getUri());
            stm.setString(6, object.getYear());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_FK_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a BiblioRef object
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioRef object id to be deleted
     * @throws VogcException
     */
    protected void deleteBiblioRef(String userName, String password, int id) throws VogcException {
        try {
            if (id < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from biblio_ref where id=?");
            stm.setInt(1, id);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the reference of the same paper
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the paper id
     * @return - an array of BiblioRef objects
     * @throws VogcException
     */
    protected BiblioRef[] selectBiblioRefByPaper(String userName, String password, int paperId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_ref", "*", "paperId", paperId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
            r.last();
            BiblioRef[] ref = new BiblioRef[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                ref[i] = (BiblioRef) r.getStatement();
            }
            return ref;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the reference with the same year of pubblication
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the year of pubblication
     * @return - an array of BiblioRef objects
     * @throws VogcException
     */
    protected BiblioRef[] selectBiblioRefByYear(String userName, String password, String year) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_ref", "*", "year", year);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
            r.last();
            BiblioRef[] ref = new BiblioRef[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                ref[i] = (BiblioRef) r.getStatement();
            }
            return ref;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a BiblioRef object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioRef id to select
     * @return - a BibliRef object
     * @throws VogcException
     */
    protected BiblioRef selectBiblioRef(String userName, String password, int id) throws VogcException {
        try {
            if (id < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("SELECT bn.objectId, bn.gcHasAttributeId, bn.starHasAttributeId, bn.plsHasAttributeId, bn.userId, bn.imageId, bn.date, bf.paperId, bf.title, bf.description, bf.uri, bf.year "
                    + "FROM biblio_notes bn, biblio_ref bf "
                    + "WHERE bn.id = bf.id and bn.id=?");
            stm.setInt(1, id);
            ResultSet r = stm.executeQuery();

            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }

            BiblioRef ref = new BiblioRef(id, r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("plsHasAttributeId"), r.getInt("starHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getInt("paperId"), r.getDate("date"),
                    r.getString("title"), r.getString("description"), r.getString("uri"), r.getString("year"));

            return ref;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select the paper id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param paperId the BiblioNotes id to select
     * @return - the paper id
     * @throws VogcException
     */
    protected int selectBiblioRefPaperId(String userName, String password, int id) throws VogcException, SQLException {
        return this.selectBiblioRef(userName, password, id).getPaperId();
    }

    /**
     * Method to select the reference's title
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the reference's title
     * @throws VogcException
     */
    protected String selectBiblioRefTitle(String userName, String password, int id) throws VogcException, SQLException {
        return this.selectBiblioRef(userName, password, id).getTitle();
    }

    /**
     * Method to select the reference's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the reference's description
     * @throws VogcException
     */
    protected String selectBiblioRefDescription(String userName, String password, int id) throws VogcException, SQLException {
        return this.selectBiblioRef(userName, password, id).getDescription();
    }

    /**
     * Method to select the reference's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the reference's uri
     * @throws VogcException
     */
    protected String selectBiblioRefUri(String userName, String password, int id) throws VogcException, SQLException {
        return this.selectBiblioRef(userName, password, id).getUri();
    }

    /**
     * Method to select the reference's year
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the reference's year
     * @throws VogcException
     */
    protected String selectBiblioRefYear(String userName, String password, int id) throws VogcException, SQLException {
        return this.selectBiblioRef(userName, password, id).getYear();
    }

    /**
     * Method to update the reference's paper
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the reference id to select
     * @param title the paper id
     * @throws VogcException
     */
    protected void updateBiblioRefPaperId(String userName, String password, int id, int paperId) throws VogcException {
        try {
            if (id < 0 || paperId < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_ref", "paperId", paperId, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the reference's title
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the reference id to select
     * @param title the reference's title
     * @throws VogcException
     */
    protected void updateBiblioRefTitle(String userName, String password, int id, String title) throws VogcException {
        try {
            if (id < 0 || title == null) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_ref", "title", title, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the reference's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the reference id to select
     * @param title the reference's description
     * @throws VogcException
     */
    protected void updateBiblioRefDescription(String userName, String password, int id, String description) throws VogcException {
        try {
            if (id < 0 || description == null) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_ref", "description", description, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the reference's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the reference id to select
     * @param title the reference's uri
     * @throws VogcException
     */
    protected void updateBiblioRefUri(String userName, String password, int id, String uri) throws VogcException {
        try {
            if (id < 0 || uri == null) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_ref", "uri", uri, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the reference's year
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the reference id to select
     * @param title the reference's year
     * @throws VogcException
     */
    protected void updateBiblioRefYear(String userName, String password, int id, String year) throws VogcException {
        try {
            if (id < 0 || year == null) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_ref", "year", year, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the last key inserted in the DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - the last key of the table
     * @throws VogcException
     */
    protected int selectLastBiblioRef(String userName, String password) throws VogcException {

        try {
            Connection con = connection.connect();

            PreparedStatement stm = con.prepareStatement("SELECT MAX(id) FROM biblio_ref;");

            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PAPER_NOT_EXISTS));
            }
            int val = r.getInt("MAX(id)");
            return val;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }
}
