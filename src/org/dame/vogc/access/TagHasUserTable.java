package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class TagHasUserTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new association between Tag and User
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag id
     * @param userId the user id
     * @throws VogcException
     */
    protected void insertTagHasUser(String userName, String password, int tagId, String userId) throws VogcException {
        if (userId == null || tagId < 0) {
            throw new VogcException(Messages.getError(Messages.TAG_HAS_USER_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            Calendar cal = Calendar.getInstance(); // istanzio un oggetto Calendar
            Date date = new Date(cal.getTimeInMillis());
            PreparedStatement stm = con.prepareStatement("insert into tag_has_user values(?,?,?)");
            stm.setInt(1, tagId);
            stm.setString(2, userId);
            stm.setString(3, date.toString());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.TAG_HAS_USER_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the tags inserted by the same user
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - an array of Tag objects with the same author
     * @throws VogcException
     */
    protected int[] selectTagByUser(String userName, String password, String userId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "tag_has_user", "*", "userId", userId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.TAG_HAS_USER_NOT_EXISTS));
            }
            r.last();
            int[] tag = new int[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                tag[i] = r.getInt("tagId");
            }
            return tag;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the users that have inserted the same tag
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId
     * @return - an array of catalogue objects with the same author
     * @throws VogcException
     */
    protected String[] selectUsersSameTag(String userName, String password, int tagId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "tag_has_user", "*", "tagId", tagId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.TAG_HAS_USER_NOT_EXISTS));
            }
            r.last();
            String[] user = new String[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                user[i] = r.getString("userId");
            }
            return user;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the userId of the user which inserted a tag
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag id
     * @return a user id
     * @throws VogcException
     */
    protected String selectUserByTag(String userName, String password, int tagId) throws VogcException {
        if (tagId < 0) {
            throw new VogcException(Messages.getError(Messages.TAG_PARAMETER_WRONG_OR_NULL));
        }
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "tag_has_user", "userId", "tagId", tagId);

            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.TAG_NOT_EXISTS));
            }
            String userId = r.getString("userId");
            return userId;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
