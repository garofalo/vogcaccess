package org.dame.vogc.access;

import java.sql.SQLException;
import java.util.Date;
import org.dame.vogc.datatypes.Author;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.datatypes.BiblioRef;
import org.dame.vogc.datatypes.Catalogue;
import org.dame.vogc.datatypes.GcAttribute;
import org.dame.vogc.datatypes.Gcluster;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.Image;
import org.dame.vogc.datatypes.Notes;
import org.dame.vogc.datatypes.Paper;
import org.dame.vogc.datatypes.PlsAttribute;
import org.dame.vogc.datatypes.Pulsar;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.Session;
import org.dame.vogc.datatypes.Source;
import org.dame.vogc.datatypes.Star;
import org.dame.vogc.datatypes.StarAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import org.dame.vogc.datatypes.Tag;
import org.dame.vogc.datatypes.User;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public interface IVOGCAccess {

// VObject methods
    void existVObjectId(String objectId) throws VogcException;

    void insertVObject(VObject object) throws VogcException;

    void deleteVObject(String objectId) throws VogcException;

    VObject selectVObject(String objectId) throws VogcException;

    VObject[] selectVObjectsByType(int type) throws VogcException;

    int selectVObjectType(String objectId) throws VogcException;

    void updateVObjectId(String oldId, String newId) throws VogcException;

    void updateVObjectType(String objectId, int type) throws VogcException;

    String[] selectPartialResult(String value) throws VogcException;
// Gcluster methods

    String[] selectClusterParameterResult(int aInt, String value, String operator) throws VogcException;

    void insertGcluster(Gcluster cluster) throws VogcException;

    void deleteGcluster(String gclusterId) throws VogcException;

    Gcluster selectGcluster(String gclusterId) throws VogcException;

    int selectGclusterType(String clusterId) throws VogcException;

    String selectGclusterName(String clusterId) throws VogcException;

    Gcluster[] selectGclustersByType(int type) throws VogcException;

    void updateGclusterId(String oldId, String newId) throws VogcException;

    void updateGclusterName(String clusterId, String name) throws VogcException;

    void updateGclusterType(String clusterId, int type) throws VogcException;

// Pulsar methods
    void insertPulsar(Pulsar object) throws VogcException;

    void deletePulsar(String pulsarId) throws VogcException;

    Pulsar selectPulsar(String pulsarId) throws VogcException;

    String selectGclusterId(String pulsarId) throws VogcException;

    Pulsar[] selectPulsarsByClusterId(String clusterId) throws VogcException;

    void updatePulsarId(String oldId, String newId) throws VogcException;

    void updatePulsarGclusterId(String objectId, String gclusterId) throws VogcException;

    // Star methods
    void insertStar(Star object) throws VogcException;

    void deleteStar(String starId) throws VogcException;

    Star selectStar(String starId) throws VogcException;

    String selectStarGclusterId(String starId) throws VogcException;

    Star[] selectStarsByClusterId(String clusterId) throws VogcException;

    void updateStarId(String oldId, String newId) throws VogcException;

    void updateStarGclusterId(String objectId, String gclusterId) throws VogcException;

    // GcAttribute methods
    int insertGcAttribute(GcAttribute att) throws VogcException, SQLException;

    void deleteGcAttribute(int gcAttributeId) throws VogcException;

    GcAttribute selectGcAttribute(int gcAttributeId) throws VogcException;

    GcAttribute[] selectPrimaryAttributes(int type) throws VogcException;

    GcAttribute[] selectGcAttributeByType(int type) throws VogcException;

    GcAttribute[] selectGcAttributeByUcd(String ucd) throws VogcException;

    String selectGcAttributeName(int gcAttributeId) throws VogcException;

    String selectGcAttributeDescription(int gcAttributeId) throws VogcException;

    String selectGcAttributeUcd(int gcAttributeId) throws VogcException;

    String selectGcAttributeDatatype(int gcAttributeId) throws VogcException;

    Boolean selectGcAttributePrimaryAtt(int gcAttributeId) throws VogcException;

    int selectGcAttributeType(int gcAttributeId) throws VogcException;

    void updateGcAttributeName(int attributeId, String name) throws VogcException;

    void updateGcAttributeDescription(int attributeId, String description) throws VogcException;

    void updateGcAttributeUcd(int attributeId, String ucd) throws VogcException;

    void updateGcAttributeDatatype(int attributeId, String datatype) throws VogcException;

    void updateGcAttributeType(int attributeId, int type) throws VogcException;

    void updateGcAttributePrimaryAtt(int gcAttributeId, boolean primaryAtt) throws VogcException;

    int selectGcAttributeIdByName(String attName) throws VogcException;

    void checkGcAttributeIsNew(String attName) throws VogcException;

    GcAttribute[] selectAllGcAttribute() throws VogcException;

    // PlsAttribute methods
    int insertPlsAttribute(PlsAttribute att) throws VogcException;

    void deletePlsAttribute(int plsAttributeId) throws VogcException;

    PlsAttribute selectPlsAttribute(int plsAttributeId) throws VogcException;

    String selectPlsAttributeName(int plsAttributeId) throws VogcException;

    String selectPlsAttributeDescription(int plsAttributeId) throws VogcException;

    String selectPlsAttributeUcd(int plsAttributeId) throws VogcException;

    String selectPlsAttributeDatatype(int plsAttributeId) throws VogcException;

    Boolean selectPlsAttributePrimaryAtt(int plsAttributeId) throws VogcException;

    PlsAttribute[] selectPlsAttributeByUcd(String ucd) throws VogcException;

    PlsAttribute[] selectPlsPrimaryAttributes(String userName, String password) throws VogcException;

    void updatePlsAttributeName(int plsAttributeId, String name) throws VogcException;

    void updatePlsAttributeDescription(int plsAttributeId, String description) throws VogcException;

    void updatePlsAttributeUcd(int plsAttributeId, String ucd) throws VogcException;

    void updatePlsAttributeDatatype(int plsAttributeId, String datatype) throws VogcException;

    void updatePlsAttributePrimaryAtt(int plsAttributeId, boolean primaryAtt) throws VogcException;

    int selectPlsAttributeIdByName(String attName) throws VogcException;

    void checkPlsAttributeIsNew(String attName) throws VogcException;

    PlsAttribute[] selectAllPlsAttribute() throws VogcException;

    PulsarHasAttribute selectPulsarSourceId(String objectId) throws VogcException;

// StarAttribute methods
    int insertStarAttribute(StarAttribute att) throws VogcException;

    void deleteStarAttribute(int starAttributeId) throws VogcException;

    StarAttribute selectStarAttribute(int starAttributeId) throws VogcException;

    String selectStarAttributeName(int starAttributeId) throws VogcException;

    String selectStarAttributeDescription(int starAttributeId) throws VogcException;

    String selectStarAttributeUcd(int starAttributeId) throws VogcException;

    String selectStarAttributeDatatype(int starAttributeId) throws VogcException;

    Boolean selectStarAttributePrimaryAtt(int starAttributeId) throws VogcException;

    StarAttribute[] selectStarAttributeByUcd(String ucd) throws VogcException;

    StarAttribute[] selectStarPrimaryAttributes(String userName, String password) throws VogcException;

    void updateStarAttributeName(int starAttributeId, String name) throws VogcException;

    void updateStarAttributeDescription(int starAttributeId, String description) throws VogcException;

    void updateStarAttributeUcd(int starAttributeId, String ucd) throws VogcException;

    void updateStarAttributeDatatype(int starAttributeId, String datatype) throws VogcException;

    void updateStarAttributePrimaryAtt(int starAttributeId, boolean primaryAtt) throws VogcException;

    int selectStarAttributeIdByName(String attName) throws VogcException;

    StarAttribute[] selectAllStarAttribute() throws VogcException;

    void checkStarAttributeIsNew(String starAttName) throws VogcException;

// GclusterHasAttribute methods
    void insertGclusterHasAttribute(GclusterHasAttribute att) throws VogcException, SQLException;

    void deleteGclusterHasAttribute(int gclusterHasAttId) throws VogcException;

    GclusterHasAttribute selectGclusterHasAttribute(int gclusterHasAttId) throws VogcException;

    GclusterHasAttribute selectGclusterSourceId(String objectId) throws VogcException;

    void updateGclusterHasAttGclusterId(int gclusterHasAttId, String gclusterId) throws VogcException;

    void updateGclusterHasAttGcAttId(int gclusterHasAttId, int gclusterAttId) throws VogcException;

    void updateGclusterHasAttSourceId(int gclusterHasAttId, String sourceId) throws VogcException;
    //void updateGclusterHasAttValue(int gclusterHasAttId, String value)throws VogcException;

    void updateGclusterHasAttValue(String value, String GcclusterId, String Gcclusterattribute) throws VogcException;

    void updateGclusterHasAttFirst(int gclusterHasAttId, boolean first) throws VogcException;
    //void updateGclusterHasAttDate(int gclusterHasAttId)throws VogcException;

    void updateGclusterHasAttDate(String value, String GcclusterId, String Gcclusterattribute) throws VogcException;

    String selectGclusterHasAttGclusterId(int gclusterHasAttId) throws VogcException;

    int selectGclusterHasAttGcAttId(int gclusterHasAttId) throws VogcException;

    String selectGclusterHasAttSourceId(int gclusterHasAttId) throws VogcException;

    String selectGclusterHasAttValue(int gclusterHasAttId) throws VogcException;

    boolean selectGclusterHasAttFirst(int GclusterHasAttId) throws VogcException;

    Date selectGclusterHasAttDate(int GclusterHasAttId) throws VogcException;

    GclusterHasAttribute[] selectGcHasAttByGclusterId(String gclusterId) throws VogcException;

    GclusterHasAttribute[] selectGcHasAttBySourceId(String sourceId) throws VogcException;

    GclusterHasAttribute[] selectGcHasAttByAttributeId(int attributeId) throws VogcException;

    GclusterHasAttribute[] selectGcHasAttByGclusterIdandObjectId(String objectId, int attId) throws VogcException;

    GclusterHasAttribute[] selectPositionalParameter() throws VogcException;

    GclusterHasAttribute[] selectPhotometricParameter() throws VogcException;

    GclusterHasAttribute[] selectStructuralParameter() throws VogcException;

    int selectGcHasAttNumber(int attributeId) throws VogcException;

// PulsarHasAttribute methods
    PulsarHasAttribute[] selectPlsHasAttByPulsarIdandObjectId(String objectId, int attId) throws VogcException;

    void insertPulsarHasAttribute(PulsarHasAttribute att) throws VogcException;

    void deletePulsarHasAttribute(int plsHasAttId) throws VogcException;

    void updatePulsarHasAttPulsarId(int plsHasAttId, String pulsarId) throws VogcException;

    void updatePulsarHasAttPlsAttributeId(int plsHasAttId, int plsAttributeId) throws VogcException;

    void updatePulsarHasAttSourceId(int plsHasAttId, String sourceId) throws VogcException;
    //void updatePulsarHasAttValue(int plsHasAttId, String value)throws VogcException;

    void updatePulsarHasAttValue(String value, String GcclusterId, String Gcclusterattribute) throws VogcException;

    void updatePulsarHasAttFirst(int plsHasAttId, boolean first) throws VogcException;
    //  void updatePulsarHasAttDate(int plsHasAttId)throws VogcException;

    void updatePulsarHasAttDate(String value, String GcclusterId, String Gcclusterattribute) throws VogcException;

    PulsarHasAttribute selectPulsarHasAttribute(int plsHasAttId) throws VogcException;

    String selectPulsarHasAttPulsarId(int plsHasAttId) throws VogcException;

    int selectPulsarHasAttPlsAttributeId(int plsHasAttId) throws VogcException;

    String selectPulsarHasAttSourceId(int plsHasAttId) throws VogcException;

    String selectPulsarHasAttValue(int plsHasAttId) throws VogcException;

    boolean selectPulsarHasAttFirst(int plsHasAttId) throws VogcException;

    Date selectPulsarHasAttDate(int plsHasAttId) throws VogcException;

    PulsarHasAttribute[] selectPlsHasAttByPulsarId(String pulsarId) throws VogcException;

    PulsarHasAttribute[] selectPlsHasAttBySourceId(String sourceId) throws VogcException;

    PulsarHasAttribute[] selectPlsHasAttByAttributeId(int attributeId) throws VogcException;

// StarHasAttribute methods
    StarHasAttribute[] selectStarHasAttByStarIdandObjectId(String objectId, int attId) throws VogcException;

    void insertStarHasAttribute(StarHasAttribute att) throws VogcException;

    void deleteStarHasAttribute(int starHasAttId) throws VogcException;

    void updateStarHasAttStarId(int starHasAttId, String starId) throws VogcException;

    void updateStarHasAttStarAttributeId(int starHasAttId, int starAttributeId) throws VogcException;

    void updateStarHasAttSourceId(int starHasAttId, String sourceId) throws VogcException;
    // void updateStarHasAttValue(int starHasAttId, String value)throws VogcException;

    void updateStarHasAttValue(String value, String GcclusterId, String Gcclusterattribute) throws VogcException;

    void updateStarHasAttFirst(int starHasAttId, boolean first) throws VogcException;
    // void updateStarHasAttDate(int starHasAttId)throws VogcException;

    void updateStarHasAttDate(String value, String GcclusterId, String Gcclusterattribute) throws VogcException;

    StarHasAttribute selectStarHasAttribute(int starHasAttId) throws VogcException;

    String selectStarHasAttStarId(int starHasAttId) throws VogcException;

    StarHasAttribute selectStarSourceId(String objectId) throws VogcException;

    int selectStarHasAttStarAttId(int starHasAttId) throws VogcException;

    String selectStarHasAttSourceId(int starHasAttId) throws VogcException;

    String selectStarHasAttValue(int starHasAttId) throws VogcException;

    boolean selectStarHasAttFirst(int starHasAttId) throws VogcException;

    Date selectStarHasAttDate(int starHasAttId) throws VogcException;

    StarHasAttribute[] selectStarHasAttByStarId(String starId) throws VogcException;

    StarHasAttribute[] selectStarHasAttBySourceId(String starId) throws VogcException;

    StarHasAttribute[] selectStarHasAttByAttributeId(int attributeId) throws VogcException;

// Author methods
    int insertAuthor(Author author) throws VogcException, SQLException;

    void deleteAuthor(int authorId) throws VogcException;

    Author selectAuthor(int authorId) throws VogcException;

    String selectAuthorName(int authorId) throws VogcException;

    String selectAuthorUri(int authorId) throws VogcException;

    void updateAuthorName(int authorId, String name) throws VogcException;

    void updateAuthorUri(int authorId, String uri) throws VogcException;

    Author[] selectAllAuthors() throws VogcException;
    //  int selectLastAuthor() throws VogcException;

// BiblioNotes methods
    int insertBiblioNotes(BiblioNotes object) throws VogcException, SQLException;

    void deleteBiblioNotes(int objectId) throws VogcException;

    BiblioNotes selectBiblioNotes(int id) throws VogcException;

    String selectBiblioNotesObjectId(int id) throws VogcException;

    int selectBiblioNotesGcAttributeId(int id) throws VogcException;

    int selectBiblioNotesStarAttributeId(int id) throws VogcException;

    int selectBiblioNotesPulsarAttributeId(int id) throws VogcException;

    String selectBiblioNotesUserId(int id) throws VogcException;

    int selectBiblioNotesImageId(int id) throws VogcException;

    Date selectBiblioNotesDate(int id) throws VogcException;

    int selectBiblioNotesType(int id) throws VogcException;

    int selectLastBiblioNotes() throws VogcException;

    void updateBiblioNotesObjectId(int id, String objectId) throws VogcException;

    void updateBiblioNotesGcAttributeId(int id, int gcAttributeId) throws VogcException;

    void updateBiblioNotesStarAttributeId(int id, int starAttributeId) throws VogcException;

    void updateBiblioNotesPlsAttributeId(int id, int plsAttributeId) throws VogcException;

    void updateBiblioNotesUserId(int id, String userId) throws VogcException;

    void updateBiblioNotesImageId(int id, int imageId) throws VogcException;

    void updateBiblioNotesDate(int id) throws VogcException;

    void updateBiblioNotesType(int id, int type) throws VogcException;

    BiblioNotes[] selectBiblioNotesByObjectId(String objectId) throws VogcException;

    BiblioNotes[] selectBiblioNotesByGcAttributeId(int gcAttId) throws VogcException;

    BiblioNotes[] selectBiblioNotesByStarAttributeId(int starAttId) throws VogcException;

    BiblioNotes[] selectBiblioNotesByPlsAttributeId(int plsAttId) throws VogcException;

    BiblioNotes[] selectBiblioNotesByUserId(String userId) throws VogcException;

    BiblioNotes[] selectBiblioNotesByImageId(int imageId) throws VogcException;

    BiblioNotes[] selectBiblioNotesByType(int type) throws VogcException;

// BiblioRef methods
    void insertBiblioRef(BiblioRef object) throws VogcException;

    void deleteBiblioRef(int id) throws VogcException;

    BiblioRef selectBiblioRef(int id) throws VogcException;

    BiblioRef[] selectBiblioRefByPaper(int paperId) throws VogcException;

    BiblioRef[] selectBiblioRefByYear(String year) throws VogcException;

    int selectBiblioRefPaperId(int id) throws VogcException;

    String selectBiblioRefTitle(int id) throws VogcException;

    String selectBiblioRefDescription(int id) throws VogcException;

    String selectBiblioRefUri(int id) throws VogcException;

    String selectBiblioRefYear(int id) throws VogcException;

    void updateBiblioRefPaperId(int id, int paperId) throws VogcException;

    void updateBiblioRefTitle(int id, String title) throws VogcException;

    void updateBiblioRefDescription(int id, String description) throws VogcException;

    void updateBiblioRefUri(int id, String uri) throws VogcException;

    void updateBiblioRefYear(int id, String year) throws VogcException;

    int selectLastBiblioRef() throws VogcException;

// Catalogue methods
    void insertCatalogue(Catalogue catalogue) throws VogcException;

    void deleteCatalogue(int id) throws VogcException;

    Catalogue selectCatalogue(String id) throws VogcException;

    String selectCatalogueTitle(String id) throws VogcException;

    String selectCatalogueDescription(String id) throws VogcException;

    String selectCatalogueYear(String id) throws VogcException;

    Date selectCatalogueDate(String id) throws VogcException;

    String selectCatalogueUri(String id) throws VogcException;

    void updateCatalogueTitle(String id, String title) throws VogcException;

    void updateCatalogueDescription(String id, String description) throws VogcException;

    void updateCatalogueYear(String id, String year) throws VogcException;

    void updateCatalogueUri(String id, String uri) throws VogcException;

    void updateCatalogueDate(String id) throws VogcException;

    Catalogue[] selectCatalogueByYear(String year) throws VogcException;

// Image methods
    void insertImage(Image img) throws VogcException;

    void deleteImage(int imageId) throws VogcException;

    Image selectImage(int imageId) throws VogcException;

    String selectImageName(int imageId) throws VogcException;

    String selectImageObjectId(int imageId) throws VogcException;

    String selectImageUserId(int imageId) throws VogcException;

    String selectImageDescription(int imageId) throws VogcException;

    String selectImageScale(int imageId) throws VogcException;

    String selectImageFormat(int imageId) throws VogcException;

    String selectImageDimension(int imageId) throws VogcException;

    String selectImageUri(int imageId) throws VogcException;

    Date selectImageDate(int imageId) throws VogcException;

    Image[] selectImageByObjectId(String objectId) throws VogcException;

    Image[] selectImageByUserId(String userId) throws VogcException;

    void updateImageObjectId(int imageId, String objectId) throws VogcException;

    void updateImageUserId(int imageId, String userId) throws VogcException;

    void updateImageName(int imageId, String name) throws VogcException;

    void updateImageDescription(int imageId, String description) throws VogcException;

    void updateImageScale(int imageId, String scale) throws VogcException;

    void updateImageFormat(int imageId, String format) throws VogcException;

    void updateImageDimension(int imageId, String dimension) throws VogcException;

    void updateImageUri(int imageId, String uri) throws VogcException;

    void updateImageDate(int imageId) throws VogcException;

// Notes methods
    void insertNotes(Notes object) throws VogcException;

    void deleteNotes(int objectId) throws VogcException;

    Notes selectNotes(int id) throws VogcException;

    Notes[] selectNotesByType(int type) throws VogcException;

    String selectNotesTitle(int id) throws VogcException;

    String selectNotesDescription(int id) throws VogcException;

    int selectNotesType(int id) throws VogcException;

    String selectNotesObjectId(int id) throws VogcException;

    int selectNotesGcAttributeId(int id) throws VogcException;

    int selectNotesStarAttributeId(int id) throws VogcException;

    int selectNotesPulsarAttributeId(int id) throws VogcException;

    String selectNotesUserId(int id) throws VogcException;

    int selectNotesImageId(int id) throws VogcException;

    Date selectNotesDate(int id) throws VogcException;

    void updateNotesTitle(int id, String title) throws VogcException;

    void updateNotesDescription(int id, String description) throws VogcException;

    void updateNotesType(int id, int type) throws VogcException;

// Paper methods
    int insertPaper(Paper paper) throws VogcException, SQLException;

    void deletePaper(int paperId) throws VogcException;

    Paper selectPaper(int paperId) throws VogcException;

    String selectPaperName(int paperId) throws VogcException;

    String selectPaperUri(int paperId) throws VogcException;

    void updatePaperName(int paperId, String name) throws VogcException;

    void updatePaperUri(int paperId, String uri) throws VogcException;

    Paper[] selectAllPapers() throws VogcException;
    //   int selectLastPaper() throws VogcException;

// Session methods
    void insertSession(String userId) throws VogcException;

    void deleteSession(int ssid) throws VogcException;

    Session selectSession(int ssid) throws VogcException;

    String selectSessionUserId(int ssid) throws VogcException;

    Date selectSessionCreationDate(int ssid) throws VogcException;

    Date selectSessionLastAccessDate(int ssid) throws VogcException;

    void updateSessionLastAccessDate(int id) throws VogcException;

    Session[] selectUserSessions(String userId) throws VogcException;

    int[] getUserSessionsId(String userId) throws VogcException;

// Source table
    void insertSource(Source src, int type) throws VogcException;

    void deleteSource(int sourceId) throws VogcException;

    Source selectSource(String sourceId) throws VogcException;

    Source[] selectSourceByType(int type) throws VogcException;

    int selectSourceType(String sourceId) throws VogcException;

    void updateSourceType(String sourceId, int type) throws VogcException;

    void updateSourceId(String oldId, String newId) throws VogcException;

// Tag table
    void insertTag(Tag tag) throws VogcException;

    void deleteTag(int tagId) throws VogcException;

    Tag selectTag(int tagId) throws VogcException;

    String selectTagName(int tagId) throws VogcException;

    void updateTagName(int tagId, String name) throws VogcException;

    int[] selectTagContainingAString(String aString) throws VogcException;

    String[] selectAllTag() throws VogcException;

// User methods
    void insertUser(User user) throws VogcException;

    void deleteUser(int userId) throws VogcException;

    User selectUser(String userId) throws VogcException;

    String selectUserName(String userId) throws VogcException;

    String selectUserPassword(String userId) throws VogcException;

    String selectUserSurname(String userId) throws VogcException;

    String selectUserMotivation(String userId) throws VogcException;

    Boolean selectUserStatus(String userId) throws VogcException;

    void updateUserId(int oldId, int newId) throws VogcException;

    void updateUserName(String userId, String name) throws VogcException;

    void updateUserPassword(String userId, String pwd) throws VogcException;

    void updateUserSurname(String surname, String userId) throws VogcException;

    void updateUserMotivation(String motiv, String userId) throws VogcException;

    void updateUserStatus(Boolean active, String userId) throws VogcException;

    User[] selectUsersByStatus(boolean status) throws VogcException;

    User[] selectAllUsers() throws VogcException;

    int authenticate(String userId) throws VogcException;

// BiblioHasAuthor methods
    void insertBiblioHasAuthor(int authorId, int biblioId) throws VogcException;

    int[] selectBiblioRefByAuthor(int authorId) throws VogcException;

    int[] selectAuthorsInBiblioRef(int biblioId) throws VogcException;

    void deleteBiblioHasAuthorByBiblioId(int biblioId) throws VogcException;

// CatalogueHasAuthor methods
    void insertCatalogueHasAuthor(String catId, int autId) throws VogcException;

    String[] selectCataloguesByAuthor(int autId) throws VogcException;

    int[] selectAuthorsInCatalogue(String catId) throws VogcException;

// ImageHasTag methods
    void insertImageHasTag(int tagId, int imageId) throws VogcException;

    int[] selectTagsInImage(int imageId) throws VogcException;

    int[] selectImageSameTag(int tag) throws VogcException;

// TagHasUser methods
    void insertTagHasUser(int tagId, String userId) throws VogcException;

    int[] selectTagByUser(String userId) throws VogcException;

    String selectUserByTag(int tagId) throws VogcException;

    String[] selectUsersSameTag(int tagId) throws VogcException;

// VObjectHasTag methods
    void insertVObjectHasTag(int tagId, String objectId) throws VogcException;

    int[] selectTagsInVObject(String objectId) throws VogcException;

    String[] selectVObjectSameTag(int tagId) throws VogcException;
}
