package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.User;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class UserTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new source in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param user the user object to insert
     * @throws VogcException
     */
    protected void insertUser(String userName, String password, User user) throws VogcException {
        if (user.getId() == null || user.getName() == null || user.getSurname() == null || user.getPassword() == null) {
            throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into user values(?,?,?,?,?,?,?)");
            stm.setString(1, user.getId());
            stm.setString(2, user.getPassword());
            stm.setString(3, user.getName());
            stm.setString(4, user.getSurname());
            stm.setString(5, user.getAffiliation());
            stm.setBoolean(6, user.isActive());
            stm.setString(7, user.getCountry());

            stm.executeUpdate();
            // if(stm.executeUpdate()== 0) throw new VogcException(Messages.getError(Messages.USER_INSERT_FAILED));

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a source from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id to delete
     * @throws VogcException
     */
    protected void deleteUser(String userName, String password, int userId) throws VogcException {
        try {
            if (userId < 0) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from user where id=?");
            stm.setInt(1, userId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a user from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id to select
     * @return - a user object
     * @throws VogcException
     */
    protected User selectUser(String userName, String password, String userId) throws VogcException {
        try {
            if (userId == null) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "user", "*", "id", userId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }
            User user = new User(userId, r.getString("password"), r.getString("name"), r.getString("surname"), r.getString("affiliation"), r.getString("country"), r.getBoolean("active"));

            return user;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the user's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - the user's name
     * @throws VogcException
     */
    protected String selectUserName(String userName, String password, String userId) throws VogcException {

        return this.selectUser(userName, password, userId).getName();
    }

    /**
     * Method to select the user's id to verify that the user is in the db
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - the user's id
     * @throws VogcException
     */
    protected String selectUserId(String userName, String password, String userId) throws VogcException {

        return this.selectUser(userName, password, userId).getId();

    }

    /**
     * Method to select the user's password
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - the user's password
     * @throws VogcException
     */
    protected String selectUserPassword(String userName, String password, String userId) throws VogcException {

        return this.selectUser(userName, password, userId).getPassword();
    }

    /**
     * Method to select the user's surname
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - the user's surname
     * @throws VogcException
     */
    protected String selectUserSurname(String userName, String password, String userId) throws VogcException {

        return this.selectUser(userName, password, userId).getSurname();
    }

    /**
     * Method to select the user's motivation
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - the user's motivation
     * @throws VogcException
     */
    protected String selectUserAffiliation(String userName, String password, String userId) throws VogcException {

        return this.selectUser(userName, password, userId).getAffiliation();
    }

    /**
     * Method to select the boolean flag that specifies the user's status
     * (active or not)
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - the user's status
     * @throws VogcException
     */
    protected Boolean selectUserStatus(String userName, String password, String userId) throws VogcException {

        return this.selectUser(userName, password, userId).isActive();
    }

    /*
     * Method to update the user id
     * @param userName          username for database access
     * @param password          password for database access
     * @param oldId             the user id to update
     * @param newId             the new user id
     * @throws VogcException
     */
    protected void updateUserId(String userName, String password, int oldId, int newId) throws VogcException {
        try {
            if (oldId < 0 || newId < 0) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "user", "id", newId, "id", oldId) == 0) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the user's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @param name the user's name
     * @throws VogcException
     */
    protected void updateUserName(String userName, String password, String userId, String name) throws VogcException {

        try {
            if (userId == null || name == null) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "user", "name", name, "id", userId) == 0) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the user's password
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId
     * @param pwd the user's password
     * @throws VogcException
     */
    protected void updateUserPassword(String userName, String password, String userId, String pwd) throws VogcException {

        try {
            if (userId == null || password == null) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "user", "password", pwd, "id", userId) == 0) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the user's surname
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @param surname the user's surname
     * @throws VogcException
     */
    protected void updateUserSurname(String userName, String password, String surname, String userId) throws VogcException {

        try {
            if (userId == null || surname == null) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "user", "surname", surname, "id", userId) == 0) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the user's motivations
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @param motiv the user's motivation
     * @throws VogcException
     */
    protected void updateUserAffiliation(String userName, String password, String motiv, String userId) throws VogcException {

        try {
            if (userId == null || motiv == null) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "user", "affiliation", motiv, "id", userId) == 0) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NOTES_PARAMETER_WRONG_OR_NULL));
        }
    }

    /**
     * Method to update the boolean flag that specifies the user's status
     * (active or not)
     *
     * @param userName username for database access
     * @param password password for database access
     * @param active
     * @param userId the user id
     * @throws VogcException
     */
    protected void updateUserStatus(String userName, String password, Boolean active, String userId) throws VogcException {

        try {
            if (userId == null) {
                throw new VogcException(Messages.getError(Messages.USER_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "user", "active", active, "id", userId) == 0) {
                throw new VogcException(Messages.getError(Messages.USER_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the active (or not) users
     *
     * @param userName username for database access
     * @param password password for database access
     * @param status
     * @return - an array of source object of the same type
     * @throws VogcException
     */
    protected User[] selectUsersByStatus(String userName, String password, boolean status) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "user", "*", "active", status);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.NO_USER_ENABLED));
            }
            r.last();
            User user[] = new User[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                user[i] = new User(r.getString("id"), r.getString("password"), r.getString("name"), r.getString("surname"), r.getString("motivation"), r.getString("country"), r.getBoolean("active"));
            }
            return user;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the user in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - an array of user object
     * @throws VogcException
     */
    protected User[] selectAllUsers(String userName, String password) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "user", "*");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.NO_USER_ENABLED));
            }
            r.last();
            User user[] = new User[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                user[i] = new User(r.getString("id"), r.getString("password"), r.getString("name"), r.getString("surname"), r.getString("affiliation"), r.getString("country"), r.getBoolean("active"));
            }
            return user;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
