package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.GcAttribute;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class GcAttributeTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param att gcattribute object to insert in the database
     * @throws VogcException
     */
    protected int insertGcAttribute(String userName, String password, GcAttribute att) throws VogcException, SQLException {
        if (att.getId() < 0 || att.getName() == null || att.getDatatype() == null) {
            throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into gc_attribute values(?,?,?,?,?,?,?)");
            stm.setInt(1, att.getId());
            stm.setString(2, att.getName());
            stm.setString(3, att.getDescription());
            stm.setString(4, att.getUcd());
            stm.setString(5, att.getDatatype());
            stm.setBoolean(6, att.isPrimaryAtt());
            stm.setInt(7, att.getType());
            try {
                stm.executeUpdate();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
            //    if(stm.executeUpdate()== 0) throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            stm = con.prepareStatement("select @@Identity as lastId");
            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.LAST_ID_NOT_RETRIEVED));
            }

            return r.getInt("lastId");


            /*      } catch (SQLException ex) {
             throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));*/
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a globular cluster's attribute from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the attribute id to delete
     * @throws VogcException
     * @throws SQLException
     */
    protected void deleteGcAttribute(String userName, String password, int gcAttributeId) throws VogcException {
        try {
            if (gcAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from gc_attribute where id=?");
            stm.setInt(1, gcAttributeId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a globular cluster attribute from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the attribute id to select
     * @return - a GcAttribute object
     * @throws VogcException
     */
    protected GcAttribute selectGcAttribute(String userName, String password, int gcAttributeId) throws VogcException {
        try {
            if (gcAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "gc_attribute", "*", "id", gcAttributeId);

            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
            //System.out.println("gcasdfsd= "+r.getString("name"));
            GcAttribute att = new GcAttribute(gcAttributeId, r.getString("name"), r.getString("description"), r.getString("ucd"), r.getString("datatype"),
                    r.getBoolean("primary_att"), r.getInt("type"));
            connection.disconnect();  //disconnect aprile 2011
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select all the globular cluster's primary attributes in a
     * set of attributes grouped by type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId globular cluster's attribute id
     * @param type globular cluster's attribute type
     */
    protected GcAttribute[] selectPrimaryAttributes(String userName, String password, int type) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "gc_attribute", "*", "primary_Att", "true", "type", type);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            GcAttribute[] attribute = new GcAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (GcAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the globular cluster's attributes belonging to the
     * same type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param type globular cluster's attribute type
     * @return
     * @throws VogcException
     */
    protected GcAttribute[] selectGcAttributeByType(String userName, String password, int type) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "gc_attribute", "*", "type", type);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            GcAttribute[] attribute = new GcAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (GcAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select all the globular cluster's attributes having the
     * same ucd
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ucd globular cluster's attribute ucd
     * @return - an array of gcAttribute objects
     * @throws VogcException
     */
    protected GcAttribute[] selectGcAttributeByUcd(String userName, String password, String ucd) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "gc_attribute", "*", "ucd", ucd);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            GcAttribute[] attribute = new GcAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (GcAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select the name of a globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the globular cluster's attribute id
     * @return - the globular cluster's attribute name
     * @throws VogcException
     */
    protected String selectGcAttributeName(String userName, String password, int gcAttributeId) throws VogcException {

        return this.selectGcAttribute(userName, password, gcAttributeId).getName();
    }

    /**
     * Method to select the description of a globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the globular cluster's attribute id
     * @return - the globular cluster's attribute description
     * @throws VogcException
     */
    protected String selectGcAttributeDescription(String userName, String password, int gcAttributeId) throws VogcException {

        return this.selectGcAttribute(userName, password, gcAttributeId).getDescription();
    }

    /**
     * Method to select the ucd of a globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the globular cluster's attribute id
     * @return - the globular cluster's attribute ucd
     * @throws VogcException
     */
    protected String selectGcAttributeUcd(String userName, String password, int gcAttributeId) throws VogcException {

        return this.selectGcAttribute(userName, password, gcAttributeId).getUcd();
    }

    /**
     * Method to select the datatype of a globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the globular cluster's attribute id
     * @return - the globular cluster's attribute datatype
     * @throws VogcException
     */
    protected String selectGcAttributeDatatype(String userName, String password, int gcAttributeId) throws VogcException {

        return this.selectGcAttribute(userName, password, gcAttributeId).getDatatype();
    }

    /**
     * Method to select the value of the boolean flag primary attribute of a
     * globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the globular cluster's attribute id
     * @return - the globular cluster's attribute boolean flag
     * @throws VogcException
     */
    protected Boolean selectGcAttributePrimaryAtt(String userName, String password, int gcAttributeId) throws VogcException {

        return this.selectGcAttribute(userName, password, gcAttributeId).isPrimaryAtt();

    }

    /**
     * Method to select the globular cluster's attribute type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the globular cluster's attribute id
     * @return - the globular cluster's attribute type
     * @throws VogcException
     */
    protected int selectGcAttributeType(String userName, String password, int gcAttributeId) throws VogcException {

        return this.selectGcAttribute(userName, password, gcAttributeId).getType();
    }

    /**
     * Method to update the globular cluster's attribute name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the globular cluster's attribute id
     * @param name the globular cluster's new name
     * @throws VogcException
     */
    protected void updateGcAttributeName(String userName, String password, int attributeId, String name) throws VogcException {
        try {
            if (attributeId < 0 || name == null) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gc_attribute", "name", name, "id", attributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the globular cluster's attribute description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the globular cluster's attribute id
     * @param description the globular cluster's new description
     * @throws VogcException
     */
    protected void updateGcAttributeDescription(String userName, String password, int attributeId, String description) throws VogcException {
        try {
            if (attributeId < 0 || description == null) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gc_attribute", "description", description, "id", attributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the globular cluster's attribute ucd
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the globular cluster's attribute id
     * @param ucd the globular cluster's new ucd
     * @throws VogcException
     */
    protected void updateGcAttributeUcd(String userName, String password, int attributeId, String ucd) throws VogcException {
        try {
            if (attributeId < 0 || ucd == null) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gc_attribute", "ucd", ucd, "id", attributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the globular cluster's attribute datatype
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the globular cluster's attribute id
     * @param datatype the globular cluster's new datatype
     * @throws VogcException
     */
    protected void updateGcAttributeDatatype(String userName, String password, int attributeId, String datatype) throws VogcException {
        try {
            if (attributeId < 0 || datatype == null) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gc_attribute", "datatype", datatype, "id", attributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the globular cluster's attribute datatype
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the globular cluster's attribute id
     * @param type the globular cluster's attribute new type
     * @throws VogcException
     */
    protected void updateGcAttributeType(String userName, String password, int attributeId, int type) throws VogcException {
        try {
            if (attributeId < 0 || type < -1) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gc_attribute", "type", type, "id", attributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag primary attribute of a
     * globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttributeId the globular cluster's attribute id
     * @param the new boolean value for the flag primaryAttribute
     * @throws VogcException
     */
    protected void updateGcAttributePrimaryAtt(String userName, String password, int gcAttributeId, boolean primaryAtt) throws VogcException {
        try {
            if (gcAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "gc_attribute", "primary_att", primaryAtt, "id", gcAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the attribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - the attribute id
     * @throws VogcException
     */
    protected int selectGcAttributeIdByName(String userName, String password, String attName) throws VogcException {

        int id = 0;
        try {
            if (attName == null) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "gc_attribute", "*", "name", attName);



            while (r.next()) {
                id = r.getInt("id");


            }


//            if (r.next()) {
//                id = r.getInt("id");
//            } else {
//                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
//            }

            return id;

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }

    }

    /**
     * Method used to check if the attribute name inserted by the user already
     * exists in the local DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attName
     * @return
     * @throws VogcException
     */
    protected void checkGcAttributeIsNew(String userName, String password, String attName) throws VogcException {


        try {
            if (attName == null) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "gc_attribute", "*", "name", attName);

            if (r.next()) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_ALREADY_EXISTS));
            }



        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            } finally {
                try {
                    connection.disconnect();
                } catch (SQLException ex) {
                    throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
                }
            }
        }
    }

    /**
     * Method used to select all the glocular cluster's attributes
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - an array of GcAttribute objects
     * @throws VogcException
     */
    protected GcAttribute[] selectAllGcAttribute(String userName, String password) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "gc_attribute", "*");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            GcAttribute attribute[] = new GcAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new GcAttribute(r.getInt("id"), r.getString("name"), r.getString("description"), r.getString("ucd"), r.getString("datatype"), r.getBoolean("primary_att"), r.getInt("type"));
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }
}
