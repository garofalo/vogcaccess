package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.PlsAttribute;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class PlsAttributeTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param att PlsAttribute object to insert in the database
     * @return
     * @throws VogcException
     */
    protected int insertPlsAttribute(String userName, String password, PlsAttribute att) throws VogcException {
        if (att.getName() == null || att.getDatatype() == null) {
            throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into pls_attribute values(?,?,?,?,?,?)");

            stm.setInt(1, 0);
            stm.setString(2, att.getName());
            stm.setString(3, att.getDescription());
            stm.setString(4, att.getUcd());
            stm.setString(5, att.getDatatype());
            stm.setBoolean(6, att.isPrimaryAtt());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
            stm = con.prepareStatement("select @@Identity as lastId");
            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.LAST_ID_NOT_RETRIEVED));
            }

            return r.getInt("lastId");

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a pulsar's attribute from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId the attribute id to delete
     * @throws VogcException
     */
    protected void deletePlsAttribute(String userName, String password, int plsAttributeId) throws VogcException {
        try {
            if (plsAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from pls_attribute where id=?");
            stm.setInt(1, plsAttributeId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to update pulsar's attribute name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @param name the pulsar's new name
     * @throws VogcException
     */
    protected void updatePlsAttributeName(String userName, String password, int plsAttributeId, String name) throws VogcException {
        try {
            if (plsAttributeId < 0 || name == null) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pls_attribute", "name", name, "id", plsAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the pulsar's attribute description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @param description the pulsar's new description
     * @throws VogcException
     */
    protected void updatePlsAttributeDescription(String userName, String password, int plsAttributeId, String description) throws VogcException {
        try {
            if (plsAttributeId < 0 || description == null) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pls_attribute", "description", description, "id", plsAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the pulsar's attribute ucd
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @param ucd the star's new ucd
     * @throws VogcException
     */
    protected void updatePlsAttributeUcd(String userName, String password, int plsAttributeId, String ucd) throws VogcException {
        try {
            if (plsAttributeId < 0 || ucd == null) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pls_attribute", "ucd", ucd, "id", plsAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the pulsar's attribute datatype
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @param datatype the pulsar's new datatype
     * @throws VogcException
     */
    protected void updatePlsAttributeDatatype(String userName, String password, int plsAttributeId, String datatype) throws VogcException {
        try {
            if (plsAttributeId < 0 || datatype == null) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pls_attribute", "datatype", datatype, "id", plsAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag primary attribute of a
     * pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @param primaryAtt the new boolean value for the flag primaryAttribute
     * @throws VogcException
     */
    protected void updatePlsAttributePrimaryAtt(String userName, String password, int plsAttributeId, boolean primaryAtt) throws VogcException {
        try {
            if (plsAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pls_attribute", "primary_att", primaryAtt, "id", plsAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a pulsar attribute from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @return - a PlsAttribute object
     * @throws VogcException
     */
    protected PlsAttribute selectPlsAttribute(String userName, String password, int plsAttributeId) throws VogcException {
        try {
            if (plsAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "pls_attribute", "*", "id", plsAttributeId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }

            PlsAttribute att = new PlsAttribute(plsAttributeId, r.getString("name"), r.getString("description"), r.getString("ucd"), r.getString("datatype"), r.getBoolean("primary_att"));

            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the name of a pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @return - the pulsar's attribute name
     * @throws VogcException
     */
    protected String selectPlsAttributeName(String userName, String password, int plsAttributeId) throws VogcException {
        return this.selectPlsAttribute(userName, password, plsAttributeId).getName();
    }

    /**
     * Method to select the description of a pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @return - the pulsar's attribute description
     * @throws VogcException
     */
    protected String selectPlsAttributeDescription(String userName, String password, int plsAttributeId) throws VogcException {

        return this.selectPlsAttribute(userName, password, plsAttributeId).getDescription();
    }

    /**
     * Method to select the ucd of a pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId the pulsar's attribute id
     * @return - the pulsar's attribute ucd
     * @throws VogcException
     */
    protected String selectPlsAttributeUcd(String userName, String password, int plsAttributeId) throws VogcException {

        return this.selectPlsAttribute(userName, password, plsAttributeId).getUcd();
    }

    /**
     * Method to select the datatype of a pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId the pulsar's attribute id
     * @return - the pulsar's attribute datatype
     * @throws VogcException
     */
    protected String selectPlsAttributeDatatype(String userName, String password, int plsAttributeId) throws VogcException {

        return this.selectPlsAttribute(userName, password, plsAttributeId).getDatatype();
    }

    /**
     * Method to select the value of the boolean flag primary attribute of a
     * pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttributeId
     * @return - the pulsar's attribute boolean flag
     * @throws VogcException
     */
    protected Boolean selectPlsAttributePrimaryAtt(String userName, String password, int plsAttributeId) throws VogcException {

        return this.selectPlsAttribute(userName, password, plsAttributeId).isPrimaryAtt();

    }

    /**
     * Method used to select all the pulsar's attributes having the same ucd
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ucd pulsar's attribute ucd
     * @return - an array of PlsAttribute objects
     * @throws VogcException
     */
    protected PlsAttribute[] selectPlsAttributeByUcd(String userName, String password, String ucd) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "pls_attribute", "*", "ucd", ucd);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            PlsAttribute[] attribute = new PlsAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (PlsAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the pulsar's primary attributes
     *
     * @param userName username for database access
     * @param password password for database access
     * @return
     * @throws org.dame.vogc.exceptions.VogcException
     */
    protected PlsAttribute[] selectPlsPrimaryAttributes(String userName, String password) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "pls_attribute", "*", "primaryAtt", "true");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            PlsAttribute[] attribute = new PlsAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (PlsAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to check if the attribute name inserted by the user already
     * exists in the local DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attName the attribute name
     * @throws VogcException
     */
    protected void checkPlsAttributeIsNew(String userName, String password, String attName) throws VogcException {
        try {
            if (attName == null) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "pls_attribute", "*", "name", attName);
            if (r.next()) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_ALREADY_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the glocular cluster's attributes
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - an array of GcAttribute objects
     * @throws VogcException
     */
    protected PlsAttribute[] selectAllPlsAttribute(String userName, String password) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "pls_attribute", "*");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            PlsAttribute attribute[] = new PlsAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new PlsAttribute(r.getInt("id"), r.getString("name"), r.getString("description"), r.getString("ucd"), r.getString("datatype"), r.getBoolean("primary_att"));
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the attribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attName
     * @return - the attribute id
     * @throws VogcException
     */
    protected int selectPlsAttributeIdByName(String userName, String password, String attName) throws VogcException {

        int id = 0;
        try {
            if (attName == null) {
                throw new VogcException(Messages.getError(Messages.GCATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "pls_attribute", "*", "name", attName);

            if (r.next()) {
                id = r.getInt("id");
            } else {
                throw new VogcException(Messages.getError(Messages.PLSATTRIBUTE_NOT_EXISTS));
            }

            return id;

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
