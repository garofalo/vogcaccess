package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class CatalogueHasAuthorTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new association between Catalogue and Author
     *
     * @param userName username for database access
     * @param password password for database access
     * @param catId the catalogue id
     * @param autId the author id
     * @throws VogcException
     */
    protected void insertCatalogueHasAuthor(String userName, String password, String catId, int autId) throws VogcException {
        if (catId == null || autId < 0) {
            throw new VogcException(Messages.getError(Messages.CATALOGUE_HAS_AUTHOR_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into catalogue_has_author values(?,?)");
            stm.setString(1, catId);
            stm.setInt(2, autId);

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_OR_AUTHOR_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the catalogue having the same author
     *
     * @param userName username for database access
     * @param password password for database access
     * @param autId the author id
     * @return - an array of catalogue objects with the same author
     * @throws VogcException
     */
    protected String[] selectCataloguesByAuthor(String userName, String password, int autId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "catalogue_has_author", "*", "authorId", autId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_OR_AUTHOR_NOT_EXISTS));
            }
            r.last();
            String[] cat = new String[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                cat[i] = r.getString("catalogueId");
            }
            return cat;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the catalogue having the same author
     *
     * @param userName username for database access
     * @param password password for database access
     * @param autId the author id
     * @return - an array of catalogue objects with the same author
     * @throws VogcException
     */
    protected int[] selectAuthorsInCatalogue(String userName, String password, String catId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "catalogue_has_author", "*", "catalogueId", catId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_OR_AUTHOR_NOT_EXISTS));
            }
            r.last();
            int[] author = new int[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                author[i] = r.getInt("authorId");
            }
            return author;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
