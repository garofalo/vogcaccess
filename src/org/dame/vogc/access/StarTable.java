package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.Star;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class StarTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method used to insert a new star
     *
     * @param userName username for database access
     * @param password password for database access
     * @param object the Star object to insert
     * @throws VogcException
     */
    protected void insertStar(String userName, String password, Star object) throws VogcException {
//        if(object.getId() == null || object.getGclusterId() == null)
//        throw new VogcException(Messages.getError(Messages.STAR_PARAMETER_WRONG_OR_NULL));
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into star values(?,?)");
            stm.setString(1, object.getId());
            stm.setString(2, object.getGclusterId());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_INSERT_FAILED));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to delete a tuple from the star table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starId the star id to delete
     * @throws VogcException
     */
    protected void deleteStar(String userName, String password, String starId) throws VogcException {
        try {
            if (starId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from star where objectId=?");
            stm.setString(1, starId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select a star object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starId the star id to select
     * @return - a star object
     * @throws VogcException
     */
    protected Star selectStar(String userName, String password, String starId) throws VogcException {
        try {
            if (starId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "star", "*", "objectId", starId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.STAR_NOT_EXISTS));
            }
            Star star = new Star(starId, r.getString("gclusterId"));
            return star;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a list of stars belonging to the same globular cluster
     *
     * @param userName username for database access
     * @param password password for database access
     * @param clusterId the globular cluster id
     * @return - an array of star objects
     * @throws VogcException
     */
    protected Star[] selectStarsByClusterId(String userName, String password, String clusterId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "star", "*", "gclusterId", clusterId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.STAR_NOT_EXISTS));
            }
            r.last();
            Star star[] = new Star[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                star[i] = new Star(r.getString("objectId"), r.getString("gclusterId"));

            }
            return star;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to update a star id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param oldId the star id to update
     * @param newId the new star id
     * @throws VogcException
     */
    protected void updateStarId(String userName, String password, String oldId, String newId) throws VogcException {
        try {
            if (oldId == null || newId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star", "objectId", newId, "objectId", oldId) == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to update the globular cluster id in which the star is
     * contained
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the star id
     * @param gclusterId the new globular cluster id
     * @throws VogcException
     */
    protected void updateStarGclusterId(String userName, String password, String objectId, String gclusterId) throws VogcException {
        try {
            if (objectId == null || gclusterId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star", "gclusterId", gclusterId, "objectId", objectId) == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the globular cluster id in which the star is contained
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starId the star id
     * @return - globular cluster id
     * @throws VogcException
     */
    protected String selectStarGclusterId(String userName, String password, String starId) throws VogcException {

        return this.selectStar(userName, password, starId).getGclusterId();
    }
}
