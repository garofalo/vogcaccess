package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.Author;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class AuthorTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new author in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param author the Author object to insert
     * @return
     * @throws VogcException
     * @throws java.sql.SQLException
     */
    protected int insertAuthor(String userName, String password, Author author) throws VogcException, SQLException {
        if (author.getId() != 0 || author.getName() == null) {
            throw new VogcException(Messages.getError(Messages.AUTHOR_PARAMETER_WRONG_OR_NULL_VALUE));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into author (name,uri) values(?,?)");
            //stm.setInt(1, 0);
            stm.setString(1, author.getName());
            stm.setString(2, author.getUri());
            //try{
            // stm.executeUpdate();
            if (stm.executeUpdate() != 0) //throw new VogcException(Messages.getError(Messages.AUTHOR_INSERT));
            // }catch(SQLException ex){
            {
                throw new VogcException(Messages.getError(Messages.AUTHOR_INSERT_FAILED));
            }

            //}
            stm = con.prepareStatement("select @@Identity as lastId");
            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.LAST_ID_NOT_RETRIEVED));
            }
//
            return r.getInt("lastId");
            //    return(1);
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete an author from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author id to delete
     * @throws VogcException
     */
    protected void deleteAuthor(String userName, String password, int authorId) throws VogcException {
        try {
            if (authorId < 0) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_PARAMETER_WRONG_OR_NULL_VALUE));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from author where id=?");
            stm.setInt(1, authorId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select an author from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author id to select
     * @return - an Author object
     * @throws VogcException
     */
    protected Author selectAuthor(String userName, String password, int authorId) throws VogcException {
        try {
            if (authorId < 0) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_PARAMETER_WRONG_OR_NULL_VALUE));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "author", "*", "id", authorId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_NOT_EXISTS));
            }
            Author author = new Author(authorId, r.getString("name"), r.getString("uri"));
            return author;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the author's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author's id
     * @return - the author's name
     * @throws VogcException
     */
    protected String selectAuthorName(String userName, String password, int authorId) throws VogcException {
        return this.selectAuthor(userName, password, authorId).getName();
    }

    /**
     * Method to select the author's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author's id
     * @return - the author's uri
     * @throws VogcException
     */
    protected String selectAuthorUri(String userName, String password, int authorId) throws VogcException {
        return this.selectAuthor(userName, password, authorId).getUri();
    }

    /**
     * Method to update the author's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author's id
     * @param name the new author's name
     * @throws VogcException
     */
    protected void updateAuthorName(String userName, String password, int authorId, String name) throws VogcException {
        try {
            if (authorId < 0 || name == null) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_PARAMETER_WRONG_OR_NULL_VALUE));
            }
            if (DbStatements.update(connection, userName, password, "author", "name", name, "id", authorId) == 0) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the author's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author's id
     * @param uri
     * @throws VogcException
     */
    protected void updateAuthorUri(String userName, String password, int authorId, String uri) throws VogcException {
        try {
            if (authorId < 0) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_PARAMETER_WRONG_OR_NULL_VALUE));
            }
            if (DbStatements.update(connection, userName, password, "author", "uri", uri, "id", authorId) == 0) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the last key inserted in the DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - the last key of the table
     * @throws VogcException
     */
    protected int selectLastAuthor(String userName, String password) throws VogcException {

        try {
            Connection con = connection.connect();

            PreparedStatement stm = con.prepareStatement("SELECT MAX(id) FROM author;");

            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_NOT_EXISTS));
            }
            int val = r.getInt("MAX(id)");
            return val;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected Author[] selectAllAuthors(String userName, String password) throws VogcException {

        try {
            ResultSet r = DbStatements.query(connection, userName, password, "author", "*");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.AUTHOR_NOT_EXISTS));
            }
            r.last();
            Author authors[] = new Author[r.getRow()];

            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                authors[i] = new Author(r.getInt("id"), r.getString("name"), r.getString("uri"));
            }
            return authors;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
