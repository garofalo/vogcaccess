package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class BiblioHasAuthorTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new association between a bibliographic reference and
     * the authors
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author id
     * @param biblioId the bibliographic reference's id
     * @throws VogcException
     */
    protected void insertBiblioHasAuthor(String userName, String password, int authorId, int biblioId) throws VogcException {
        if (authorId < 0 || biblioId < 0) {
            throw new VogcException(Messages.getError(Messages.BIBLIO_HAS_AUTHOR_WRONG_OR_NULL_VALUE));
        }
        try {

            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into biblio_has_author values(?,?)");
            stm.setInt(1, biblioId);
            stm.setInt(2, authorId);

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIO_OR_AUTHOR_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the bibliographic references written by the same
     * author
     *
     * @param userName username for database access
     * @param password password for database access
     * @param authorId the author id
     * @return - an array of reference's id
     * @throws VogcException
     */
    protected int[] selectBiblioRefByAuthor(String userName, String password, int authorId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_has_author", "*", "authorId", authorId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIO_OR_AUTHOR_NOT_EXISTS));
            }
            r.last();
            int[] ref = new int[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                ref[i] = r.getInt("biblioId");
            }
            return ref;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the authors in a bibliographic reference
     *
     * @param userName username for database access
     * @param password password for database access
     * @param biblioId
     * @return - an array of Author's id
     * @throws VogcException
     */
    protected int[] selectAuthorsInBiblioRef(String userName, String password, int biblioId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_has_author", "*", "biblioId", biblioId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIO_OR_AUTHOR_NOT_EXISTS));
            }
            r.last();
            int[] authorsId = new int[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                authorsId[i] = r.getInt("authorId");
            }
            return authorsId;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to delete an association between a bibliographic reference and one
     * or more authors
     *
     * @param userName username for database access
     * @param password password for database access
     * @param bibRefId
     * @throws VogcException
     */
    protected void deleteBiblioHasAuthorByBiblioId(String userName, String password, int bibRefId) throws VogcException {
        try {
            if (bibRefId < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from biblio_has_author where biblioId=?");
            stm.setInt(1, bibRefId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIOREF_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }
}
