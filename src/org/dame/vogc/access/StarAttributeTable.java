package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.StarAttribute;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina
 */
public class StarAttributeTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new star's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param att starAttribute object to insert in the database
     * @return
     * @throws VogcException
     */
    protected int insertStarAttribute(String userName, String password, StarAttribute att) throws VogcException {
        if (att.getName() == null || att.getDatatype() == null) {
            throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into star_attribute values(?,?,?,?,?,?)");
            stm.setInt(1, att.getId());
            stm.setString(2, att.getName());
            stm.setString(3, att.getDescription());
            stm.setString(4, att.getUcd());
            stm.setString(5, att.getDatatype());
            stm.setBoolean(6, att.isPrimaryAtt());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
            stm = con.prepareStatement("select @@Identity as lastId");
            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.LAST_ID_NOT_RETRIEVED));
            }

            return r.getInt("lastId");

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a star's attribute from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the attribute id to delete
     * @throws VogcException
     */
    protected void deleteStarAttribute(String userName, String password, int starAttributeId) throws VogcException {
        try {
            if (starAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from star_attribute where id=?");
            stm.setInt(1, starAttributeId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to update star's attribute name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId
     * @param name the star's new name
     * @throws VogcException
     */
    protected void updateStarAttributeName(String userName, String password, int starAttributeId, String name) throws VogcException {
        try {
            if (starAttributeId < 0 || name == null) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_attribute", "name", name, "id", starAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the star's attribute description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the star's attribute id
     * @param description the star's new description
     * @throws VogcException
     */
    protected void updateStarAttributeDescription(String userName, String password, int starAttributeId, String description) throws VogcException {
        try {
            if (starAttributeId < 0 || description == null) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_attribute", "description", description, "id", starAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the star's attribute ucd
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the star's attribute id
     * @param ucd the star's new ucd
     * @throws VogcException
     */
    protected void updateStarAttributeUcd(String userName, String password, int starAttributeId, String ucd) throws VogcException {
        try {
            if (starAttributeId < 0 || ucd == null) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_attribute", "ucd", ucd, "id", starAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the star's attribute datatype
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId
     * @param datatype the star's new datatype
     * @throws VogcException
     */
    protected void updateStarAttributeDatatype(String userName, String password, int starAttributeId, String datatype) throws VogcException {
        try {
            if (starAttributeId < 0 || datatype == null) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_attribute", "datatype", datatype, "id", starAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag primary attribute of a
     * star's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the star's attribute id
     * @param primaryAtt the new boolean value for the flag primaryAttribute
     * @throws VogcException
     */
    protected void updateStarAttributePrimaryAtt(String userName, String password, int starAttributeId, boolean primaryAtt) throws VogcException {
        try {
            if (starAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_attribute", "primary_att", primaryAtt, "id", starAttributeId) == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a star attribute from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the attribute id to select
     * @return - a StarAttribute object
     * @throws VogcException
     */
    protected StarAttribute selectStarAttribute(String userName, String password, int starAttributeId) throws VogcException {
        try {
            if (starAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "star_attribute", "*", "id", starAttributeId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
            StarAttribute att = new StarAttribute(starAttributeId, r.getString("name"), r.getString("description"), r.getString("ucd"), r.getString("datatype"), r.getBoolean("primary_att"));
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the name of a star's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the star's attribute id
     * @return - the star's attribute name
     * @throws VogcException
     */
    protected String selectStarAttributeName(String userName, String password, int starAttributeId) throws VogcException {

        return this.selectStarAttribute(userName, password, starAttributeId).getName();
    }

    /**
     * Method to select the description of a globular cluster's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the globular cluster's attribute id
     * @return - the globular cluster's attribute description
     * @throws VogcException
     */
    protected String selectStarAttributeDescription(String userName, String password, int starAttributeId) throws VogcException {

        return this.selectStarAttribute(userName, password, starAttributeId).getDescription();
    }

    /**
     * Method to select the ucd of a star's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the star's attribute id
     * @return - the star's attribute ucd
     * @throws VogcException
     */
    protected String selectStarAttributeUcd(String userName, String password, int starAttributeId) throws VogcException {

        return this.selectStarAttribute(userName, password, starAttributeId).getUcd();
    }

    /**
     * Method to select the datatype of a star's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId the star's attribute id
     * @return - thestar's attribute datatype
     * @throws VogcException
     */
    protected String selectStarAttributeDatatype(String userName, String password, int starAttributeId) throws VogcException {

        return this.selectStarAttribute(userName, password, starAttributeId).getDatatype();
    }

    /**
     * Method to select the value of the boolean flag primary attribute of a
     * star's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttributeId
     * @return - the star's attribute boolean flag
     * @throws VogcException
     */
    protected Boolean selectStarAttributePrimaryAtt(String userName, String password, int starAttributeId) throws VogcException {

        return this.selectStarAttribute(userName, password, starAttributeId).isPrimaryAtt();
    }

    /**
     * Method used to select all the star's attributes having the same ucd
     *
     * @param userName username for database access
     * @param password password for database access
     * @param ucd star's attribute ucd
     * @return - an array of starAttribute objects
     * @throws VogcException
     */
    protected StarAttribute[] selectStarAttributeByUcd(String userName, String password, String ucd) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "star_attribute", "*", "ucd", ucd);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            StarAttribute[] attribute = new StarAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (StarAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the star's primary attributes
     *
     * @param userName username for database access
     * @param password password for database access
     * @return
     * @throws org.dame.vogc.exceptions.VogcException
     */
    protected StarAttribute[] selectStarPrimaryAttributes(String userName, String password) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "star_attribute", "*", "primary_att", "true");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            StarAttribute[] attribute = new StarAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (StarAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to check if the attribute name inserted by the user already
     * exists in the local DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attName the attribute name
     * @throws VogcException
     */
    protected void checkStarAttributeIsNew(String userName, String password, String attName) throws VogcException {
        try {
            if (attName == null) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "star_attribute", "*", "name", attName);
            if (r.next()) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_ALREADY_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the glocular cluster's attributes
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - an array of GcAttribute objects
     * @throws VogcException
     */
    protected StarAttribute[] selectAllStarAttribute(String userName, String password) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "star_attribute", "*");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            StarAttribute attribute[] = new StarAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new StarAttribute(r.getInt("id"), r.getString("name"), r.getString("description"), r.getString("ucd"), r.getString("datatype"), r.getBoolean("primary_att"));
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the attribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attName
     * @return - the attribute id
     * @throws VogcException
     */
    protected int selectStarAttributeIdByName(String userName, String password, String attName) throws VogcException {

        int id = 0;
        try {
            if (attName == null) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "star_attribute", "*", "name", attName);

            if (r.next()) {
                id = r.getInt("id");
            } else {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }

            return id;

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
