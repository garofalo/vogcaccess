package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.dame.vogc.datatypes.StarHasAttribute;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author luca
 */
public class StarHasAttributeTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new starHasAttribute object in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param att the starHasAttribute object to insert
     * @throws VogcException
     */
    protected void insertStarHasAttribute(String userName, String password, StarHasAttribute att) throws VogcException {
        if (att.getStarAttId() < 0 || att.getStarId() == null || att.getSourceId() == null || att.getValue() == null) {
            throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into star_has_attribute values(?,?,?,?,?,?,?)");
            stm.setInt(1, att.getId());
            stm.setString(2, att.getStarId());
            stm.setInt(3, att.getStarAttId());
            stm.setString(4, att.getSourceId());
            stm.setString(5, att.getValue());
            stm.setBoolean(6, att.isFirst());
            stm.setString(7, date.toString());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }

    }

    protected StarHasAttribute[] selectStarHasAttByStarIdandObjectId(String userName, String password, String objectId, int attId) throws VogcException {

        ResultSet r = null;

        try {
            r = DbStatements.queryHistoryStar(connection, userName, password, objectId, attId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            StarHasAttribute attribute[] = new StarHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new StarHasAttribute(r.getInt("id"), r.getInt("starAttributeId"), r.getString("starId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a starHasAttribute object from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId
     * @throws VogcException
     */
    protected void deleteStarHasAttribute(String userName, String password, int starHasAttId) throws VogcException {
        try {
            if (starHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from star_has_attribute where id=?");
            stm.setInt(1, starHasAttId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to update a star id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @param starId the new star id
     * @throws VogcException
     */
    protected void updateStarHasAttStarId(String userName, String password, int starHasAttId, String starId) throws VogcException {
        try {
            if (starHasAttId < 0 || starId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_has_attribute", "starId", starId, "id", starHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the star attribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @param starAttributeId
     * @throws VogcException
     */
    protected void updateStarHasAttStarAttributeId(String userName, String password, int starHasAttId, int starAttributeId) throws VogcException {
        try {
            if (starHasAttId < 0 || starAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_has_attribute", "starAttributeId", starAttributeId, "id", starHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the source id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @param sourceId
     * @throws VogcException
     */
    protected void updateStarHasAttSourceId(String userName, String password, int starHasAttId, String sourceId) throws VogcException {
        try {
            if (starHasAttId < 0 || sourceId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_has_attribute", "sourceId", sourceId, "id", starHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value in a starHasAttrbute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId
     * @return
     * @throws VogcException
     */
// protected void updateStarHasAttValue(String userName, String password, int starHasAttId, String value)throws VogcException{
//       try{
//          if (starHasAttId  < 0 || value == null)
//                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
//          if (DbStatements.update(connection, userName, password, "star_has_attribute", "value", value, "id", starHasAttId) == 0)
//              throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
//       } catch (SQLException ex) {
//            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
//        }
//    }
    protected StarHasAttribute selectStarSourceId(String userName, String password, String objectId) throws VogcException {
        ResultSet r = null;
        try {
            if (objectId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }

            r = DbStatements.query2(connection, userName, password, "star_has_attribute", "sourceId", "starId", objectId);

            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            StarHasAttribute att = new StarHasAttribute();
            att.setSourceId(r.getString("sourceId"));
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                r.close();
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    protected void updateStarHasAttValue(String userName, String password, String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        try {
            if (Gcclusterattribute == null || GcclusterId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_has_attribute", value, "starId", GcclusterId, "starAttributeId", "star_attribute", Gcclusterattribute) == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag first in a starHasAttrbute
     * table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @param first the new boolean value to insert
     * @throws VogcException
     */
    protected void updateStarHasAttFirst(String userName, String password, int starHasAttId, boolean first) throws VogcException {
        try {
            if (starHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "star_has_attribute", "first", first, "id", starHasAttId) == 0) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the value of the boolean flag first in a starHasAttrbute
     * table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param value
     * @param GcclusterId
     * @param Gcclusterattribute
     * @throws VogcException
     */
    protected void updateStarHasAttDate(String userName, String password, String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        try {
            if (Gcclusterattribute == null || GcclusterId == null) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
//            Calendar cal = Calendar.getInstance();
//            Date date = new Date(cal.getTimeInMillis());
            if (DbStatements.update(connection, userName, password, "star_has_attribute", "starId", GcclusterId, "starAttributeId", "star_attribute", Gcclusterattribute) == 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
// protected void updateStarHasAttDate(String userName, String password, int starHasAttId)throws VogcException{
//       try{
//          if (starHasAttId  < 0 )
//                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
//          Calendar cal = Calendar.getInstance();
//          Date date = new Date(cal.getTimeInMillis());
//          if (DbStatements.update(connection, userName, password, "star_has_attribute", "date", date, "id", starHasAttId) == 0)
//              throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
//       } catch (SQLException ex) {
//            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
//        }
//    }

    /**
     * Method to select the star id of a starHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @return - the star id
     * @throws VogcException
     */
    protected String selectStarHasAttStarId(String userName, String password, int starHasAttId) throws VogcException {
        return this.selectStarHasAttribute(userName, password, starHasAttId).getStarId();
    }

    /**
     * Method to select the star's attribute id in a starHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @return - the star's attribute id
     * @throws VogcException
     */
    protected int selectStarHasAttStarAttId(String userName, String password, int starHasAttId) throws VogcException {

        return this.selectStarHasAttribute(userName, password, starHasAttId).getStarAttId();
    }

    /**
     * Method to select the source id in a starHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @return - the source id
     * @throws VogcException
     */
    protected String selectStarHasAttSourceId(String userName, String password, int starHasAttId) throws VogcException {
        return this.selectStarHasAttribute(userName, password, starHasAttId).getSourceId();

    }

    /**
     * Method to select an attribute's value in a starHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @return - the attribute's value
     * @throws VogcException
     */
    protected String selectStarHasAttValue(String userName, String password, int starHasAttId) throws VogcException {

        return this.selectStarHasAttribute(userName, password, starHasAttId).getValue();
    }

    /**
     * Method to select the value of the boolean flag first in a
     * starHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @return - the boolean flag's value
     * @throws VogcException
     */
    protected boolean selectStarHasAttFirst(String userName, String password, int starHasAttId) throws VogcException {

        return this.selectStarHasAttribute(userName, password, starHasAttId).isFirst();
    }

    /**
     * Method to select the date of insert in a starHasAttribute table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId the starHasAttribute id
     * @return - date of insert
     * @throws VogcException
     */
    protected Date selectStarHasAttDate(String userName, String password, int starHasAttId) throws VogcException {

        return (Date) this.selectStarHasAttribute(userName, password, starHasAttId).getDate();
    }

    /**
     * Method used to select all the starHasAttribute objects having the same
     * star id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starId the star id
     * @return - an array of starHasAttribute objects
     * @throws VogcException
     */
    protected StarHasAttribute[] selectStarHasAttributeByStarId(String userName, String password, String starId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "star_has_attribute", "*", "starId", starId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            StarHasAttribute attribute[] = new StarHasAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new StarHasAttribute(r.getInt("id"), r.getInt("starAttributeId"), r.getString("starId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the starHasAttribute objects having the same
     * source id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starId the source id
     * @return - an array of starHasAttribute objects
     * @throws VogcException
     */
    protected StarHasAttribute[] selectStarHasAttBySourceId(String userName, String password, String starId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "star_has_attribute", "*", "sourceId", starId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }
            r.last();
            StarHasAttribute[] attribute = new StarHasAttribute[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = (StarHasAttribute) r.getStatement();
            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a starHasAttribute object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starHasAttId
     * @return - a StarHasAttribute object
     * @throws VogcException
     */
    protected StarHasAttribute selectStarHasAttribute(String userName, String password, int starHasAttId) throws VogcException {
        try {
            if (starHasAttId < 0) {
                throw new VogcException(Messages.getError(Messages.STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "star_has_attribute", "*", "id", starHasAttId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.STARATTRIBUTE_NOT_EXISTS));
            }
            StarHasAttribute att = new StarHasAttribute(starHasAttId, r.getInt("starAttributeId"), r.getString("starId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));
            return att;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the PulsarHasAttribute objects having the same
     * plsAttribute id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param attributeId the pulsar's attribute id
     * @return - an array of PulsarHasAttribute objects
     * @throws VogcException
     */
    protected StarHasAttribute[] selectStarHasAttByAttributeId(String userName, String password, int attributeId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "star_has_attribute", "*", "starAttributeId", attributeId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_HAS_ATTRIBUTE_NOT_EXISTS));
            }

            r.last();

            StarHasAttribute attribute[] = new StarHasAttribute[r.getRow()];
            r.beforeFirst();

            int i = -1;
            while (r.next()) {
                i++;
                attribute[i] = new StarHasAttribute(r.getInt("id"), r.getInt("starAttributeId"), r.getString("starId"), r.getString("sourceId"), r.getString("value"), r.getDate("date"), r.getBoolean("first"));

            }
            return attribute;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
