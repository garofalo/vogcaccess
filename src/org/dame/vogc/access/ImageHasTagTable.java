package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class ImageHasTagTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new association between Image and Tag
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag id
     * @param imageId
     * @throws VogcException
     */
    protected void insertImageHasTag(String userName, String password, int tagId, int imageId) throws VogcException {
        if (imageId < 0 || tagId < 0) {
            throw new VogcException(Messages.getError(Messages.IMAGE_HAS_TAG_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
//            Calendar cal = Calendar.getInstance();
//            Date dateNow = new Date(cal.getTimeInMillis());
            PreparedStatement stm = con.prepareStatement("insert into image_has_tag values(?,?)");
            stm.setInt(1, imageId);
            stm.setInt(2, tagId);

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_OR_TAG_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the tags asscoiated to an image
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - an array of Tag objects
     * @throws VogcException
     */
    protected int[] selectTagsInImage(String userName, String password, int imageId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "image_has_tag", "*", "imageId", imageId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.IMAGE_OR_TAG_NOT_EXISTS));
            }
            r.last();
            int[] tag = new int[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                tag[i] = r.getInt("tagId");
            }
            return tag;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the tags asscoiated to an image
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag id
     * @return - an array of Image objects
     * @throws VogcException
     */
    protected int[] selectImageSameTag(String userName, String password, int tagId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "image_has_tag", "*", "tagId", tagId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.IMAGE_OR_TAG_NOT_EXISTS));
            }
            r.last();
            int[] img = new int[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                img[i] = r.getInt("imageId");
            }
            return img;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
