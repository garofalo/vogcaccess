package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.Tag;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina
 */
public class TagTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new tag in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tag the tag object to insert
     * @throws VogcException
     */
    protected void insertTag(String userName, String password, Tag tag) throws VogcException {
        if (tag == null || tag.getId() < 0) {
            throw new VogcException(Messages.getError(Messages.TAG_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into tag values(?,?)");
            stm.setInt(1, tag.getId());
            stm.setString(2, tag.getName());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.TAG_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a tag from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag id to delete
     * @throws VogcException
     */
    protected void deleteTag(String userName, String password, int tagId) throws VogcException {
        try {
            if (tagId < 0) {
                throw new VogcException(Messages.getError(Messages.TAG_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from tag where id=?");
            stm.setInt(1, tagId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.TAG_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a tag from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag id to select
     * @return - a tag object
     * @throws VogcException
     */
    protected Tag selectTag(String userName, String password, int tagId) throws VogcException {
        try {
            if (tagId < 0) {
                throw new VogcException(Messages.getError(Messages.TAG_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "tag", "*", "id", tagId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.TAG_NOT_EXISTS));
            }
            Tag tag = new Tag(tagId, r.getString("name"));
            return tag;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * This method selects all the tag that correspond wholly or in part to a
     * string
     *
     * @param userName username for database access
     * @param password password for database access
     * @param aString the string to search
     * @return an array of tag corresponding to the string
     * @throws VogcException
     */
    protected int[] selectTagContainingAString(String userName, String password, String aString) throws VogcException {

        try {
            if (aString == null || aString.trim().length() == 0) {
                throw new VogcException(Messages.getError(Messages.TAG_PARAMETER_WRONG_OR_NULL));
            }

            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("select id from tag where name like '%" + aString + "%'");
            //  PreparedStatement stm = con.prepareStatement("select id from vobject where id like '%"+aString+"%'");
            //PreparedStatement stm = con.prepareStatement("select id from tag where name = ?");// like '%"+aString+"%'");
            //stm.setString(1, aString);
            //  PreparedStatement stm = con.prepareStatement(" select * from vobject");
            ResultSet r = stm.executeQuery();
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.TAG_NOT_EXISTS));
            }
            r.last();
            int[] tagsId = new int[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                tagsId[i] = r.getInt("id");
            }
            return tagsId;

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select the tag's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag id
     * @return - the tag's name
     * @throws VogcException
     */
    protected String selectTagName(String userName, String password, int tagId) throws VogcException {

        return this.selectTag(userName, password, tagId).getName();
    }

    /**
     * Method to update the tag's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId the tag's id
     * @param name the new tag's name
     * @throws VogcException
     */
    protected void updateTagName(String userName, String password, int tagId, String name) throws VogcException {
        try {
            if (tagId < 0 || name == null) {
                throw new VogcException(Messages.getError(Messages.TAG_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "tag", "name", name, "id", tagId) == 0) {
                throw new VogcException(Messages.getError(Messages.TAG_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the tag stored in the DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - an array of String objects containig the tags
     * @throws VogcException
     */
    protected String[] selectAllTag(String userName, String password) throws VogcException {

        try {
            ResultSet r = DbStatements.query(connection, userName, password, "tag", "name");
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.TAG_NOT_EXISTS));
            }
            r.last();
            String tag[] = new String[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                tag[i] = r.getString("name");
            }
            return tag;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
