package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class VObjectHasTagTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new association between Object and Tag
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the objectId
     * @param tagId the tag id
     * @throws VogcException
     */
    protected void insertVObjectHasTag(String userName, String password, int tagId, String objectId) throws VogcException {
        if (objectId == null || tagId < 0) {
            throw new VogcException(Messages.getError(Messages.VOBJECT_HAS_TAG_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
//            Calendar cal = Calendar.getInstance();
//            Date date = new Date(cal.getTimeInMillis());
            PreparedStatement stm = con.prepareStatement("insert into object_has_tag values(?,?)");
            stm.setString(1, objectId);
            stm.setInt(2, tagId);

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_HAS_TAG_INSERT_FAILED));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the tags asscoiated to an object
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId
     * @return - an array of Tag id
     * @throws VogcException
     */
    protected int[] selectTagsInVObject(String userName, String password, String objectId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "vobject_has_tag", "*", "objectId", objectId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_HAS_TAG_NOT_EXISTS));
            }
            r.last();
            int[] tagId = new int[r.getRow()];

            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                tagId[i] = r.getInt("tagId");
            }
            return tagId;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the object with the same tag
     *
     * @param userName username for database access
     * @param password password for database access
     * @param tagId
     * @return - an array of objects id
     * @throws VogcException
     */
    protected String[] selectVObjectSameTag(String userName, String password, int tagId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "vobject_has_tag", "*", "tagId", tagId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.VOBJECT_HAS_TAG_NOT_EXISTS));
            }

            r.last();
            String[] obId = new String[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                obId[i] = r.getString("objectId");
            }
            return obId;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
