package org.dame.vogc.access;

import java.sql.SQLException;
import java.util.Date;
import org.dame.vogc.datatypes.Author;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.datatypes.BiblioRef;
import org.dame.vogc.datatypes.Catalogue;
import org.dame.vogc.datatypes.GcAttribute;
import org.dame.vogc.datatypes.Gcluster;
import org.dame.vogc.datatypes.GclusterHasAttribute;
import org.dame.vogc.datatypes.Image;
import org.dame.vogc.datatypes.Notes;
import org.dame.vogc.datatypes.Paper;
import org.dame.vogc.datatypes.PlsAttribute;
import org.dame.vogc.datatypes.Pulsar;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.datatypes.Session;
import org.dame.vogc.datatypes.Source;
import org.dame.vogc.datatypes.Star;
import org.dame.vogc.datatypes.StarAttribute;
import org.dame.vogc.datatypes.StarHasAttribute;
import org.dame.vogc.datatypes.Tag;
import org.dame.vogc.datatypes.User;
import org.dame.vogc.datatypes.VObject;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class VogcAccess implements IVOGCAccess {

    private static String userName;
    private static String password;

    /**
     *
     * @param newUserName
     * @param newPassword
     */
    public VogcAccess(String newUserName, String newPassword) {

        userName = newUserName;
        password = newPassword;
    }

    @Override
    public void insertVObject(VObject object) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();

        vobjectTable.insertVObject(userName, password, object);
    }

    @Override
    public void deleteVObject(String objectId) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();
        vobjectTable.deleteVObject(userName, password, objectId);
    }

    @Override
    public VObject selectVObject(String objectId) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();
        return vobjectTable.selectVObject(userName, password, objectId);
    }

    @Override
    public VObject[] selectVObjectsByType(int type) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();
        return vobjectTable.selectVObjectsByType(userName, password, type);

    }

    @Override
    public int selectVObjectType(String objectId) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();
        return vobjectTable.selectVObjectType(userName, password, objectId);
    }

    @Override
    public void updateVObjectId(String oldId, String newId) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();
        vobjectTable.updateVObjectId(userName, password, oldId, newId);
    }

    @Override
    public void updateVObjectType(String objectId, int type) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();
        vobjectTable.updateVObjectType(userName, password, objectId, type);
    }

    @Override
    public void insertGcluster(Gcluster cluster) throws VogcException {
        GclusterTable gclusterTable = new GclusterTable();
        gclusterTable.insertGcluster(userName, password, cluster);
    }

    @Override
    public String[] selectPartialResult(String value) throws VogcException {
        VObjectTable vobjectTable = new VObjectTable();
        return vobjectTable.selectPartialResult(userName, password, value);
    }

    @Override
    public void deleteGcluster(String gclusterId) throws VogcException {
        GclusterTable gclusterTable = new GclusterTable();
        gclusterTable.deleteGcluster(userName, password, gclusterId);
    }

    @Override
    public Gcluster selectGcluster(String gclusterId) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        return cluster.selectGcluster(userName, password, gclusterId);
    }

    @Override
    public int selectGclusterType(String clusterId) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        return cluster.selectGclusterType(userName, password, clusterId);
    }

    @Override
    public String selectGclusterName(String clusterId) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        return cluster.selectGclusterName(userName, password, clusterId);
    }

    @Override
    public Gcluster[] selectGclustersByType(int type) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        return cluster.selectGclustersByType(userName, password, type);
    }

    @Override
    public void updateGclusterId(String oldId, String newId) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        cluster.updateGclusterId(userName, password, oldId, newId);
    }

    @Override
    public void updateGclusterName(String clusterId, String name) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        cluster.updateGclusterName(userName, password, clusterId, name);
    }

    @Override
    public void updateGclusterType(String clusterId, int type) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        cluster.updateGclusterType(userName, password, clusterId, type);
    }

    @Override
    public void insertPulsar(Pulsar object) throws VogcException {
        PulsarTable pulsarTable = new PulsarTable();
        pulsarTable.insertPulsar(userName, password, object);

    }

    @Override
    public void deletePulsar(String pulsarId) throws VogcException {
        PulsarTable pulsarTable = new PulsarTable();
        pulsarTable.deletePulsar(userName, password, pulsarId);
    }

    @Override
    public Pulsar selectPulsar(String pulsarId) throws VogcException {
        PulsarTable pulsarTable = new PulsarTable();
        return pulsarTable.selectPulsar(userName, password, pulsarId);
    }

    @Override
    public String selectGclusterId(String pulsarId) throws VogcException {
        PulsarTable pulsarTable = new PulsarTable();
        return pulsarTable.selectGclusterId(userName, password, pulsarId);
    }

    @Override
    public Pulsar[] selectPulsarsByClusterId(String clusterId) throws VogcException {
        PulsarTable pulsarTable = new PulsarTable();
        return pulsarTable.selectPulsarsByClusterId(userName, password, clusterId);
    }

    @Override
    public void updatePulsarId(String oldId, String newId) throws VogcException {
        PulsarTable pulsarTable = new PulsarTable();
        pulsarTable.updatePulsarId(userName, password, oldId, newId);
    }

    @Override
    public void updatePulsarGclusterId(String objectId, String gclusterId) throws VogcException {
        PulsarTable pulsarTable = new PulsarTable();
        pulsarTable.updatePulsarGclusterId(userName, password, objectId, gclusterId);
    }

    @Override
    public void insertStar(Star object) throws VogcException {
        StarTable starTable = new StarTable();
        starTable.insertStar(userName, password, object);
    }

    @Override
    public void deleteStar(String starId) throws VogcException {
        StarTable starTable = new StarTable();
        starTable.deleteStar(userName, password, starId);
    }

    @Override
    public Star selectStar(String starId) throws VogcException {
        StarTable starTable = new StarTable();
        return starTable.selectStar(userName, password, starId);
    }

    @Override
    public String selectStarGclusterId(String starId) throws VogcException {
        StarTable starTable = new StarTable();
        return starTable.selectStarGclusterId(userName, password, starId);
    }

    @Override
    public Star[] selectStarsByClusterId(String clusterId) throws VogcException {
        StarTable starTable = new StarTable();
        return starTable.selectStarsByClusterId(userName, password, clusterId);
    }

    @Override
    public void updateStarId(String oldId, String newId) throws VogcException {
        StarTable starTable = new StarTable();
        starTable.updateStarId(userName, password, oldId, newId);
    }

    @Override
    public void updateStarGclusterId(String objectId, String gclusterId) throws VogcException {
        StarTable starTable = new StarTable();
        starTable.updateStarGclusterId(userName, password, objectId, gclusterId);
    }

    @Override
    public int insertGcAttribute(GcAttribute att) throws VogcException, SQLException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.insertGcAttribute(userName, password, att);
    }

    @Override
    public void deleteGcAttribute(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.deleteGcAttribute(userName, password, gcAttributeId);
    }

    @Override
    public GcAttribute selectGcAttribute(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttribute(userName, password, gcAttributeId);
    }

    @Override
    public GcAttribute[] selectPrimaryAttributes(int type) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectPrimaryAttributes(userName, password, type);
    }

    @Override
    public GcAttribute[] selectGcAttributeByType(int type) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeByType(userName, password, type);
    }

    @Override
    public GcAttribute[] selectGcAttributeByUcd(String ucd) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeByUcd(userName, password, ucd);
    }

    @Override
    public String selectGcAttributeName(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeName(userName, password, gcAttributeId);
    }

    @Override
    public String selectGcAttributeDescription(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeDescription(userName, password, gcAttributeId);
    }

    @Override
    public String selectGcAttributeUcd(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeUcd(userName, password, gcAttributeId);
    }

    @Override
    public String selectGcAttributeDatatype(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeDatatype(userName, password, gcAttributeId);
    }

    @Override
    public Boolean selectGcAttributePrimaryAtt(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributePrimaryAtt(userName, password, gcAttributeId);
    }

    @Override
    public int selectGcAttributeType(int gcAttributeId) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeType(userName, password, gcAttributeId);
    }

    @Override
    public int selectGcAttributeIdByName(String gcAttName) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectGcAttributeIdByName(userName, password, gcAttName);
    }

    @Override
    public void checkGcAttributeIsNew(String gcAttName) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.checkGcAttributeIsNew(userName, password, gcAttName);
    }

    @Override
    public GcAttribute[] selectAllGcAttribute() throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        return gcAtt.selectAllGcAttribute(userName, password);
    }

    @Override
    public void updateGcAttributeName(int attributeId, String name) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.updateGcAttributeName(userName, password, attributeId, name);
    }

    @Override
    public void updateGcAttributeDescription(int attributeId, String description) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.updateGcAttributeDescription(userName, password, attributeId, description);
    }

    @Override
    public void updateGcAttributeUcd(int attributeId, String ucd) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.updateGcAttributeUcd(userName, password, attributeId, ucd);
    }

    @Override
    public void updateGcAttributeDatatype(int attributeId, String datatype) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.updateGcAttributeDatatype(userName, password, attributeId, datatype);
    }

    @Override
    public void updateGcAttributeType(int attributeId, int type) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.updateGcAttributeType(userName, password, attributeId, type);
    }

    @Override
    public void updateGcAttributePrimaryAtt(int gcAttributeId, boolean primaryAtt) throws VogcException {
        GcAttributeTable gcAtt = new GcAttributeTable();
        gcAtt.updateGcAttributePrimaryAtt(userName, password, gcAttributeId, primaryAtt);
    }

    @Override
    public int insertPlsAttribute(PlsAttribute att) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.insertPlsAttribute(userName, password, att);
    }

    @Override
    public void deletePlsAttribute(int plsAttributeId) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        plsAtt.deletePlsAttribute(userName, password, plsAttributeId);
    }

    @Override
    public PlsAttribute selectPlsAttribute(int plsAttributeId) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsAttribute(userName, password, plsAttributeId);
    }

    @Override
    public int selectPlsAttributeIdByName(String plsAttName) throws VogcException {
        PlsAttributeTable gcAtt = new PlsAttributeTable();
        return gcAtt.selectPlsAttributeIdByName(userName, password, plsAttName);
    }

    @Override
    public void checkPlsAttributeIsNew(String plsAttName) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        plsAtt.checkPlsAttributeIsNew(userName, password, plsAttName);
    }

    @Override
    public PlsAttribute[] selectAllPlsAttribute() throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectAllPlsAttribute(userName, password);
    }

    @Override
    public String selectPlsAttributeName(int plsAttributeId) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsAttributeName(userName, password, plsAttributeId);
    }

    @Override
    public String selectPlsAttributeDescription(int plsAttributeId) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsAttributeDescription(userName, password, plsAttributeId);
    }

    @Override
    public String selectPlsAttributeUcd(int plsAttributeId) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsAttributeUcd(userName, password, plsAttributeId);
    }

    @Override
    public String selectPlsAttributeDatatype(int plsAttributeId) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsAttributeDatatype(userName, password, plsAttributeId);
    }

    @Override
    public Boolean selectPlsAttributePrimaryAtt(int plsAttributeId) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsAttributePrimaryAtt(userName, password, plsAttributeId);
    }

    @Override
    public PlsAttribute[] selectPlsAttributeByUcd(String ucd) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsAttributeByUcd(userName, password, ucd);
    }

    @Override
    public PlsAttribute[] selectPlsPrimaryAttributes(String userName, String password) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        return plsAtt.selectPlsPrimaryAttributes(userName, password);
    }

    @Override
    public void updatePlsAttributeName(int plsAttributeId, String name) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        plsAtt.updatePlsAttributeName(userName, password, plsAttributeId, name);
    }

    @Override
    public void updatePlsAttributeDescription(int plsAttributeId, String description) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        plsAtt.updatePlsAttributeDescription(userName, password, plsAttributeId, description);
    }

    @Override
    public void updatePlsAttributeUcd(int plsAttributeId, String ucd) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        plsAtt.updatePlsAttributeUcd(userName, password, plsAttributeId, ucd);
    }

    @Override
    public void updatePlsAttributeDatatype(int plsAttributeId, String datatype) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        plsAtt.updatePlsAttributeDatatype(userName, password, plsAttributeId, datatype);
    }

    @Override
    public void updatePlsAttributePrimaryAtt(int plsAttributeId, boolean primaryAtt) throws VogcException {
        PlsAttributeTable plsAtt = new PlsAttributeTable();
        plsAtt.updatePlsAttributePrimaryAtt(userName, password, plsAttributeId, primaryAtt);
    }

    @Override
    public int insertStarAttribute(StarAttribute att) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.insertStarAttribute(userName, password, att);
    }

    @Override
    public void deleteStarAttribute(int starAttributeId) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        starAtt.deleteStarAttribute(userName, password, starAttributeId);
    }

    @Override
    public StarAttribute selectStarAttribute(int starAttributeId) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttribute(userName, password, starAttributeId);
    }

    @Override
    public void checkStarAttributeIsNew(String starAttName) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        starAtt.checkStarAttributeIsNew(userName, password, starAttName);
    }

    @Override
    public StarAttribute[] selectAllStarAttribute() throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectAllStarAttribute(userName, password);
    }

    @Override
    public int selectStarAttributeIdByName(String attName) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttributeIdByName(userName, password, attName);
    }

    @Override
    public String selectStarAttributeName(int starAttributeId) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttributeName(userName, password, starAttributeId);
    }

    @Override
    public String selectStarAttributeDescription(int starAttributeId) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttributeDescription(userName, password, starAttributeId);
    }

    @Override
    public String selectStarAttributeUcd(int starAttributeId) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttributeUcd(userName, password, starAttributeId);
    }

    @Override
    public String selectStarAttributeDatatype(int starAttributeId) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttributeDatatype(userName, password, starAttributeId);
    }

    @Override
    public Boolean selectStarAttributePrimaryAtt(int starAttributeId) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttributePrimaryAtt(userName, password, starAttributeId);
    }

    @Override
    public StarAttribute[] selectStarAttributeByUcd(String ucd) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarAttributeByUcd(userName, password, ucd);
    }

    @Override
    public StarAttribute[] selectStarPrimaryAttributes(String userName, String password) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        return starAtt.selectStarPrimaryAttributes(userName, password);
    }

    @Override
    public void updateStarAttributeName(int starAttributeId, String name) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        starAtt.updateStarAttributeName(userName, password, starAttributeId, name);
    }

    @Override
    public void updateStarAttributeDescription(int starAttributeId, String description) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        starAtt.updateStarAttributeDescription(userName, password, starAttributeId, description);
    }

    @Override
    public void updateStarAttributeUcd(int starAttributeId, String ucd) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        starAtt.updateStarAttributeUcd(userName, password, starAttributeId, ucd);
    }

    @Override
    public void updateStarAttributeDatatype(int starAttributeId, String datatype) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        starAtt.updateStarAttributeUcd(userName, password, starAttributeId, userName);
    }

    @Override
    public void updateStarAttributePrimaryAtt(int starAttributeId, boolean primaryAtt) throws VogcException {
        StarAttributeTable starAtt = new StarAttributeTable();
        starAtt.updateStarAttributePrimaryAtt(userName, password, starAttributeId, primaryAtt);
    }

    @Override
    public void insertGclusterHasAttribute(GclusterHasAttribute att) throws VogcException, SQLException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        gcHasAtt.insertGclusterHasAttribute(userName, password, att);
    }

    @Override
    public void deleteGclusterHasAttribute(int gclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        gcHasAtt.deleteGclusterHasAttribute(userName, password, gclusterHasAttId);
    }

    @Override
    public GclusterHasAttribute selectGclusterHasAttribute(int gclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterHasAttribute(userName, password, gclusterHasAttId);
    }

    @Override
    public GclusterHasAttribute selectGclusterSourceId(String objectId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterSourceId(userName, password, objectId);
    }

    @Override
    public void updateGclusterHasAttGclusterId(int gclusterHasAttId, String gclusterId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        gcHasAtt.updateGclusterHasAttGclusterId(userName, password, gclusterHasAttId, gclusterId);
    }

    @Override
    public void updateGclusterHasAttGcAttId(int gclusterHasAttId, int gclusterAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        gcHasAtt.updateGclusterHasAttGcAttId(userName, password, gclusterHasAttId, gclusterAttId);

    }

    @Override
    public void updateGclusterHasAttSourceId(int gclusterHasAttId, String sourceId) throws VogcException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

//    public void updateGclusterHasAttValue(int gclusterHasAttId, String value) throws VogcException {
//        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
//        gcHasAtt.updateGclusterHasAttValue(userName, password, gclusterHasAttId, value);
//    }
    @Override
    public void updateGclusterHasAttValue(String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        gcHasAtt.updateGclusterHasAttValue(userName, password, value, GcclusterId, Gcclusterattribute);
    }

    @Override
    public void updateGclusterHasAttFirst(int gclusterHasAttId, boolean first) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        gcHasAtt.updateGclusterHasAttFirst(userName, password, gclusterHasAttId, first);
    }

//    public void updateGclusterHasAttDate(int gclusterHasAttId) throws VogcException {
//        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
//        gcHasAtt.updateGclusterHasAttDate(userName, password, gclusterHasAttId);
//    }
    @Override
    public void updateGclusterHasAttDate(String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        gcHasAtt.updateGclusterHasAttDate(userName, password, value, GcclusterId, Gcclusterattribute);
    }

    @Override
    public String selectGclusterHasAttGclusterId(int gclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterHasAttGclusterId(userName, password, gclusterHasAttId);
    }

    @Override
    public int selectGclusterHasAttGcAttId(int gclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterHasAttGcAttId(userName, password, gclusterHasAttId);
    }

    @Override
    public String selectGclusterHasAttSourceId(int gclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterHasAttSourceId(userName, password, gclusterHasAttId);
    }

    @Override
    public String selectGclusterHasAttValue(int gclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterHasAttValue(userName, password, gclusterHasAttId);
    }

    @Override
    public boolean selectGclusterHasAttFirst(int GclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterHasAttFirst(userName, password, GclusterHasAttId);
    }

    @Override
    public Date selectGclusterHasAttDate(int GclusterHasAttId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGclusterHasAttDate(userName, password, GclusterHasAttId);
    }

    @Override
    public GclusterHasAttribute[] selectGcHasAttByGclusterId(String gclusterId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGcHasAttByGclusterId(userName, password, gclusterId);
    }

    @Override
    public GclusterHasAttribute[] selectGcHasAttBySourceId(String sourceId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGcHasAttBySourceId(userName, password, sourceId);
    }

    @Override
    public GclusterHasAttribute[] selectGcHasAttByGclusterIdandObjectId(String objectId, int attId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGcHasAttByGclusterIdandObjectId(userName, password, objectId, attId);
    }

    @Override
    public GclusterHasAttribute[] selectPositionalParameter() throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectParameter(userName, password);
    }

    @Override
    public GclusterHasAttribute[] selectPhotometricParameter() throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectPhotometricParameter(userName, password);
    }

    @Override
    public GclusterHasAttribute[] selectStructuralParameter() throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectStructuralParameter(userName, password);
    }

    @Override
    public GclusterHasAttribute[] selectGcHasAttByAttributeId(int attributeId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGcHasAttByAttributeId(userName, password, attributeId);
    }

    @Override
    public int selectGcHasAttNumber(int attributeId) throws VogcException {
        GclusterHasAttributeTable gcHasAtt = new GclusterHasAttributeTable();
        return gcHasAtt.selectGcHasAttNumber(userName, password, attributeId);
    }

    @Override
    public void insertPulsarHasAttribute(PulsarHasAttribute att) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.insertPulsarHasAttribute(userName, password, att);
    }

    @Override
    public void deletePulsarHasAttribute(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.deletePulsarHasAttribute(userName, password, plsHasAttId);
    }

    @Override
    public void updatePulsarHasAttPulsarId(int plsHasAttId, String pulsarId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.updatePulsarHasAttPulsarId(userName, password, plsHasAttId, plsHasAttId);
    }

    @Override
    public void updatePulsarHasAttPlsAttributeId(int plsHasAttId, int plsAttributeId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.updatePulsarHasAttPlsAttributeId(userName, password, plsHasAttId, plsAttributeId);
    }

    @Override
    public void updatePulsarHasAttSourceId(int plsHasAttId, String sourceId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.updatePulsarHasAttSourceId(userName, password, plsHasAttId, sourceId);
    }

    @Override
    public PulsarHasAttribute selectPulsarSourceId(String objectId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarSourceId(userName, password, objectId);
    }
//    public void updatePulsarHasAttValue(int plsHasAttId, String value) throws VogcException {
//        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
//        plsHasAtt.updatePulsarHasAttValue(userName, password, plsHasAttId, value);
//    }

    @Override
    public PulsarHasAttribute[] selectPlsHasAttByPulsarIdandObjectId(String objectId, int attId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPlsHasAttByPulsarIdandObjectId(userName, password, objectId, attId);
    }

    @Override
    public void updatePulsarHasAttValue(String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.updatePulsarHasAttValue(userName, password, value, GcclusterId, Gcclusterattribute);
    }

    @Override
    public void updatePulsarHasAttFirst(int plsHasAttId, boolean first) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.updatePulsarHasAttFirst(userName, password, plsHasAttId, first);
    }

//    public void updatePulsarHasAttDate(int plsHasAttId) throws VogcException {
//        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
//        plsHasAtt.updatePulsarHasAttDate(userName, password, plsHasAttId);
//    }
    @Override
    public void updatePulsarHasAttDate(String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        plsHasAtt.updatePulsarHasAttDate(userName, password, value, GcclusterId, Gcclusterattribute);
    }

    @Override
    public PulsarHasAttribute selectPulsarHasAttribute(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarHasAttribute(userName, password, plsHasAttId);
    }

    @Override
    public String selectPulsarHasAttPulsarId(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarHasAttPulsarId(userName, password, plsHasAttId);
    }

    @Override
    public int selectPulsarHasAttPlsAttributeId(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarHasAttPlsAttributeId(userName, password, plsHasAttId);
    }

    @Override
    public String selectPulsarHasAttSourceId(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarHasAttSourceId(userName, password, plsHasAttId);
    }

    @Override
    public String selectPulsarHasAttValue(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarHasAttValue(userName, password, plsHasAttId);
    }

    @Override
    public boolean selectPulsarHasAttFirst(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarHasAttFirst(userName, password, plsHasAttId);
    }

    @Override
    public Date selectPulsarHasAttDate(int plsHasAttId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPulsarHasAttDate(userName, password, plsHasAttId);
    }

    @Override
    public StarHasAttribute[] selectStarHasAttByStarIdandObjectId(String objectId, int attId) throws VogcException {
        StarHasAttributeTable StarHasAtt = new StarHasAttributeTable();
        return StarHasAtt.selectStarHasAttByStarIdandObjectId(userName, password, objectId, attId);
    }

    @Override
    public PulsarHasAttribute[] selectPlsHasAttByPulsarId(String pulsarId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPlsHasAttByPulsarId(userName, password, pulsarId);
    }

    @Override
    public PulsarHasAttribute[] selectPlsHasAttBySourceId(String sourceId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPlsHasAttBySourceId(userName, password, sourceId);
    }

    @Override
    public PulsarHasAttribute[] selectPlsHasAttByAttributeId(int attributeId) throws VogcException {
        PulsarHasAttributeTable plsHasAtt = new PulsarHasAttributeTable();
        return plsHasAtt.selectPlsHasAttByAttributeId(userName, password, attributeId);
    }

    @Override
    public void insertStarHasAttribute(StarHasAttribute att) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.insertStarHasAttribute(userName, password, att);
    }

    @Override
    public void deleteStarHasAttribute(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.deleteStarHasAttribute(userName, password, starHasAttId);
    }

    @Override
    public void updateStarHasAttStarId(int starHasAttId, String starId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.updateStarHasAttStarId(userName, password, starHasAttId, starId);
    }

    @Override
    public void updateStarHasAttStarAttributeId(int starHasAttId, int starAttributeId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.updateStarHasAttStarAttributeId(userName, password, starHasAttId, starAttributeId);
    }

    @Override
    public void updateStarHasAttSourceId(int starHasAttId, String sourceId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.updateStarHasAttSourceId(userName, password, starHasAttId, sourceId);
    }

//    public void updateStarHasAttValue(int starHasAttId, String value) throws VogcException {
//        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
//        starHasAtt.updateStarHasAttValue(userName, password, starHasAttId, value);
//    }
    @Override
    public void updateStarHasAttValue(String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.updateStarHasAttValue(userName, password, value, GcclusterId, Gcclusterattribute);
    }

    @Override
    public void updateStarHasAttFirst(int starHasAttId, boolean first) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.updateStarHasAttFirst(userName, password, starHasAttId, first);
    }

//    public void updateStarHasAttDate(int starHasAttId) throws VogcException {
//        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
//        starHasAtt.updateStarHasAttDate(userName, password, starHasAttId);
//    }
    @Override
    public void updateStarHasAttDate(String value, String GcclusterId, String Gcclusterattribute) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        starHasAtt.updateStarHasAttDate(userName, password, value, GcclusterId, Gcclusterattribute);
    }

    @Override
    public StarHasAttribute selectStarHasAttribute(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttribute(userName, password, starHasAttId);
    }

    @Override
    public String selectStarHasAttStarId(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttStarId(userName, password, starHasAttId);
    }

    @Override
    public int selectStarHasAttStarAttId(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttStarAttId(userName, password, starHasAttId);
    }

    @Override
    public String selectStarHasAttSourceId(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttSourceId(userName, password, starHasAttId);
    }

    @Override
    public String selectStarHasAttValue(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttValue(userName, password, starHasAttId);
    }

    @Override
    public StarHasAttribute selectStarSourceId(String objectId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarSourceId(userName, password, objectId);
    }

    @Override
    public boolean selectStarHasAttFirst(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttFirst(userName, password, starHasAttId);
    }

    @Override
    public Date selectStarHasAttDate(int starHasAttId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttDate(userName, password, starHasAttId);
    }

    @Override
    public StarHasAttribute[] selectStarHasAttByStarId(String starId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttributeByStarId(userName, password, starId);
    }

    @Override
    public StarHasAttribute[] selectStarHasAttBySourceId(String starId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttBySourceId(userName, password, starId);
    }

    @Override
    public StarHasAttribute[] selectStarHasAttByAttributeId(int attributeId) throws VogcException {
        StarHasAttributeTable starHasAtt = new StarHasAttributeTable();
        return starHasAtt.selectStarHasAttByAttributeId(userName, password, attributeId);
    }

    @Override
    public int insertAuthor(Author author) throws VogcException, SQLException {
        AuthorTable aut = new AuthorTable();
        return aut.insertAuthor(userName, password, author);
    }

    @Override
    public void deleteAuthor(int authorId) throws VogcException {
        AuthorTable aut = new AuthorTable();
        aut.deleteAuthor(userName, password, authorId);
    }

    @Override
    public Author selectAuthor(int authorId) throws VogcException {
        AuthorTable aut = new AuthorTable();
        return aut.selectAuthor(userName, password, authorId);
    }

    @Override
    public Author[] selectAllAuthors() throws VogcException {
        AuthorTable aut = new AuthorTable();
        return aut.selectAllAuthors(userName, password);
    }

    @Override
    public String selectAuthorName(int authorId) throws VogcException {
        AuthorTable aut = new AuthorTable();
        return aut.selectAuthorName(userName, password, authorId);
    }

    @Override
    public String selectAuthorUri(int authorId) throws VogcException {
        AuthorTable aut = new AuthorTable();
        return aut.selectAuthorUri(userName, password, authorId);
    }

    public int selectLastAuthor() throws VogcException {
        AuthorTable aut = new AuthorTable();
        return aut.selectLastAuthor(userName, password);
    }

    @Override
    public void updateAuthorName(int authorId, String name) throws VogcException {
        AuthorTable aut = new AuthorTable();
        aut.updateAuthorName(userName, password, authorId, name);
    }

    @Override
    public void updateAuthorUri(int authorId, String uri) throws VogcException {
        AuthorTable aut = new AuthorTable();
        aut.updateAuthorUri(userName, password, authorId, uri);
    }

    @Override
    public int insertBiblioNotes(BiblioNotes object) throws VogcException, SQLException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.insertBiblioNotes(userName, password, object);
    }

    @Override
    public void deleteBiblioNotes(int objectId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.deleteBiblioNotes(userName, password, objectId);
    }

    @Override
    public BiblioNotes selectBiblioNotes(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotes(userName, password, id);
    }

    @Override
    public int selectLastBiblioNotes() throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectLastBiblioNotes(userName, password);
    }

    @Override
    public String selectBiblioNotesObjectId(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesObjectId(userName, password, id);
    }

    @Override
    public int selectBiblioNotesGcAttributeId(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesGcAttributeId(userName, password, id);
    }

    @Override
    public int selectBiblioNotesStarAttributeId(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesStarAttributeId(userName, password, id);
    }

    @Override
    public int selectBiblioNotesPulsarAttributeId(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesPulsarAttributeId(userName, password, id);
    }

    @Override
    public String selectBiblioNotesUserId(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesUserId(userName, password, id);
    }

    @Override
    public int selectBiblioNotesImageId(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesImageId(userName, password, id);
    }

    @Override
    public Date selectBiblioNotesDate(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesDate(userName, password, id);
    }

    @Override
    public int selectBiblioNotesType(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesType(userName, password, id);
    }

    @Override
    public void updateBiblioNotesObjectId(int id, String objectId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesObjectId(userName, password, id, objectId);
    }

    @Override
    public void updateBiblioNotesGcAttributeId(int id, int gcAttributeId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesGcAttributeId(userName, password, id, gcAttributeId);
    }

    @Override
    public void updateBiblioNotesStarAttributeId(int id, int starAttributeId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesStarAttributeId(userName, password, id, starAttributeId);
    }

    @Override
    public void updateBiblioNotesPlsAttributeId(int id, int plsAttributeId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesPlsAttributeId(userName, password, id, plsAttributeId);
    }

    @Override
    public void updateBiblioNotesUserId(int id, String userId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesUserId(userName, password, id, userId);
    }

    @Override
    public void updateBiblioNotesImageId(int id, int imageId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesImageId(userName, password, id, imageId);
    }

    @Override
    public void updateBiblioNotesDate(int id) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesDate(userName, password, id);
    }

    @Override
    public void updateBiblioNotesType(int id, int type) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        biblio.updateBiblioNotesType(userName, password, id, type);
    }

    @Override
    public BiblioNotes[] selectBiblioNotesByObjectId(String objectId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesByObjectId(userName, password, objectId);
    }

    @Override
    public BiblioNotes[] selectBiblioNotesByGcAttributeId(int gcAttId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesByGcAttributeId(userName, password, gcAttId);
    }

    @Override
    public BiblioNotes[] selectBiblioNotesByStarAttributeId(int starAttId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesByStarAttributeId(userName, password, starAttId);
    }

    @Override
    public BiblioNotes[] selectBiblioNotesByPlsAttributeId(int plsAttId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesByPlsAttributeId(userName, password, plsAttId);
    }

    @Override
    public BiblioNotes[] selectBiblioNotesByUserId(String userId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesByUserId(userName, password, userId);
    }

    @Override
    public BiblioNotes[] selectBiblioNotesByImageId(int imageId) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesByImageId(userName, password, imageId);
    }

    @Override
    public BiblioNotes[] selectBiblioNotesByType(int type) throws VogcException {
        BiblioNotesTable biblio = new BiblioNotesTable();
        return biblio.selectBiblioNotesByType(userName, password, type);
    }

    @Override
    public void insertBiblioRef(BiblioRef object) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        biblioRef.insertBiblioRef(userName, password, object);
    }

    @Override
    public void deleteBiblioRef(int id) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        biblioRef.deleteBiblioRef(userName, password, id);
    }

    @Override
    public BiblioRef selectBiblioRef(int id) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        return biblioRef.selectBiblioRef(userName, password, id);
    }

    @Override
    public BiblioRef[] selectBiblioRefByPaper(int paperId) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        return biblioRef.selectBiblioRefByPaper(userName, password, paperId);
    }

    @Override
    public BiblioRef[] selectBiblioRefByYear(String year) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        return biblioRef.selectBiblioRefByYear(userName, password, year);
    }

    @Override
    public int selectBiblioRefPaperId(int id) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        try {
            return biblioRef.selectBiblioRefPaperId(userName, password, id);
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
        }
    }

    @Override
    public String selectBiblioRefTitle(int id) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        try {
            return biblioRef.selectBiblioRefTitle(userName, password, id);
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
        }
    }

    @Override
    public String selectBiblioRefDescription(int id) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        try {
            return biblioRef.selectBiblioRefDescription(userName, password, id);
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
        }
    }

    @Override
    public String selectBiblioRefUri(int id) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        try {
            return biblioRef.selectBiblioRefUri(userName, password, id);
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
        }
    }

    @Override
    public String selectBiblioRefYear(int id) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        try {
            return biblioRef.selectBiblioRefYear(userName, password, id);
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.BIBLIOREF_PARAMETER_WRONG_OR_NULL));
        }
    }

    @Override
    public void updateBiblioRefPaperId(int id, int paperId) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        biblioRef.updateBiblioRefPaperId(userName, password, id, paperId);
    }

    @Override
    public void updateBiblioRefTitle(int id, String title) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        biblioRef.updateBiblioRefTitle(userName, password, id, title);
    }

    @Override
    public void updateBiblioRefDescription(int id, String description) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        biblioRef.updateBiblioRefDescription(userName, password, id, description);
    }

    @Override
    public void updateBiblioRefUri(int id, String uri) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        biblioRef.updateBiblioRefUri(userName, password, id, uri);
    }

    @Override
    public void updateBiblioRefYear(int id, String year) throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        biblioRef.updateBiblioRefYear(userName, password, id, year);
    }

    @Override
    public int selectLastBiblioRef() throws VogcException {
        BiblioRefTable biblioRef = new BiblioRefTable();
        return biblioRef.selectLastBiblioRef(userName, password);
    }

    @Override
    public void insertCatalogue(Catalogue catalogue) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        cat.insertCatalogue(userName, password, catalogue);
    }

    @Override
    public void deleteCatalogue(int id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        cat.deleteCatalogue(userName, password, id);
    }

    @Override
    public Catalogue selectCatalogue(String id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        return cat.selectCatalogue(userName, password, id);
    }

    @Override
    public String selectCatalogueTitle(String id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        return cat.selectCatalogueTitle(userName, password, id);
    }

    @Override
    public String selectCatalogueDescription(String id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        return cat.selectCatalogueDescription(userName, password, id);
    }

    @Override
    public String selectCatalogueYear(String id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        return cat.selectCatalogueYear(userName, password, id);
    }

    @Override
    public Date selectCatalogueDate(String id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        return cat.selectCatalogueDate(userName, password, id);
    }

    @Override
    public String selectCatalogueUri(String id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        return cat.selectCatalogueUri(userName, password, id);
    }

    @Override
    public void updateCatalogueTitle(String id, String title) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        cat.updateCatalogueTitle(userName, password, id, title);
    }

    @Override
    public void updateCatalogueDescription(String id, String description) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        cat.updateCatalogueDescription(userName, password, id, description);
    }

    @Override
    public void updateCatalogueYear(String id, String year) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        cat.updateCatalogueYear(userName, password, id, year);
    }

    @Override
    public void updateCatalogueUri(String id, String uri) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        cat.updateCatalogueUri(userName, password, id, uri);
    }

    @Override
    public void updateCatalogueDate(String id) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        cat.updateCatalogueDate(userName, password, id);
    }

    @Override
    public Catalogue[] selectCatalogueByYear(String year) throws VogcException {
        CatalogueTable cat = new CatalogueTable();
        return cat.selectCatalogueByYear(userName, password, year);
    }

    @Override
    public void insertImage(Image img) throws VogcException {
        ImageTable image = new ImageTable();
        image.insertImage(userName, password, img);
    }

    @Override
    public void deleteImage(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        image.deleteImage(userName, password, imageId);
    }

    @Override
    public Image selectImage(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImage(userName, password, imageId);
    }

    @Override
    public String selectImageName(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageName(userName, password, imageId);
    }

    @Override
    public String selectImageObjectId(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageObjectId(userName, password, imageId);
    }

    @Override
    public String selectImageUserId(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageUserId(userName, password, imageId);
    }

    @Override
    public String selectImageDescription(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageDescription(userName, password, imageId);
    }

    @Override
    public String selectImageScale(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageScale(userName, password, imageId);
    }

    @Override
    public String selectImageFormat(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageFormat(userName, password, imageId);
    }

    @Override
    public String selectImageDimension(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageDimension(userName, password, imageId);
    }

    @Override
    public String selectImageUri(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageUri(userName, password, imageId);
    }

    @Override
    public Date selectImageDate(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImageDate(userName, password, imageId);
    }

    @Override
    public Image[] selectImageByObjectId(String objectId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImagesByObjectId(userName, password, objectId);
    }

    @Override
    public Image[] selectImageByUserId(String userId) throws VogcException {
        ImageTable image = new ImageTable();
        return image.selectImagesByUserId(userName, password, userId);
    }

    @Override
    public void updateImageObjectId(int imageId, String objectId) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageObjectId(userName, password, imageId, objectId);
    }

    @Override
    public void updateImageUserId(int imageId, String userId) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageUserId(userName, password, imageId, userId);
    }

    @Override
    public void updateImageName(int imageId, String name) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageName(userName, password, imageId, name);
    }

    @Override
    public void updateImageDescription(int imageId, String description) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageDescription(userName, password, imageId, description);
    }

    @Override
    public void updateImageScale(int imageId, String scale) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageScale(userName, password, imageId, scale);
    }

    @Override
    public void updateImageFormat(int imageId, String format) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageFormat(userName, password, imageId, format);
    }

    @Override
    public void updateImageDimension(int imageId, String dimension) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageDimension(userName, password, imageId, dimension);
    }

    @Override
    public void updateImageUri(int imageId, String uri) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageUri(userName, password, imageId, uri);
    }

    @Override
    public void updateImageDate(int imageId) throws VogcException {
        ImageTable image = new ImageTable();
        image.updateImageDate(userName, password, imageId);
    }

    @Override
    public void insertNotes(Notes object) throws VogcException {
        NotesTable notes = new NotesTable();
        notes.insertNotes(userName, password, object);
    }

    @Override
    public void deleteNotes(int objectId) throws VogcException {
        NotesTable notes = new NotesTable();
        notes.deleteNotes(userName, password, objectId);
    }

    @Override
    public Notes selectNotes(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotes(userName, password, id);
    }

    @Override
    public Notes[] selectNotesByType(int type) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesByType(userName, password, type);
    }

    @Override
    public String selectNotesTitle(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesTitle(userName, password, id);
    }

    @Override
    public String selectNotesDescription(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesDescription(userName, password, id);
    }

    @Override
    public int selectNotesType(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesType(userName, password, id);
    }

    @Override
    public String selectNotesObjectId(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesObjectId(userName, password, id);
    }

    @Override
    public int selectNotesGcAttributeId(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesGcAttributeId(userName, password, id);
    }

    @Override
    public int selectNotesStarAttributeId(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesStarAttributeId(userName, password, id);
    }

    @Override
    public int selectNotesPulsarAttributeId(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesPulsarAttributeId(userName, password, id);
    }

    @Override
    public String selectNotesUserId(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesUserId(userName, password, id);
    }

    @Override
    public int selectNotesImageId(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesImageId(userName, password, id);
    }

    @Override
    public Date selectNotesDate(int id) throws VogcException {
        NotesTable notes = new NotesTable();
        return notes.selectNotesDate(userName, password, id);
    }

    @Override
    public void updateNotesTitle(int id, String title) throws VogcException {
        NotesTable notes = new NotesTable();
        notes.updateNotesTitle(userName, password, id, title);
    }

    @Override
    public void updateNotesDescription(int id, String description) throws VogcException {
        NotesTable notes = new NotesTable();
        notes.updateNotesDescription(userName, password, id, description);
    }

    @Override
    public void updateNotesType(int id, int type) throws VogcException {
        NotesTable notes = new NotesTable();
        notes.updateNotesType(userName, password, id, type);
    }

    @Override
    public int insertPaper(Paper paper) throws VogcException, SQLException {
        PaperTable p = new PaperTable();
        return p.insertPaper(userName, password, paper);
    }

    @Override
    public void deletePaper(int paperId) throws VogcException {
        PaperTable p = new PaperTable();
        p.deletePaper(userName, password, paperId);
    }

    @Override
    public Paper selectPaper(int paperId) throws VogcException {
        PaperTable p = new PaperTable();
        return p.selectPaper(userName, password, paperId);
    }

    @Override
    public Paper[] selectAllPapers() throws VogcException {
        PaperTable p = new PaperTable();
        return p.selectAllPapers(userName, password);
    }

    @Override
    public String selectPaperName(int paperId) throws VogcException {
        PaperTable p = new PaperTable();
        return p.selectPaperName(userName, password, paperId);
    }

    @Override
    public String selectPaperUri(int paperId) throws VogcException {
        PaperTable p = new PaperTable();
        return p.selectPaperUri(userName, password, paperId);
    }

    public int selectLastPaper() throws VogcException {
        PaperTable p = new PaperTable();
        return p.selectLastPaper(userName, password);
    }

    @Override
    public void updatePaperName(int paperId, String name) throws VogcException {
        PaperTable p = new PaperTable();
        p.updatePaperName(userName, password, paperId, name);
    }

    @Override
    public void updatePaperUri(int paperId, String uri) throws VogcException {
        PaperTable p = new PaperTable();
        p.updatePaperUri(userName, password, paperId, uri);
    }

    @Override
    public void insertSession(String userId) throws VogcException {
        SessionTable session = new SessionTable();
        session.insertSession(userName, password, userId);
    }

    @Override
    public void deleteSession(int ssid) throws VogcException {
        SessionTable session = new SessionTable();
        session.deleteSession(userName, password, ssid);
    }

    @Override
    public Session selectSession(int ssid) throws VogcException {
        SessionTable session = new SessionTable();
        return session.selectSession(userName, password, ssid);
    }

    @Override
    public String selectSessionUserId(int ssid) throws VogcException {
        SessionTable session = new SessionTable();
        return session.selectSessionUserId(userName, password, ssid);
    }

    @Override
    public Date selectSessionCreationDate(int ssid) throws VogcException {
        SessionTable session = new SessionTable();
        return session.selectSessionCreationDate(userName, password, ssid);
    }

    @Override
    public Date selectSessionLastAccessDate(int ssid) throws VogcException {
        SessionTable session = new SessionTable();
        return session.selectSessionLastAccessDate(userName, password, ssid);
    }

    @Override
    public void updateSessionLastAccessDate(int id) throws VogcException {
        SessionTable session = new SessionTable();
        session.updateSessionLastAccessDate(userName, password, id);
    }

    @Override
    public Session[] selectUserSessions(String userId) throws VogcException {
        SessionTable session = new SessionTable();
        return session.selectUserSessions(userName, password, userId);
    }

    @Override
    public int[] getUserSessionsId(String userId) throws VogcException {
        SessionTable session = new SessionTable();
        return session.getUserSessionsId(userName, password, userId);
    }
    /*
     * public void checkSsid(String mail, String ssid) throws VogcException {
     * UserTable userTable = new UserTable();
     * if (!userTable..getSsid(userName, password, mail).equals(ssid))
     * throw new VogcException(Messages.USER_WRONG_SSID);
     * }
     */

    @Override
    public int authenticate(String userId) throws VogcException {
        UserTable userTable = new UserTable();
        int ret = 0;
        if (userTable.selectUser(userName, password, userId) != null) {
            ret = 1;
        }
        return ret;
    }

    @Override
    public void insertSource(Source src, int type) throws VogcException {
        SourceTable source = new SourceTable();
        source.insertSource(userName, password, src, type);
    }

    @Override
    public void deleteSource(int sourceId) throws VogcException {
        SourceTable source = new SourceTable();
        source.deleteSource(userName, password, sourceId);
    }

    @Override
    public Source selectSource(String sourceId) throws VogcException {
        SourceTable source = new SourceTable();
        return source.selectSource(userName, password, sourceId);

    }

    @Override
    public Source[] selectSourceByType(int type) throws VogcException {
        SourceTable source = new SourceTable();
        return source.selectSourceByType(userName, password, type);
    }

    @Override
    public int selectSourceType(String sourceId) throws VogcException {
        SourceTable source = new SourceTable();
        return source.selectSourceType(userName, password, sourceId);
    }

    @Override
    public void updateSourceType(String sourceId, int type) throws VogcException {
        SourceTable source = new SourceTable();
        source.updateSourceType(userName, password, sourceId, type);
    }

    @Override
    public void updateSourceId(String oldId, String newId) throws VogcException {
        SourceTable source = new SourceTable();
        source.updateSourceId(userName, password, oldId, newId);
    }

    @Override
    public void insertTag(Tag object) throws VogcException {
        TagTable tag = new TagTable();
        tag.insertTag(userName, password, object);
    }

    @Override
    public void deleteTag(int tagId) throws VogcException {
        TagTable tag = new TagTable();
        tag.deleteTag(userName, password, tagId);
    }

    @Override
    public Tag selectTag(int tagId) throws VogcException {
        TagTable tag = new TagTable();
        return tag.selectTag(userName, password, tagId);
    }

    @Override
    public String selectTagName(int tagId) throws VogcException {
        TagTable tag = new TagTable();
        return tag.selectTagName(userName, password, tagId);
    }

    @Override
    public int[] selectTagContainingAString(String aString) throws VogcException {
        TagTable tag = new TagTable();
        return tag.selectTagContainingAString(userName, password, aString);
    }

    @Override
    public String[] selectAllTag() throws VogcException {
        TagTable tag = new TagTable();
        return tag.selectAllTag(userName, password);
    }

    @Override
    public void updateTagName(int tagId, String name) throws VogcException {
        TagTable tag = new TagTable();
        tag.updateTagName(userName, password, tagId, name);
    }

    @Override
    public void insertUser(User user) throws VogcException {
        UserTable usr = new UserTable();
        usr.insertUser(userName, password, user);
    }

    @Override
    public void deleteUser(int userId) throws VogcException {
        UserTable usr = new UserTable();
        usr.deleteUser(userName, password, userId);
    }

    @Override
    public User selectUser(String userId) throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectUser(userName, password, userId);
    }

    @Override
    public String selectUserName(String userId) throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectUserName(userName, password, userId);
    }

    @Override
    public String selectUserPassword(String userId) throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectUserPassword(userName, password, userId);
    }

    @Override
    public String selectUserSurname(String userId) throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectUserSurname(userName, password, userId);
    }

    @Override
    public String selectUserMotivation(String userId) throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectUserAffiliation(userName, password, userId);
    }

    @Override
    public Boolean selectUserStatus(String userId) throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectUserStatus(userName, password, userId);
    }

    @Override
    public void updateUserId(int oldId, int newId) throws VogcException {
        UserTable usr = new UserTable();
        usr.updateUserId(userName, password, oldId, newId);
    }

    @Override
    public void updateUserName(String userId, String name) throws VogcException {
        UserTable usr = new UserTable();
        usr.updateUserName(userName, password, userId, name);
    }

    @Override
    public void updateUserPassword(String userId, String pwd) throws VogcException {
        UserTable usr = new UserTable();
        usr.updateUserPassword(userName, password, userId, pwd);
    }

    @Override
    public void updateUserSurname(String surname, String userId) throws VogcException {
        UserTable usr = new UserTable();
        usr.updateUserSurname(userName, password, surname, userId);
    }

    @Override
    public void updateUserMotivation(String motiv, String userId) throws VogcException {
        UserTable usr = new UserTable();
        usr.updateUserAffiliation(userName, password, motiv, userId);
    }

    @Override
    public void updateUserStatus(Boolean active, String userId) throws VogcException {
        UserTable usr = new UserTable();
        usr.updateUserStatus(userName, password, active, userId);
    }

    @Override
    public User[] selectUsersByStatus(boolean status) throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectUsersByStatus(userName, password, status);
    }

    @Override
    public User[] selectAllUsers() throws VogcException {
        UserTable usr = new UserTable();
        return usr.selectAllUsers(userName, password);
    }

    @Override
    public void insertBiblioHasAuthor(int authorId, int biblioId) throws VogcException {
        BiblioHasAuthorTable biblio = new BiblioHasAuthorTable();
        biblio.insertBiblioHasAuthor(userName, password, authorId, biblioId);
    }

    @Override
    public void deleteBiblioHasAuthorByBiblioId(int biblioId) throws VogcException {
        BiblioHasAuthorTable biblio = new BiblioHasAuthorTable();
        biblio.deleteBiblioHasAuthorByBiblioId(userName, password, biblioId);
    }

    @Override
    public int[] selectBiblioRefByAuthor(int authorId) throws VogcException {
        BiblioHasAuthorTable biblio = new BiblioHasAuthorTable();
        return biblio.selectBiblioRefByAuthor(userName, password, authorId);
    }

    @Override
    public int[] selectAuthorsInBiblioRef(int biblioId) throws VogcException {
        BiblioHasAuthorTable biblio = new BiblioHasAuthorTable();
        return biblio.selectAuthorsInBiblioRef(userName, password, biblioId);
    }

    @Override
    public void insertCatalogueHasAuthor(String catId, int autId) throws VogcException {
        CatalogueHasAuthorTable catHasAut = new CatalogueHasAuthorTable();
        catHasAut.insertCatalogueHasAuthor(userName, password, catId, autId);
    }

    @Override
    public String[] selectCataloguesByAuthor(int autId) throws VogcException {
        CatalogueHasAuthorTable catHasAut = new CatalogueHasAuthorTable();
        return catHasAut.selectCataloguesByAuthor(userName, password, autId);
    }

    @Override
    public int[] selectAuthorsInCatalogue(String catId) throws VogcException {
        CatalogueHasAuthorTable catHasAut = new CatalogueHasAuthorTable();
        return catHasAut.selectAuthorsInCatalogue(userName, password, catId);
    }

    @Override
    public void insertImageHasTag(int tagId, int imageId) throws VogcException {
        ImageHasTagTable imgHasTag = new ImageHasTagTable();
        imgHasTag.insertImageHasTag(userName, password, tagId, imageId);
    }

    @Override
    public int[] selectTagsInImage(int imageId) throws VogcException {
        ImageHasTagTable imgHasTag = new ImageHasTagTable();
        return imgHasTag.selectTagsInImage(userName, password, imageId);
    }

    @Override
    public int[] selectImageSameTag(int tagId) throws VogcException {
        ImageHasTagTable imgHasTag = new ImageHasTagTable();
        return imgHasTag.selectImageSameTag(userName, password, tagId);
    }

    @Override
    public void insertTagHasUser(int tagId, String userId) throws VogcException {
        TagHasUserTable object = new TagHasUserTable();
        object.insertTagHasUser(userName, password, tagId, userId);
    }

    @Override
    public int[] selectTagByUser(String userId) throws VogcException {
        TagHasUserTable object = new TagHasUserTable();
        return object.selectTagByUser(userName, password, userId);
    }

    @Override
    public String selectUserByTag(int tagId) throws VogcException {
        TagHasUserTable object = new TagHasUserTable();
        return object.selectUserByTag(userName, password, tagId);
    }

    @Override
    public String[] selectUsersSameTag(int tagId) throws VogcException {
        TagHasUserTable object = new TagHasUserTable();
        return object.selectUsersSameTag(userName, password, tagId);
    }

    @Override
    public void insertVObjectHasTag(int tagId, String objectId) throws VogcException {
        VObjectHasTagTable obHasTag = new VObjectHasTagTable();
        obHasTag.insertVObjectHasTag(userName, password, tagId, objectId);
    }

    @Override
    public int[] selectTagsInVObject(String objectId) throws VogcException {
        VObjectHasTagTable obHasTag = new VObjectHasTagTable();
        return obHasTag.selectTagsInVObject(userName, password, objectId);
    }

    @Override
    public String[] selectVObjectSameTag(int tagId) throws VogcException {
        VObjectHasTagTable obHasTag = new VObjectHasTagTable();
        return obHasTag.selectVObjectSameTag(userName, password, tagId);
    }

    @Override
    public String[] selectClusterParameterResult(int aInt, String value, String operator) throws VogcException {
        GclusterTable cluster = new GclusterTable();
        return cluster.selectClusterParameterValue(userName, password, aInt, value, operator);

    }

    @Override
    public void existVObjectId(String objectId) throws VogcException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
