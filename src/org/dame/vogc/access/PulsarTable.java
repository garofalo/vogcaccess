package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.Pulsar;
import org.dame.vogc.datatypes.PulsarHasAttribute;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina
 */
public class PulsarTable {

    public PulsarHasAttribute att;
    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method used to insert a new pulsar
     *
     * @param userName username for database access
     * @param password password for database access
     * @param object the Pulsar object to insert
     * @throws VogcException
     */
    protected void insertPulsar(String userName, String password, Pulsar object) throws VogcException {
        if (object.getId() == null || object.getGclusterId() == null) {
            throw new VogcException(Messages.getError(Messages.PULSAR_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into pulsar values(?,?)");
            stm.setString(1, object.getId());
            stm.setString(2, object.getGclusterId());
            stm.executeUpdate(); //aggiunto da LUCA

//            PulsarHasAttributeTable plstable = new PulsarHasAttributeTable();
//            plstable.insertPulsarHasAttribute(userName, password, att);
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

//    try {
//            Connection con = connection.connect();
//           PreparedStatement stm = con.prepareStatement("insert into pulsar values(?,?)");
//
//
//            stm.setString(1, object.getId());
//            stm.setString(2, object.getGclusterId());
//
//
//
//           if(stm.executeUpdate()== 0)
//                throw new VogcException(Messages.getError(Messages.PULSAR_NOT_EXISTS));
//
//
//
//        } catch (SQLException ex) {
//            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
//        }
//        finally{
//            try {
//                connection.disconnect();
//            } catch (SQLException ex) {
//                 throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
//            }
//        }
//    }
    /**
     * Method used to delete a tuple from the pulsar table
     *
     * @param userName username for database access
     * @param password password for database access
     * @param pulsarId the pulsar id to delete
     * @throws VogcException
     */
    protected void deletePulsar(String userName, String password, String pulsarId) throws VogcException {
        try {
            if (pulsarId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from pulsar where objectId=?");
            stm.setString(1, pulsarId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method used to select a pulsar object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param pulsarId
     * @return - a pulsar object
     * @throws VogcException
     */
    protected Pulsar selectPulsar(String userName, String password, String pulsarId) throws VogcException {
        try {
            if (pulsarId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "pulsar", "*", "objectId", pulsarId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_NOT_EXISTS));
            }
            Pulsar pulsar = new Pulsar(pulsarId, r.getString("gclusterId"));
            return pulsar;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a list of pulsars belonging to the same globular cluster
     *
     * @param userName username for database access
     * @param password password for database access
     * @param clusterId the globular cluster id
     * @return - an array of pulsar objects
     * @throws VogcException
     */
    protected Pulsar[] selectPulsarsByClusterId(String userName, String password, String clusterId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "pulsar", "*", "gclusterId", clusterId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.PULSAR_NOT_EXISTS));
            }
            r.last();
            Pulsar pulsar[] = new Pulsar[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                pulsar[i] = new Pulsar(r.getString("objectId"), r.getString("gclusterId"));
            }
            return pulsar;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to update a pulsar id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param oldId the pulsar id to update
     * @param newId the new pulsar id
     * @throws VogcException
     */
    protected void updatePulsarId(String userName, String password, String oldId, String newId) throws VogcException {
        try {
            if (oldId == null || newId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar", "objectId", newId, "objectId", oldId) == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to update the globular cluster id in which the pulsar is
     * contained
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the pulsar id
     * @param gclusterId the new globular cluster id
     * @throws VogcException
     */
    protected void updatePulsarGclusterId(String userName, String password, String objectId, String gclusterId) throws VogcException {
        try {
            if (objectId == null || gclusterId == null) {
                throw new VogcException(Messages.getError(Messages.PULSAR_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "pulsar", "gclusterId", gclusterId, "objectId", objectId) == 0) {
                throw new VogcException(Messages.getError(Messages.PULSAR_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the globular cluster id in which the pulsar is contained
     *
     * @param userName username for database access
     * @param password password for database access
     * @param pulsarId
     * @return - globular cluster id
     * @throws VogcException
     */
    protected String selectGclusterId(String userName, String password, String pulsarId) throws VogcException {

        return this.selectPulsar(userName, password, pulsarId).getGclusterId();

    }
}
