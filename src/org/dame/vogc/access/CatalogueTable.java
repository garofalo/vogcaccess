package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import org.dame.vogc.datatypes.Catalogue;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class CatalogueTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new catalogue in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param catalogue the Catalogue object to insert
     * @throws VogcException
     */
    protected void insertCatalogue(String userName, String password, Catalogue catalogue) throws VogcException {
        if (catalogue.getTitle() == null || catalogue.getUri() == null || catalogue.getYear() == null || catalogue.getDate() == null) {
            throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into catalogue values(?,?,?,?,?,?)");
            stm.setString(1, catalogue.getId());
            stm.setString(2, catalogue.getTitle());
            stm.setString(1, catalogue.getDescription());
            stm.setString(1, catalogue.getYear());
            stm.setString(1, catalogue.getUri());
            stm.setString(4, date.toString());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_INSERT_ERROR));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.GENERIC_ERROR));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a catalogue from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param catId the catalogue id to delete
     * @throws VogcException
     */
    protected void deleteCatalogue(String userName, String password, int id) throws VogcException {
        try {
            if (id < 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from catalogue where id=?");
            stm.setInt(1, id);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a catalogue from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id to select
     * @return - a catalogue object
     * @throws VogcException
     */
    protected Catalogue selectCatalogue(String userName, String password, String id) throws VogcException {

        try {
            if (id == null) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "catalogue", "*", "id", id);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }
            Catalogue cat = new Catalogue(id, r.getString("title"), r.getString("description"), r.getString("year"), r.getString("uri"), r.getDate("date"));
            return cat;

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the catalogue's title
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @return - the catalogue's name
     * @throws VogcException
     */
    protected String selectCatalogueTitle(String userName, String password, String id) throws VogcException {

        return this.selectCatalogue(userName, password, id).getTitle();
    }

    /**
     * Method to select the catalogue's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @return - the catalogue's description
     * @throws VogcException
     */
    protected String selectCatalogueDescription(String userName, String password, String id) throws VogcException {

        return this.selectCatalogue(userName, password, id).getDescription();
    }

    /**
     * Method to select the catalogue's year of pubblication
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @return - the catalogue's year of pubblication
     * @throws VogcException
     */
    protected String selectCatalogueYear(String userName, String password, String id) throws VogcException {

        return this.selectCatalogue(userName, password, id).getYear();
    }

    /**
     * Method to select the date in which the catalogue was added in the
     * database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @return - the catalogue's date
     * @throws VogcException
     */
    protected Date selectCatalogueDate(String userName, String password, String id) throws VogcException {

        return this.selectCatalogue(userName, password, id).getDate();
    }

    /**
     * Method to select the catalogue's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @return - the catalogue's uri
     * @throws VogcException
     */
    protected String selectCatalogueUri(String userName, String password, String id) throws VogcException {

        return this.selectCatalogue(userName, password, id).getUri();
    }

    /**
     * Method to update the catalogue's title
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @param name the catalogue's name
     * @throws VogcException
     */
    protected void updateCatalogueTitle(String userName, String password, String id, String title) throws VogcException {

        try {
            if (id == null || title == null) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "catalogue", "title", title, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the catalogue's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @param description the catalogue's description
     * @throws VogcException
     */
    protected void updateCatalogueDescription(String userName, String password, String id, String description) throws VogcException {

        try {
            if (id == null || description == null) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "catalogue", "description", description, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the catalogue's year of pubblication
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @param year the catalogue's year
     * @throws VogcException
     */
    protected void updateCatalogueYear(String userName, String password, String id, String year) throws VogcException {

        try {
            if (id == null || year == null) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "catalogue", "year", year, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the catalogue's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @param year the catalogue's uri
     * @throws VogcException
     */
    protected void updateCatalogueUri(String userName, String password, String id, String uri) throws VogcException {

        try {
            if (id == null || uri == null) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "catalogue", "uri", uri, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the catalogue's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the catalogue id
     * @throws VogcException
     */
    protected void updateCatalogueDate(String userName, String password, String id) throws VogcException {

        try {
            if (id == null) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_PARAMETER_WRONG_OR_NULL));
            }
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            if (DbStatements.update(connection, userName, password, "catalogue", "date", date, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the active (or not) users
     *
     * @param userName username for database access
     * @param password password for database access
     * @param year the catalogue's year of pubblication
     * @return - an array of catalogue objects of the same year
     * @throws VogcException
     */
    protected Catalogue[] selectCatalogueByYear(String userName, String password, String year) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "catalogue", "*", "year", year);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.CATALOGUE_NOT_EXISTS));
            }
            r.last();
            Catalogue[] cat = new Catalogue[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                cat[i] = (Catalogue) r.getStatement();
            }
            return cat;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
