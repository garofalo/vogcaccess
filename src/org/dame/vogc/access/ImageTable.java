package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.dame.vogc.datatypes.Image;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Luca
 */
public class ImageTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new image in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param img
     * @throws VogcException
     */
    protected void insertImage(String userName, String password, Image img) throws VogcException {
        if (img.getObjectId() == null || img.getUserId() == null || img.getUri() == null) {
            throw new VogcException(Messages.getError(Messages.GENERIC_ERROR));
        }

        try {
            Connection con = connection.connect();
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            PreparedStatement stm = con.prepareStatement("insert into image values (?,?,?,?,?,?,?,?,?,?,?)");
            //insert into image values (5,'AM4','admin','terzo','ciccciobomba322',null,'jpp','800x600','www.ww',date,1)
            stm.setInt(1, 0);
            stm.setString(2, img.getObjectId());
            stm.setString(3, img.getUserId());
            stm.setString(4, img.getName());
            stm.setString(5, img.getDescription());
            stm.setString(6, img.getScale());
            stm.setString(7, img.getFormat());
            stm.setString(8, img.getDimension());
            stm.setString(9, img.getUri());
            stm.setDate(10, date);
            stm.setInt(11, 1);
            if (stm.executeUpdate() == 0) //throw new VogcException(Messages.getError(Messages.AUTHOR_INSERT));
            // }catch(SQLException ex){
            {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.GENERIC_ERROR));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete an image from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id to delete
     * @throws VogcException
     */
    protected void deleteImage(String userName, String password, int imageId) throws VogcException {
        try {
            if (imageId < 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from image where id=?");
            stm.setInt(1, imageId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select an image from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id to select
     * @return - an image object
     * @throws VogcException
     */
    protected Image selectImage(String userName, String password, int imageId) throws VogcException {
        try {
            if (imageId < 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "image", "*", "id", imageId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
            Image img = new Image(imageId, r.getString("objectId"), r.getString("userId"), r.getString("name"), r.getString("description"), r.getString("format"), r.getString("dimension"),
                    r.getString("uri"), r.getDate("date"), r.getString("scale"), r.getInt("type"));
            return img;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the image's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the tag id
     * @return - the image's name
     * @throws VogcException
     */
    protected String selectImageName(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getName();
    }

    /**
     * Method to choose the id of the object that corresponds to the image
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the object id
     * @throws VogcException
     */
    protected String selectImageObjectId(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getObjectId();
    }

    /**
     * Method to select the user id which uploaded the image
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the user id
     * @throws VogcException
     */
    protected String selectImageUserId(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getUserId();
    }

    /**
     * Method to select the image's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the image's description
     * @throws VogcException
     */
    protected String selectImageDescription(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getDescription();
    }

    /**
     * Method to select the image's scale
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the image's scale
     * @throws VogcException
     */
    protected String selectImageScale(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getScale();
    }

    /**
     * Method to select the image's format
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the image's format
     * @throws VogcException
     */
    protected String selectImageFormat(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getFormat();
    }

    /**
     * Method to select the image's format
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the image's format
     * @throws VogcException
     */
    protected String selectImageDimension(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getDimension();
    }

    /**
     * Method to select the image's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the image's uri
     * @throws VogcException
     */
    protected String selectImageUri(String userName, String password, int imageId) throws VogcException {

        return this.selectImage(userName, password, imageId).getUri();
    }

    /**
     * Method to select the image's date of insert
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - the image's date of insert
     * @throws VogcException
     */
    protected Date selectImageDate(String userName, String password, int imageId) throws VogcException {

        return (Date) this.selectImage(userName, password, imageId).getDate();
    }

    /**
     * Method used to select all the images associated to the same object
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the object id
     * @return - an array of image's objects
     * @throws VogcException
     */
    protected Image[] selectImagesByObjectId(String userName, String password, String objectId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "image", "*", "objectId", objectId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
            r.last();

            Image img[] = new Image[r.getRow()];
            r.beforeFirst();
            int i = -1;

            while (r.next()) {
                i++;
                img[i] = new Image(r.getInt("id"), r.getString("objectId"), r.getString("userId"), r.getString("name"), r.getString("description"), r.getString("format"), r.getString("dimension"), r.getString("uri"), r.getDate("date"), r.getString("scale"), r.getInt("type"));
            }
            return img;
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select all the images inserted by the same user
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId
     * @return - an array of image's objects
     * @throws VogcException
     */
    protected Image[] selectImagesByUserId(String userName, String password, String userId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "image", "*", "userId", userId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
            r.last();
            Image img[] = new Image[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                img[i] = new Image(r.getInt("id"), r.getString("objectId"), r.getString("userId"), r.getString("name"), r.getString("description"), r.getString("format"), r.getString("dimension"), r.getString("uri"), r.getDate("date"), r.getString("scale"), r.getInt("type"));
            }
            return img;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the id of the object that corresponds to the image
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param objectId
     * @throws VogcException
     */
    protected void updateImageObjectId(String userName, String password, int imageId, String objectId) throws VogcException {
        try {
            if (imageId < 0 || objectId == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "objectId", objectId, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the user id which uploaded the image
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param userId the new user id
     * @throws VogcException
     */
    protected void updateImageUserId(String userName, String password, int imageId, String userId) throws VogcException {
        try {
            if (imageId < 0 || userId == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "objectId", userId, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param name the new image's name
     * @throws VogcException
     */
    protected void updateImageName(String userName, String password, int imageId, String name) throws VogcException {
        try {
            if (imageId < 0 || name == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "name", name, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's description
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param description the new image's description
     * @throws VogcException
     */
    protected void updateImageDescription(String userName, String password, int imageId, String description) throws VogcException {
        try {
            if (imageId < 0 || description == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "description", description, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's scale
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param scale the new image's scale
     * @throws VogcException
     */
    protected void updateImageScale(String userName, String password, int imageId, String scale) throws VogcException {
        try {
            if (imageId < 0 || scale == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "scale", scale, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's format
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param format the new image's format
     * @throws VogcException
     */
    protected void updateImageFormat(String userName, String password, int imageId, String format) throws VogcException {
        try {
            if (imageId < 0 || format == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "format", format, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's dimension
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param dimension the new image's dimension
     * @throws VogcException
     */
    protected void updateImageDimension(String userName, String password, int imageId, String dimension) throws VogcException {
        try {
            if (imageId < 0 || dimension == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "dimension", dimension, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's uri
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @param uri the new image's uri
     * @throws VogcException
     */
    protected void updateImageUri(String userName, String password, int imageId, String uri) throws VogcException {
        try {
            if (imageId < 0 || uri == null) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "image", "uri", uri, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's date
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @throws VogcException
     */
    protected void updateImageDate(String userName, String password, int imageId) throws VogcException {
        try {
            if (imageId < 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_PARAMETER_WRONG_OR_NULL));
            }
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            if (DbStatements.update(connection, userName, password, "image", "date", date, "id", imageId) == 0) {
                throw new VogcException(Messages.getError(Messages.IMAGE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
