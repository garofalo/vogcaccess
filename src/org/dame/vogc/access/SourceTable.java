package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.dame.vogc.datatypes.Source;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class SourceTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new source in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param src the source object to insert
     * @param type integer that represents the type of source to be included in
     * the database (user or catalogue)
     * @throws VogcException
     */
    protected void insertSource(String userName, String password, Source src, int type) throws VogcException {
        if (src.getType() < 0) {
            throw new VogcException(Messages.getError(Messages.SOURCE_PARAMETER_WRONG_OR_NULL));
        }
        try {
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("insert into source values(?,?)");
            stm.setString(1, src.getId());
            stm.setInt(2, src.getType());

            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.SOURCE_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.GENERIC_ERROR));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a source from the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param sourceId the source id to delete
     * @throws VogcException
     */
    protected void deleteSource(String userName, String password, int sourceId) throws VogcException {
        try {
            if (sourceId < 0) {
                throw new VogcException(Messages.getError(Messages.SOURCE_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from source where id=?");
            stm.setInt(1, sourceId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.SOURCE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select all the sources of the same type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param type the source type
     * @return - an array of source object of the same type
     * @throws VogcException
     */
    protected Source[] selectSourceByType(String userName, String password, int type) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "source", "*", "type", type);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.SOURCE_NOT_EXISTS));
            }
            r.last();
            Source[] src = new Source[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                src[i] = (Source) r.getStatement();
            }
            return src;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select a source from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param sourceId the source id to select
     * @return - a Source object
     * @throws VogcException
     */
    protected Source selectSource(String userName, String password, String sourceId) throws VogcException {
        try {
            if (sourceId == null) {
                throw new VogcException(Messages.getError(Messages.SOURCE_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "source", "*", "id", sourceId);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.SOURCE_NOT_EXISTS));
            }
            Source src = new Source(sourceId, r.getInt("type"));
            return src;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the source type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param sourceId the source id
     * @return - the source type
     * @throws VogcException
     */
    protected int selectSourceType(String userName, String password, String sourceId) throws VogcException {

        return this.selectSource(userName, password, sourceId).getType();
    }

    /**
     * Method to update the tag's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param sourceId the source id
     * @param type the new tsource type
     * @throws VogcException
     */
    protected void updateSourceType(String userName, String password, String sourceId, int type) throws VogcException {
        try {
            if (sourceId == null || type < 0) {
                throw new VogcException(Messages.getError(Messages.SOURCE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "source", "type", type, "id", sourceId) == 0) {
                throw new VogcException(Messages.getError(Messages.SOURCE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the tag's name
     *
     * @param userName username for database access
     * @param password password for database access
     * @param oldId
     * @param newId
     * @throws VogcException
     */
    protected void updateSourceId(String userName, String password, String oldId, String newId) throws VogcException {
        try {
            if (oldId == null || newId == null) {
                throw new VogcException(Messages.getError(Messages.SOURCE_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "source", "id", newId, "id", oldId) == 0) {
                throw new VogcException(Messages.getError(Messages.SOURCE_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }
}
