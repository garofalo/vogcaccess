package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luca
 */
public class ConnectionManager {

    private static ConnectionManager istanza = null;
    private static String address;
    private static String port;
    private static String dbName;
    private static Connection dbconn;

    public static synchronized ConnectionManager getConnectionManager() {

        if (istanza == null || dbconn == null) {
            istanza = new ConnectionManager();
        }

        return istanza;

    }
    private final String user;
    private final String pwd;

    private ConnectionManager() {

        address = "localhost";
        port = "3306";
        dbName = "vogclusters";
        user = "framework";
        pwd = "mfiore";
        dbconn = null;
        try {

            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://" + address + ":" + port + "/" + dbName;
            Class.forName(driver);
            dbconn = DriverManager.getConnection(url, user, pwd);
            Thread.sleep(1000);

        } catch (Exception ex) {
            Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    protected void setHost(String newAddress, String newPort, String newDbName) {

        address = newAddress;
        port = newPort;
        dbName = newDbName;
    }

    protected ConnectionManager getHost() {

        return this;
    }

    protected Connection connect() throws SQLException {

        return ConnectionManager.dbconn;

    }

    protected boolean disconnect() throws SQLException {
        boolean ret = false;
        if (dbconn != null) {
            dbconn.close();
            ret = true;
            dbconn = null;
        }
        return ret;
    }
}
