package org.dame.vogc.access;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.dame.vogc.datatypes.BiblioNotes;
import org.dame.vogc.exceptions.Messages;
import org.dame.vogc.exceptions.VogcException;

/**
 *
 * @author Sabrina Checola
 */
public class BiblioNotesTable {

    private ConnectionManager connection = ConnectionManager.getConnectionManager();

    /**
     * Method to insert a new tag in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param object the BiblioNotes object to be inserted
     * @return - the last id inserted
     * @throws VogcException
     * @throws java.sql.SQLException
     */
    protected int insertBiblioNotes(String userName, String password, BiblioNotes object) throws VogcException, SQLException {

        if (object.getObjectId() == null || object.getGcAttributeId() < 0
                || object.getPlsAttributeId() < 0
                || object.getStarAttributeId() < 0
                || object.getImageId() < 0
                || object.getUserId() == null
                || (object.getType() < 0 || object.getType() > 2)) {
            throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
        }

        try {
            Connection con = connection.connect();
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());

            PreparedStatement stm = con.prepareStatement("insert into biblio_notes values(?,?,?,?,?,?,?,?,?)");
            stm.setInt(1, 0);
            stm.setString(2, object.getObjectId());
            stm.setInt(3, object.getGcAttributeId());
            stm.setInt(4, object.getStarAttributeId());
            stm.setInt(5, object.getPlsAttributeId());
            stm.setString(6, object.getUserId());
            stm.setInt(7, object.getImageId());
            stm.setDate(8, date);
            stm.setInt(9, object.getType());

            try {
                stm.executeUpdate();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_FK_NOT_EXISTS));
            }
            /*         if(stm.executeUpdate() != 1){
             throw new VogcException(Messages.getError(Messages.BIBLIONOTES_FK_NOT_EXISTS));}*/

            stm = con.prepareStatement("select @@Identity as lastId");
            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.LAST_ID_NOT_RETRIEVED));
            }

            return r.getInt("lastId");

            //     } catch (SQLException ex) {
            //         throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to delete a BiblioNote object
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the BiblioNotes object id to be deleted
     * @throws VogcException
     */
    protected void deleteBiblioNotes(String userName, String password, int objectId) throws VogcException {
        try {
            if (objectId < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            Connection con = connection.connect();
            PreparedStatement stm = con.prepareStatement("delete from biblio_notes where id=?");
            stm.setInt(1, objectId);
            if (stm.executeUpdate() == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }

        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }

    /**
     * Method to select a BiblioNotes object from his id
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - a BiblioNotes object
     * @throws VogcException
     */
    protected BiblioNotes selectBiblioNotes(String userName, String password, int id) throws VogcException {
        try {
            if (id < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "id", id);
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }

            BiblioNotes object = new BiblioNotes(id, r.getString("objectId"),
                    r.getInt("gcHasAttributeId"),
                    r.getInt("plsHasAttributeId"),
                    r.getInt("starHasAttributeId"),
                    r.getString("userId"),
                    r.getInt("imageId"),
                    r.getDate("date"),
                    r.getInt("type"));
            return object;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select the id of the object that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the object id
     * @throws VogcException
     */
    protected String selectBiblioNotesObjectId(String userName, String password, int id) throws VogcException {
        return this.selectBiblioNotes(userName, password, id).getObjectId();
    }

    /**
     * Method to select the globular cluster's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the globular cluster's attribute id
     * @throws VogcException
     */
    protected int selectBiblioNotesGcAttributeId(String userName, String password, int id) throws VogcException {
        return this.selectBiblioNotes(userName, password, id).getGcAttributeId();
    }

    /**
     * Method to select the star's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the star's attribute id
     * @throws VogcException
     */
    protected int selectBiblioNotesStarAttributeId(String userName, String password, int id) throws VogcException {
        return this.selectBiblioNotes(userName, password, id).getStarAttributeId();
    }

    /**
     * Method to select the pulsar's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the pulsar's attribute id
     * @throws VogcException
     */
    protected int selectBiblioNotesPulsarAttributeId(String userName, String password, int id) throws VogcException {
        return this.selectBiblioNotes(userName, password, id).getPlsAttributeId();
    }

    /**
     * Method to select the user id which has inserted the notes
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the user id
     * @throws VogcException
     */
    protected String selectBiblioNotesUserId(String userName, String password, int id) throws VogcException {
        return this.selectBiblioNotes(userName, password, id).getUserId();
    }

    /**
     * Method to select the image's id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - the image id
     * @throws VogcException
     */
    protected int selectBiblioNotesImageId(String userName, String password, int id) throws VogcException {
        return this.selectBiblioNotes(userName, password, id).getImageId();
    }

    /**
     * Method to select the date when the notes were inserted in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - a date
     * @throws VogcException
     */
    protected Date selectBiblioNotesDate(String userName, String password, int id) throws VogcException {
        return (Date) this.selectBiblioNotes(userName, password, id).getDate();
    }

    /**
     * Method to select the type of note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @return - an integer representing the type of the note
     * @throws VogcException
     */
    protected int selectBiblioNotesType(String userName, String password, int id) throws VogcException {
        return this.selectBiblioNotes(userName, password, id).getType();
    }

    /**
     * Method to update the id of the object that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id BiblioNotes id
     * @param objectId Object id
     * @throws VogcException
     */
    protected void updateBiblioNotesObjectId(String userName, String password, int id, String objectId) throws VogcException {
        try {
            if (id < 0 || objectId == null) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_notes", "objectId", objectId, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the globular cluster's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id BiblioNotes id
     * @param gcAttributeId globular cluster's attribute id
     * @throws VogcException
     */
    protected void updateBiblioNotesGcAttributeId(String userName, String password, int id, int gcAttributeId) throws VogcException {
        try {
            if (id < 0 || gcAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_notes", "gcAttributeId", gcAttributeId, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the star's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id BiblioNotes id
     * @param starAttributeId star's attribute id
     * @throws VogcException
     */
    protected void updateBiblioNotesStarAttributeId(String userName, String password, int id, int starAttributeId) throws VogcException {
        try {
            if (id < 0 || starAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_notes", "starAttributeId", starAttributeId, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the pulsar's attribute id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id BiblioNotes id
     * @param plsAttributeId pulsar's attribute id
     * @throws VogcException
     */
    protected void updateBiblioNotesPlsAttributeId(String userName, String password, int id, int plsAttributeId) throws VogcException {
        try {
            if (id < 0 || plsAttributeId < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_notes", "plsAttributeId", plsAttributeId, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the user id which has inserted the notes
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @param userId the user id
     * @throws VogcException
     */
    protected void updateBiblioNotesUserId(String userName, String password, int id, String userId) throws VogcException {
        try {
            if (id < 0 || userId == null) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_notes", "userId", userId, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the image's id that owns the note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @param imageId the image id
     * @throws VogcException
     */
    protected void updateBiblioNotesImageId(String userName, String password, int id, int imageId) throws VogcException {
        try {
            if (id < 0 || imageId < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_notes", "imageId", imageId, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the date when the notes were inserted in the database
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @throws VogcException
     */
    protected void updateBiblioNotesDate(String userName, String password, int id) throws VogcException {
        try {
            if (id < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            Calendar cal = Calendar.getInstance();
            Date date = new Date(cal.getTimeInMillis());
            if (DbStatements.update(connection, userName, password, "biblio_notes", "date", date, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to update the type of note
     *
     * @param userName username for database access
     * @param password password for database access
     * @param id the BiblioNotes id to select
     * @param type an integer representing the type of note
     * @throws VogcException
     */
    protected void updateBiblioNotesType(String userName, String password, int id, int type) throws VogcException {
        try {
            if (id < 0 || type < 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
            }
            if (DbStatements.update(connection, userName, password, "biblio_notes", "type", type, "id", id) == 0) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the notes belonging to the same object
     *
     * @param userName username for database access
     * @param password password for database access
     * @param objectId the object id
     * @return - an array of BiblioNotes objects
     * @throws VogcException
     */
    protected BiblioNotes[] selectBiblioNotesByObjectId(String userName, String password, String objectId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "objectId", objectId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            r.last();
            BiblioNotes notes[] = new BiblioNotes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the notes belonging to the same globular cluster's
     * attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param gcAttId the globular cluster's attribute id
     * @return - an array of BiblioNotes objects
     * @throws VogcException
     */
    protected BiblioNotes[] selectBiblioNotesByGcAttributeId(String userName, String password, int gcAttId) throws VogcException {
        /*     BiblioNotes[] notes = null;
         try{
         ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "gcHasAttributeId", gcAttId);
         if(r.next())
         {
         r.last();
         notes = new BiblioNotes[r.getRow()];
         r.beforeFirst();
         int i = -1;
         while(r.next()){
         i++;
         notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
         }
         }
         return notes;
         }catch (SQLException ex) {
         throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
         }*/

        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "gcHasAttributeId", gcAttId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            r.last();
            BiblioNotes notes[] = new BiblioNotes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the notes belonging to the same star's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param starAttId the star's attribute id
     * @return - an array of BiblioNotes objects
     * @throws VogcException
     */
    protected BiblioNotes[] selectBiblioNotesByStarAttributeId(String userName, String password, int starAttId) throws VogcException {

        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "starHasAttributeId", starAttId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            r.last();
            BiblioNotes notes[] = new BiblioNotes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the notes belonging to the same pulsar's attribute
     *
     * @param userName username for database access
     * @param password password for database access
     * @param plsAttId the pulsar's attribute id
     * @return - an array of BiblioNotes objects
     * @throws VogcException
     */
    protected BiblioNotes[] selectBiblioNotesByPlsAttributeId(String userName, String password, int plsAttId) throws VogcException {

        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "plsHasAttributeId", plsAttId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            r.last();
            BiblioNotes notes[] = new BiblioNotes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the notes inserted by the same user
     *
     * @param userName username for database access
     * @param password password for database access
     * @param userId the user id
     * @return - an array of BiblioNotes objects
     * @throws VogcException
     */
    protected BiblioNotes[] selectBiblioNotesByUserId(String userName, String password, String userId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "userId", userId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            r.last();
            BiblioNotes notes[] = new BiblioNotes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the notes associated to the same image
     *
     * @param userName username for database access
     * @param password password for database access
     * @param imageId the image id
     * @return - an array of BiblioNotes objects
     * @throws VogcException
     */
    protected BiblioNotes[] selectBiblioNotesByImageId(String userName, String password, int imageId) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "imageId", imageId);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            r.last();
            BiblioNotes notes[] = new BiblioNotes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method to select all the notes of the same type
     *
     * @param userName username for database access
     * @param password password for database access
     * @param type an integer representing the type of note
     * @return - an array of BiblioNotes objects
     * @throws VogcException
     */
    protected BiblioNotes[] selectBiblioNotesByType(String userName, String password, int type) throws VogcException {
        try {
            ResultSet r = DbStatements.query(connection, userName, password, "biblio_notes", "*", "type", type);
            if (r.wasNull()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            r.last();
            BiblioNotes notes[] = new BiblioNotes[r.getRow()];
            r.beforeFirst();
            int i = -1;
            while (r.next()) {
                i++;
                notes[i] = new BiblioNotes(r.getInt("id"), r.getString("objectId"), r.getInt("gcHasAttributeId"), r.getInt("starHasAttributeId"), r.getInt("plsHasAttributeId"), r.getString("userId"), r.getInt("imageId"), r.getDate("date"), r.getInt("type"));
            }
            return notes;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        }
    }

    /**
     * Method used to select the last key inserted in the DB
     *
     * @param userName username for database access
     * @param password password for database access
     * @return - the last key of the table
     * @throws VogcException
     */
    protected int selectLastBiblioNotes(String userName, String password) throws VogcException {

        try {
            Connection con = connection.connect();

            PreparedStatement stm = con.prepareStatement("SELECT MAX(id) FROM biblio_notes;");

            ResultSet r = stm.executeQuery();
            if (!r.next()) {
                throw new VogcException(Messages.getError(Messages.BIBLIONOTES_NOT_EXISTS));
            }
            int val = r.getInt("MAX(id)");
            return val;
        } catch (SQLException ex) {
            throw new VogcException(Messages.getError(Messages.NO_CONNECTION_TO_DB_SERVER));
        } finally {
            try {
                connection.disconnect();
            } catch (SQLException ex) {
                throw new VogcException(Messages.getError(Messages.DISCONNECT_FAILED));
            }
        }
    }
}
