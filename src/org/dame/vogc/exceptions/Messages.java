package org.dame.vogc.exceptions;

import java.util.HashMap;
import java.util.Map;
import org.dame.vogc.error.Error;

/**
 *
 * @author luca
 */
public class Messages {

    private final static String VOGC = "da01000000";
    public final static String GENERIC_ERROR = "GENERIC_ERROR";
    public final static String LAST_ID_NOT_RETRIEVED = "LAST_ID_NOT_RETRIEVED";
    // Author errors
    public final static String AUTHOR_PARAMETER_WRONG_OR_NULL_VALUE = "AUTHOR_PARAMETER_WRONG_OR_NULL_VALUE";
    public final static String AUTHOR_INSERT_FAILED = "AUTHOR_INSERT_FAILED";
    public final static String AUTHOR_NOT_EXISTS = "AUTHOR_NOT_EXISTS";
    // BiblioHasAuthor errors
    public final static String BIBLIO_HAS_AUTHOR_WRONG_OR_NULL_VALUE = "BIBLIO_HAS_AUTHOR_WRONG_OR_NULL_VALUE";
    public final static String BIBLIO_OR_AUTHOR_NOT_EXISTS = "BIBLIO_OR_AUTHOR_NOT_EXISTS";
    // BiblioNotes errors
    public final static String BIBLIONOTES_PARAMETER_WRONG_OR_NULL = "BIBLIONOTES_PARAMETER_WRONG";
    public final static String BIBLIONOTES_NOT_EXISTS = "BIBLIONOTES_NOT_EXISTS";
    public final static String BIBLIONOTES_FK_NOT_EXISTS = "BIBLIONOTES_FK_NOT_EXISTS";
    // BiblioRef errors
    public final static String BIBLIOREF_PARAMETER_WRONG_OR_NULL = "BIBLIOREF_PARAMETER_WRONG_OR_NULL";
    public final static String BIBLIOREF_FK_NOT_EXISTS = "BIBLIOREF_FK_NOT_EXISTS";
    public final static String BIBLIOREF_NOT_EXISTS = "BIBLIOREF_NOT_EXISTS";
    // CatalogueHasAuthor errors
    public final static String CATALOGUE_HAS_AUTHOR_PARAMETER_WRONG_OR_NULL = "CATALOGUE_HAS_AUTHOR_PARAMETER_WRONG_OR_NULL";
    public final static String CATALOGUE_OR_AUTHOR_NOT_EXISTS = "CATALOGUE_OR_AUTHOR_NOT_EXISTS";
    // Catalogue errors
    public final static String CATALOGUE_PARAMETER_WRONG_OR_NULL = "CATALOGUE_PARAMETER_WRONG_OR_NULL";
    public final static String CATALOGUE_INSERT_ERROR = "CATALOGUE_INSERT_ERROR";
    public final static String CATALOGUE_NOT_EXISTS = "CATALOGUE_NOT_EXISTS";
    // GcAttribute errors
    public final static String GCATTRIBUTE_PARAMETER_WRONG_OR_NULL = "GCATTRIBUTE_PARAMETER_WRONG_OR_NULL";
    public final static String GCATTRIBUTE_NOT_EXISTS = "GCATTRIBUTE_NOT_EXISTS";
    public final static String GCATTRIBUTE_ALREADY_EXISTS = "GCATTRIBUTE_ALREADY_EXISTS";
    // GclusterHasAttribute errors
    public final static String GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL = "GCLUSTER_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL";
    public final static String GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS = "GCLUSTER_HAS_ATTRIBUTE_NOT_EXISTS";
    public final static String GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS = "GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS";
    // Gcluster errors
    public final static String GCLUSTER_PARAMETER_WRONG_OR_NULL = "GCLUSTER_PARAMETER_WRONG_OR_NULL";
    public final static String GCLUSTER_NOT_EXISTS = "GCLUSTER_NOT_EXISTS";
    // ImageHasTag errors
    public final static String IMAGE_HAS_TAG_PARAMETER_WRONG_OR_NULL = "IMAGE_HAS_TAG_PARAMETER_WRONG_OR_NULL";
    public final static String IMAGE_OR_TAG_NOT_EXISTS = "IMAGE_OR_TAG_NOT_EXISTS";
    // Image errors
    public final static String IMAGE_PARAMETER_WRONG_OR_NULL = "IMAGE_PARAMETER_WRONG_OR_NULL";
    public final static String IMAGE_NOT_EXISTS = "IMAGE_NOT_EXISTS";
    // Notes errors
    public final static String NOTES_PARAMETER_WRONG_OR_NULL = "NOTES_PARAMETER_WRONG_OR_NULL";
    public final static String NOTES_FK_NOT_EXISTS = "NOTES_FK_NOT_EXISTS";
    public final static String NOTES_NOT_EXISTS = "NOTES_NOT_EXISTS";
    // Paper errors
    public final static String PAPER_PARAMETER_WRONG_OR_NULL = "PAPER_PARAMETER_WRONG_OR_NULL";
    public final static String PAPER_INSERT_FAILED = "PAPER_INSERT_FAILED";
    public final static String PAPER_NOT_EXISTS = "PAPER_NOT_EXISTS";
    // PlsAttribute errors
    public final static String PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL = "PLSATTRIBUTE_PARAMETER_WRONG_OR_NULL";
    public final static String PLSATTRIBUTE_NOT_EXISTS = "PLSATTRIBUTE_NOT_EXISTS";
    public final static String PLSATTRIBUTE_INSERT_ERROR = "PLSATTRIBUTE_INSERT_ERROR";
    public final static String PLSATTRIBUTE_ALREADY_EXISTS = "PLSATTRIBUTE_ALREADY_EXISTS";
    //PulsarHasAttribute errors
    public final static String PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL = "PULSAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL";
    public final static String PULSAR_HAS_ATTRIBUTE_NOT_EXISTS = "PULSAR_HAS_ATTRIBUTE_NOT_EXISTS";
    public final static String PULSAR_HAS_ATTRIBUTE_INSERT_FAILED = "PULSAR_HAS_ATTRIBUTE_INSERT_FAILED";
    // Pulsar errors
    public final static String PULSAR_PARAMETER_WRONG_OR_NULL = "PULSAR_PARAMETER_WRONG_OR_NULL";
    public final static String PULSAR_INSERT_FAILED = "PULSAR_INSERT_FAILED";
    public final static String PULSAR_NOT_EXISTS = "PULSAR_NOT_EXISTS";
    // Session errors
    public final static String SESSION_PARAMETER_WRONG_OR_NULL = "SESSION_PARAMETER_WRONG_OR_NULL";
    public final static String SESSION_NOT_EXISTS = "SESSION_NOT_EXISTS";
    public final static String SESSION_INSERT_FAILED = "SESSION_INSERT_FAILED";
    // Source errors
    public final static String SOURCE_PARAMETER_WRONG_OR_NULL = "SOURCE_PARAMETER_WRONG_OR_NULL";
    public final static String SOURCE_NOT_EXISTS = "SOURCE_NOT_EXISTS";
    public final static String SOURCE_INSERT_FAILED = "SOURCE_INSERT_FAILED";
    // StarAttribute errors
    public final static String STARATTRIBUTE_PARAMETER_WRONG_OR_NULL = "STARATTRIBUTE_PARAMETER_WRONG_OR_NULL";
    public final static String STARATTRIBUTE_NOT_EXISTS = "STARATTRIBUTE_NOT_EXISTS";
    public final static String STARATTRIBUTE_INSERT_ERROR = "STARATTRIBUTE_INSERT_ERROR";
    public final static String STARATTRIBUTE_ALREADY_EXISTS = "STARATTRIBUTE_ALREADY_EXISTS";
    // StarHasAttribute errors
    public final static String STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL = "STAR_HAS_ATTRIBUTE_PARAMETER_WRONG_OR_NULL";
    public final static String STAR_HAS_ATTRIBUTE_NOT_EXISTS = "STAR_HAS_ATTRIBUTE_NOT_EXISTS";
    public final static String STAR_HAS_ATTRIBUTE_INSERT_FAILED = "STAR_HAS_ATTRIBUTE_INSERT_FAILED";
    // Star errors
    public final static String STAR_PARAMETER_WRONG_OR_NULL = "STAR_PARAMETER_WRONG_OR_NULL";
    public final static String STAR_INSERT_FAILED = "STAR_INSERT_FAILED";
    public final static String STAR_NOT_EXISTS = "STAR_NOT_EXISTS";
    // TagHasUser errors
    public final static String TAG_HAS_USER_PARAMETER_WRONG_OR_NULL = "TAG_HAS_USER_PARAMETER_WRONG_OR_NULL";
    public final static String TAG_HAS_USER_NOT_EXISTS = "TAG_HAS_USER_NOT_EXISTS";
    // Tag errors
    public final static String TAG_PARAMETER_WRONG_OR_NULL = "TAG_PARAMETER_WRONG_OR_NULL";
    public final static String TAG_INSERT_FAILED = "TAG_INSERT_FAILED";
    public final static String TAG_NOT_EXISTS = "TAG_NOT_EXISTS";
    // User errors
    public final static String USER_PARAMETER_WRONG_OR_NULL = "USER_PARAMETER_WRONG_OR_NULL";
    public final static String USER_INSERT_FAILED = "USER_INSERT_FAILED";
    public final static String USER_NOT_EXISTS = "USER_NOT_EXISTS";
    public final static String NO_USER_ENABLED = "NO_USER_ENABLED";
    public final static String USER_WRONG_PASSWORD = "USER_WRONG_PASSWORD";
    // VObjectHasTag errors
    public final static String VOBJECT_HAS_TAG_PARAMETER_WRONG_OR_NULL = "VOBJECT_HAS_TAG_PARAMETER_WRONG_OR_NULL";
    public final static String VOBJECT_HAS_TAG_NOT_EXISTS = "VOBJECT_HAS_TAG_NOT_EXISTS";
    public final static String VOBJECT_HAS_TAG_INSERT_FAILED = "VOBJECT_HAS_TAG_INSERT_FAILED";
    // VObject errors
    public final static String VOBJECT_PARAMETER_WRONG_OR_NULL = "TAG_PARAMETER_WRONG_OR_NULL";
    public final static String VOBJECT_INSERT_FAILED = "VOBJECT_INSERT_FAILED";
    public final static String VOBJECT_NOT_EXISTS = "VOBJECT_NOT_EXISTS";
    public final static String VOBJECT_ALREADY_EXISTS = "VOBJECT_ALREADY_EXISTS";
    public final static String DISCONNECT_FAILED = "DISCONNECT_FAILED";
    public final static String NO_CONNECTION_TO_DB_SERVER = "NO_CONNECTION_TO_DB_SERVER";
    // error code
    private final static String GENERIC_ERROR_code = VOGC + "001";
    private final static String USER_NOT_EXISTS_code = "12345";
    private final static String GCATTRIBUTE_ALREADY_EXISTS_code = "002";
    private final static String GCATTRIBUTE_NOT_EXISTS_code = "003";
    private final static String PLSATTRIBUTE_NOT_EXISTS_code = "004";
    private final static String STARATTRIBUTE_NOT_EXISTS_code = "005";
    private final static String PLSATTRIBUTE_ALREADY_EXISTS_code = "006";
    private final static String STARATTRIBUTE_ALREADY_EXISTS_code = "007";
    private final static String BIBLIONOTES_PARAMETER_WRONG_OR_NULL_code = "008";
    private final static String BIBLIONOTES_NOT_EXISTS_code = "009";
    private final static String BIBLIONOTES_FK_NOT_EXISTS_code = "010";
    private final static String NOTES_PARAMETER_WRONG_OR_NULL_code = "011";
    private final static String NOTES_FK_NOT_EXISTS_code = "012";
    private final static String NOTES_NOT_EXISTS_code = "013";
    private final static String LAST_ID_NOT_RETRIEVED_code = "014";
    private final static String NO_CONNECTION_TO_DB_SERVER_code = "015";
    private final static String GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS_code = "016";
    private final static String VOBJECT_ALREADY_EXISTS_code = VOGC + "009";
    public final static String VOBJECT_NOT_EXISTS_code = VOGC + "004";
    private final static String IMAGE_NOT_EXISTS_code = "010";
    private static Map<String, Error> errorMap_;

    static {
        errorMap_ = new HashMap<String, Error>(25);
        errorMap_.put(Messages.GENERIC_ERROR, new Error(Messages.GENERIC_ERROR_code, Messages.GENERIC_ERROR));
        errorMap_.put(Messages.USER_NOT_EXISTS, new Error(Messages.USER_NOT_EXISTS_code, Messages.USER_NOT_EXISTS));
        errorMap_.put(Messages.VOBJECT_ALREADY_EXISTS, new Error(Messages.VOBJECT_ALREADY_EXISTS_code, Messages.VOBJECT_ALREADY_EXISTS));
        errorMap_.put(Messages.VOBJECT_NOT_EXISTS, new Error(Messages.VOBJECT_NOT_EXISTS_code, Messages.VOBJECT_NOT_EXISTS));
        errorMap_.put(Messages.IMAGE_NOT_EXISTS, new Error(Messages.IMAGE_NOT_EXISTS_code, Messages.IMAGE_NOT_EXISTS));
        errorMap_.put(Messages.GCATTRIBUTE_ALREADY_EXISTS, new Error(Messages.GCATTRIBUTE_ALREADY_EXISTS_code, Messages.GCATTRIBUTE_ALREADY_EXISTS));
        errorMap_.put(Messages.PLSATTRIBUTE_ALREADY_EXISTS, new Error(Messages.PLSATTRIBUTE_ALREADY_EXISTS_code, Messages.PLSATTRIBUTE_ALREADY_EXISTS));
        errorMap_.put(Messages.STARATTRIBUTE_ALREADY_EXISTS, new Error(Messages.STARATTRIBUTE_ALREADY_EXISTS_code, Messages.STARATTRIBUTE_ALREADY_EXISTS));
        errorMap_.put(Messages.GCATTRIBUTE_NOT_EXISTS, new Error(Messages.GCATTRIBUTE_NOT_EXISTS_code, Messages.GCATTRIBUTE_NOT_EXISTS));
        errorMap_.put(Messages.PLSATTRIBUTE_NOT_EXISTS, new Error(Messages.PLSATTRIBUTE_NOT_EXISTS_code, Messages.PLSATTRIBUTE_NOT_EXISTS));
        errorMap_.put(Messages.STARATTRIBUTE_NOT_EXISTS, new Error(Messages.STARATTRIBUTE_NOT_EXISTS_code, Messages.STARATTRIBUTE_NOT_EXISTS));
        errorMap_.put(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL, new Error(Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL_code, Messages.BIBLIONOTES_PARAMETER_WRONG_OR_NULL));
        errorMap_.put(Messages.BIBLIONOTES_NOT_EXISTS, new Error(Messages.BIBLIONOTES_NOT_EXISTS_code, Messages.BIBLIONOTES_NOT_EXISTS));
        errorMap_.put(Messages.BIBLIONOTES_FK_NOT_EXISTS, new Error(Messages.BIBLIONOTES_FK_NOT_EXISTS_code, Messages.BIBLIONOTES_FK_NOT_EXISTS));
        errorMap_.put(Messages.NOTES_PARAMETER_WRONG_OR_NULL, new Error(Messages.NOTES_PARAMETER_WRONG_OR_NULL_code, Messages.NOTES_PARAMETER_WRONG_OR_NULL));
        errorMap_.put(Messages.NOTES_FK_NOT_EXISTS, new Error(Messages.NOTES_FK_NOT_EXISTS_code, Messages.NOTES_FK_NOT_EXISTS));
        errorMap_.put(Messages.NOTES_NOT_EXISTS, new Error(Messages.NOTES_NOT_EXISTS_code, Messages.NOTES_NOT_EXISTS));
        errorMap_.put(Messages.LAST_ID_NOT_RETRIEVED, new Error(Messages.LAST_ID_NOT_RETRIEVED_code, Messages.LAST_ID_NOT_RETRIEVED));
        errorMap_.put(Messages.NO_CONNECTION_TO_DB_SERVER, new Error(Messages.NO_CONNECTION_TO_DB_SERVER_code, Messages.NO_CONNECTION_TO_DB_SERVER));
        errorMap_.put(Messages.GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS, new Error(Messages.GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS_code, Messages.GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS));

        //GCLUSTER_HAS_ATTRIBUTE_FK_NOT_EXISTS
        //NO_CONNECTION_TO_DB_SERVER

        /*
         public final static String NOTES_NOT_EXISTS = "NOTES_NOT_EXISTS";*/
    }

    public static String getErrorString(String error) {
        Error e = errorMap_.get(error);
        return e.getCode() + "- " + e.getMessage();
    }

    public static Error getError(String error) {
        return errorMap_.get(error);
    }
}
