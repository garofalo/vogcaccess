package org.dame.vogc.exceptions;

import org.dame.vogc.error.Error;

/**
 *
 * @author Sabrina
 */
public class VogcException extends Exception {

    private static final long serialVersionUID = 1L;
    String code_;

    public VogcException() {

        super();
    }

    public VogcException(String s) {

        super(s);
    }

    public VogcException(String code, String message) { // invertito

        super(message);
        code_ = code;
    }

    public VogcException(Error e) {

        this(e.getCode(), e.getMessage());

    }

    public String getCode() {
        return code_;
    }

    public Error getError() {

        return new Error(getCode(), getMessage());

    }
}
